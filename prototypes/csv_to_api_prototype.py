# -*- coding: utf-8 -*-
"""
Prototype Edison light front-end API creator

 - this program assumes CSV structures created upstream with all required data for API
   - they may not be CSVs in reality, but will create function to read them into pandas
   - and downstream code will only be pandas, so only inital function will need to change
 - in this prototype, I will dump the data into a JSON file in the structure required by the API
 - in the actual module, the data will be posted to an actual API
 
Created on Thu Sep 16 10:26:01 2021

@author: DanielSweeney
"""

import json
import pandas as pd
import numpy as np

 
in_path = "C:/Users/DanielSweeney/Desktop/git_local/edison_lite/data/"
in_esps = ["vitalite_20210916.csv",
           "fenix_20210916.csv",
           "ecs_20210916.csv",
           "smg_20210916.csv"
           ]

coef_path = "coef_lookup.csv"

out_json_path = 'api_like_json.json'

#configure field names (since these may change)
in_esp = "esp_id"
in_province = "province"
in_acq_dt = "acquisition_dt"
in_gender = "gender"
in_tier = "tier"
in_watts = "nbr_w_coefficient"
in_lights = "nbr_lamps_displace_coefficient"
in_co2 = "nbr_co2_coeffiecent"

out_esp = "espCompanyName"
out_province = "province"
out_acq_dt = "dtAcquisition"
out_gender = "customerGender"
out_tier = "tierLevel"
out_benf = "beneficiaryCoefficient"
out_watts = "wattCoefficient"
out_lights = "lightCoefficient"
out_co2 = "co2Coefficient"
out_conn_type = "connectionType"
out_use = "productUse"

#coefficients
CONST_BENF = 5.2
CONST_CONN_TYPE = "SHS"
CONST_USE = "Household"

# =============================================================================
# STEP 1: Read in data from CSV (probably stored in S3 in production?)
# =============================================================================

def clean_gender(x):
    if pd.isnull(x):
        return "N"
    elif x in ("F", "f"):
        return "F"
    elif x in ("M", "m"):
        return "M"
    else:
        return "N"

def dt_to_date(in_ser):
    return in_ser.dt.strftime("%Y-%m-%d")
    #return in_ser.dt.date



def get_esps(base_path, esp_list):
    ds_list = []
    for i in esp_list:
        curr_ds = pd.read_csv(base_path+i, parse_dates=[in_acq_dt])
        
        ds_list.append(curr_ds)
        
    ds_combo = pd.concat(ds_list)
    
    return ds_combo


def clean_data(df):
    df[out_acq_dt] = dt_to_date(df[in_acq_dt])
    df[out_gender] = df[in_gender].apply(lambda x:clean_gender(x))
    return df


ds_all = get_esps(in_path, in_esps)

ds_all = clean_data(ds_all)

# =============================================================================
# STEP 2: Merge with KPI Coefficients
# =============================================================================

# STEP 2.1: CONSTANT coefficients
ds_all[out_benf] = CONST_BENF
ds_all[out_conn_type] = CONST_CONN_TYPE
ds_all[out_use] = CONST_USE

# STEP 2.2: coeff looked up by tier
ds_coef = pd.read_csv(in_path+coef_path)

#trivial number of productive use systems with tier > 3
ds_all["lookup_tier"] = np.minimum(ds_all[in_tier],3)

ds_all = ds_all.merge(ds_coef, left_on="lookup_tier", right_on="id")


#for dev, just dropping to csv, not required for program function
ds_all.to_csv(in_path+"ds_mid_check.csv", index=False)


# =============================================================================
# STEP 3: Create JSON Structures 
# =============================================================================

# STEP 3.1: drop unused columns
drop_cols = ['shs_key', 
             'acquisition_dt', 
             'lookup_tier', 
             'id', 
             'nbr_beneficiary_coefficient', 
             'nbr_light_coefficient', 
             'nbr_tier_level', 
             'nbr_wh_coefficient', 
             'text_application_tier',
             'gender'
            ]

ds_to_json = ds_all.drop(columns=drop_cols)

# STEP 3.2: re-name columns to output convention
rename_cols = {in_esp : out_esp,
               in_tier : out_tier,
               in_province : out_province,
               in_co2 : out_co2,
               in_lights : out_lights,
               in_watts : out_watts,
              }

ds_to_json.rename(columns = rename_cols, inplace=True)

# STEP 3.3: format as JSON by row
rec_dict = ds_to_json.to_dict(orient='records')


# STEP 3.4: add payload wrapper
out_dict = {'payload':rec_dict}


#out_api = json.dumps(out_dict)
with open(in_path+out_json_path, "w") as f:
    json.dump(out_dict, f)








            