webpackJsonp(["admin.module"],{

/***/ "./src/app/admin/admin-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_component__ = __webpack_require__("./src/app/admin/admin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__data_management_data_management_component__ = __webpack_require__("./src/app/admin/data-management/data-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_management_user_management_component__ = __webpack_require__("./src/app/admin/user-management/user-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__esp_management_esp_management_component__ = __webpack_require__("./src/app/admin/esp-management/esp-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__esp_api_log_esp_api_log_component__ = __webpack_require__("./src/app/admin/esp-api-log/esp-api-log.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var appRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__admin_component__["a" /* AdminComponent */]
    },
    {
        path: 'data-management',
        component: __WEBPACK_IMPORTED_MODULE_3__data_management_data_management_component__["a" /* DataManagementComponent */]
    },
    {
        path: 'user-management',
        component: __WEBPACK_IMPORTED_MODULE_4__user_management_user_management_component__["a" /* UserManagementComponent */]
    },
    {
        path: 'esp-management',
        component: __WEBPACK_IMPORTED_MODULE_5__esp_management_esp_management_component__["a" /* EspManagementComponent */]
    },
    {
        path: 'esp-api-log',
        component: __WEBPACK_IMPORTED_MODULE_6__esp_api_log_esp_api_log_component__["a" /* EspApiLogComponent */]
    }
];
var AdminRoutingModule = /** @class */ (function () {
    function AdminRoutingModule() {
    }
    AdminRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"].forChild(appRoutes)
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"]
            ],
            providers: []
        }),
        __metadata("design:paramtypes", [])
    ], AdminRoutingModule);
    return AdminRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/admin.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/admin.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  admin works!\n</p>\n"

/***/ }),

/***/ "./src/app/admin/admin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AdminComponent = /** @class */ (function () {
    function AdminComponent() {
    }
    AdminComponent.prototype.ngOnInit = function () {
    };
    AdminComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin',
            template: __webpack_require__("./src/app/admin/admin.component.html"),
            styles: [__webpack_require__("./src/app/admin/admin.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AdminComponent);
    return AdminComponent;
}());



/***/ }),

/***/ "./src/app/admin/admin.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_component__ = __webpack_require__("./src/app/admin/admin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__admin_routing_module__ = __webpack_require__("./src/app/admin/admin-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__data_management_data_management_component__ = __webpack_require__("./src/app/admin/data-management/data-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__user_management_user_management_component__ = __webpack_require__("./src/app/admin/user-management/user-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__esp_management_esp_management_component__ = __webpack_require__("./src/app/admin/esp-management/esp-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__esp_api_log_esp_api_log_component__ = __webpack_require__("./src/app/admin/esp-api-log/esp-api-log.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__data_management_data_management_service__ = __webpack_require__("./src/app/admin/data-management/data-management.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__esp_management_esp_management_service__ = __webpack_require__("./src/app/admin/esp-management/esp-management.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__user_management_user_management_service__ = __webpack_require__("./src/app/admin/user-management/user-management.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__esp_api_log_esp_api_log_service__ = __webpack_require__("./src/app/admin/esp-api-log/esp-api-log.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_primeng_primeng__ = __webpack_require__("./node_modules/primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__shared_validator_password_validator__ = __webpack_require__("./src/app/shared/validator/password.validator.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_4__admin_routing_module__["a" /* AdminRoutingModule */], __WEBPACK_IMPORTED_MODULE_3__shared_shared_module__["a" /* SharedModule */].forRoot(), __WEBPACK_IMPORTED_MODULE_13_primeng_primeng__["ConfirmDialogModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__admin_component__["a" /* AdminComponent */], __WEBPACK_IMPORTED_MODULE_5__data_management_data_management_component__["a" /* DataManagementComponent */], __WEBPACK_IMPORTED_MODULE_6__user_management_user_management_component__["a" /* UserManagementComponent */], __WEBPACK_IMPORTED_MODULE_7__esp_management_esp_management_component__["a" /* EspManagementComponent */], __WEBPACK_IMPORTED_MODULE_8__esp_api_log_esp_api_log_component__["a" /* EspApiLogComponent */], __WEBPACK_IMPORTED_MODULE_14__shared_validator_password_validator__["a" /* PasswordValidator */]],
            providers: [__WEBPACK_IMPORTED_MODULE_13_primeng_primeng__["ConfirmationService"], __WEBPACK_IMPORTED_MODULE_9__data_management_data_management_service__["a" /* DataManagementService */], __WEBPACK_IMPORTED_MODULE_10__esp_management_esp_management_service__["a" /* EspManagementService */], __WEBPACK_IMPORTED_MODULE_11__user_management_user_management_service__["a" /* UserManagementService */], __WEBPACK_IMPORTED_MODULE_12__esp_api_log_esp_api_log_service__["a" /* EspApiLogService */], __WEBPACK_IMPORTED_MODULE_14__shared_validator_password_validator__["a" /* PasswordValidator */]]
        })
    ], AdminModule);
    return AdminModule;
}());



/***/ }),

/***/ "./src/app/admin/data-management/data-management.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/data-management/data-management.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Data Management</h2>\n<div class=\"indent min-header-margin\">\n  <!-- <h3>Raw Export</h3> -->\n  <div class=\"date-range-div\">\n    <div>\n      <div><h3>Raw Export Type (JSON)</h3></div>\n      <p-dropdown [options]=\"downloadTypes\" [(ngModel)]=\"downloadType\" placeholder=\"Select Download Type\"></p-dropdown>\n    </div>\n    <div>\n      <div><h3>Start Date</h3></div>\n      <p-calendar [(ngModel)]=\"startDate\" [readonlyInput]=\"true\" [maxDate]=\"endDate\" placeholder=\"Beginning Of Time\" dateFormat=\"dd.mm.yy\"></p-calendar>\n    </div>\n    <span>to</span>\n    <div>\n      <div><h3>End Date</h3></div>\n      <p-calendar [(ngModel)]=\"endDate\" [readonlyInput]=\"true\" [minDate]=\"startDate\" placeholder=\"End Of Time\" dateFormat=\"dd.mm.yy\"></p-calendar>\n    </div>\n    <!-- [minDate]=\"minDate\" [maxDate]=\"maxDate\"  -->\n    <button pButton [disabled]=\"!downloadType\" label=\"Export\" (click)=\"exportXML()\"></button>\n    <a id=\"downloader\" [href]=\"safeUriExport\" [download]=\"downloadType + '.json'\" style=\"display:none;\"></a>\n    <!-- <button pButton label=\"Clear\" (click)=\"clearDates()\"></button> -->\n  </div>\n  <div class=\"divSpacer\"></div>\n  <h3>CSV Upload</h3>\n  <p-dropdown [options]=\"uploadTypes\" [(ngModel)]=\"selectedType\" placeholder=\"Select Upload Type\" (onChange)=\"drowDownChange()\"></p-dropdown>\n  <!-- <div class=\"divSpacer\"></div> -->\n  <!-- <p-fileUpload [disabled]=\"!selectedType\" name=\"myfile[]\" url=\"./upload.php\"\n    accept=\".csv,.xls,.xlsx\" (onSelect)=\"fileSelect()\">\n  </p-fileUpload> -->\n\n  <button pButton type=\"button\" label=\"Download\" [disabled]=\"!active\" (click)=\"dtData.exportCSV()\"></button>\n  <div style=\"position:relative;top:7px;display:inline-block;\">\n    <p-fileUpload mode=\"basic\" name=\"file\" [url]=\"getUrl()\" accept=\".csv\" maxFileSize=\"1000000\" (onBeforeUpload)=\"onBeforeUpload($event)\"\n      (onBeforeSend)=\"onBeforeSend($event)\" (onUpload)=\"onBasicUpload($event)\" [disabled]=\"!active\" (onError)=\"onError($event)\"\n      (onUpload)=\"onUpload($event)\"></p-fileUpload>\n  </div>\n  <div class=\"divSpacer\"></div>\n  <div [ngStyle]=\"{'display': active ? 'block' : 'none' }\">\n    <div class=\"ui-widget-header\" style=\"padding:4px 10px;border-bottom: 0 none\">\n        <i class=\"fa fa-search\" style=\"margin:4px 4px 0 0\"></i>\n        <input #gb type=\"text\" pInputText size=\"50\" placeholder=\"Global Filter\">\n    </div>\n    <p-dataTable [value]=\"downloadData\" #dtData exportFilename=\"data_output\" [rows]=\"15\" [paginator]=\"true\" [globalFilter]=\"gb\">\n        <p-column *ngFor=\"let col of cols\" [field]=\"col.field\" [header]=\"col.header\"></p-column>\n    </p-dataTable>\n  </div>\n</div>\n<div class=\"divSpacer\"></div>\n"

/***/ }),

/***/ "./src/app/admin/data-management/data-management.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataManagementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_observable_of__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_delay__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/delay.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_components_fileupload_fileupload__ = __webpack_require__("./node_modules/primeng/components/fileupload/fileupload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_components_fileupload_fileupload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_components_fileupload_fileupload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_service_env_config_service__ = __webpack_require__("./src/app/shared/service/env.config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__data_management_service__ = __webpack_require__("./src/app/admin/data-management/data-management.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice__ = __webpack_require__("./node_modules/primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__auth_service__ = __webpack_require__("./src/app/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var DataManagementComponent = /** @class */ (function () {
    function DataManagementComponent(envConfig, dataManagementService, messageService, loadingService, authService, sanitizer) {
        this.envConfig = envConfig;
        this.dataManagementService = dataManagementService;
        this.messageService = messageService;
        this.loadingService = loadingService;
        this.authService = authService;
        this.sanitizer = sanitizer;
        this.active = false;
    }
    DataManagementComponent.prototype.ngOnInit = function () {
        this.endDate = new Date();
        this.startDate = new Date();
        this.startDate.setMonth(this.startDate.getMonth() - 3);
        this.colsByType =
            { tier: [
                    { field: "id", header: "id" },
                    { field: "nbrTierLevel", header: "nbrTierLevel" },
                    { field: "textApplicationTier", header: "textApplicationTier" },
                    { field: "nbrWhCoefficient", header: "nbrWhCoefficient" },
                    { field: "nbrBeneficiaryCoefficient", header: "nbrBeneficiaryCoefficient" },
                    { field: "nbrCo2Coeffiecent", header: "nbrCo2Coeffiecent" },
                    { field: "nbrLampsDisplaceCoefficient", header: "nbrLampsDisplaceCoefficient" },
                    { field: "nbrLightCoefficient", header: "nbrLightCoefficient" },
                    { field: "nbrWCoefficient", header: "nbrWCoefficient" }
                ],
                product: [
                    { field: "uiId", header: "id" },
                    { field: "extKey", header: "extKey" },
                    { field: "espCompanyName", header: "espCompanyName" },
                    { field: "nbrTierLevel", header: "nbrTierLevel" },
                    { field: "textApplicationTier", header: "textApplicationTier" },
                    { field: "productType", header: "productType" },
                    { field: "nameProduct", header: "nameProduct" },
                    { field: "nbrWhMin", header: "nbrWhMin" },
                    { field: "nbrWhMax", header: "nbrWhMax" },
                    { field: "nbrLights", header: "nbrLights" },
                    { field: "nbrWtPeak", header: "nbrWtPeak" }
                ],
                "esp-target": [
                    { field: "uiId", header: "id" },
                    { field: "espCompanyName", header: "espCompanyName" },
                    { field: "nbrTierLevel", header: "nbrTierLevel" },
                    { field: "textApplicationTier", header: "textApplicationTier" },
                    { field: "textYrQtr", header: "textYrQtr" },
                    { field: "nbrEssTarget", header: "nbrEssTarget" },
                    { field: "nbrQtrDistribution", header: "nbrQtrDistribution" }
                ],
                "financial-leverage": [
                    { field: "uiId", header: "id" },
                    { field: "espCompanyName", header: "espCompanyName" },
                    { field: "financeType", header: "financeType" },
                    { field: "amtFunding", header: "amtFunding" },
                    { field: "dtFunding", header: "dtFunding" }
                ],
                jobs: [
                    { field: "id", hdeader: "id" },
                    { field: "textJobType", hdeader: "textJobType" },
                    { field: "nbrJobsCreated", hdeader: "nbrJobsCreated" }
                ],
                "revenue-customer-class": [
                    { field: "id", header: "id" },
                    { field: "customerClass", header: "customerClass" },
                    { field: "nbrRevenueMin", header: "nbrRevenueMin" },
                    { field: "nbrRevenueMax", header: "nbrRevenueMax" },
                    { field: "nbrPlannedCoefficient", header: "nbrPlannedCoefficient" }
                ]
            };
        this.uploadTypes = [
            { label: 'ESS Schedule', value: 'esp-target' },
            { label: 'Financial Leverage', value: 'financial-leverage' },
            { label: 'Jobs', value: 'jobs' },
            { label: 'Product Types', value: 'product' },
            { label: 'Revenue Customer Class', value: 'revenue-customer-class' },
            { label: 'Tiers', value: 'tier' }
        ];
        this.downloadTypes = [
            { label: 'Event Data', value: 'event' },
            { label: 'Microgrid Data', value: 'microgrid' },
            { label: 'Microgrid ESS Data', value: 'microgrid-ess' },
            { label: 'Party Data', value: 'party' },
            { label: 'PAYG Data', value: 'payg' },
            { label: 'SHS ESS Data', value: 'shs-ess' },
            { label: 'Wh Transaction Data', value: 'wh-transaction' }
        ];
    };
    DataManagementComponent.prototype.fileSelect = function () {
        console.log('File Select');
    };
    DataManagementComponent.prototype.drowDownChange = function () {
        this.active = true;
        this.fileUploader.clear();
        this.getSelectedTableData();
    };
    DataManagementComponent.prototype.getSelectedTableData = function () {
        var _this = this;
        this.dataManagementService.getTableData(this.selectedType).subscribe(function (data) {
            _this.cols = _this.colsByType[_this.selectedType];
            _this.downloadData = data;
        }, function (error) {
            console.log(error);
            _this.authService.logout();
        });
    };
    DataManagementComponent.prototype.onBeforeUpload = function (e) {
        //Callback to invoke before file upload begins to customize the request such as post parameters before the files.
        this.loadingService.show();
    };
    DataManagementComponent.prototype.onBeforeSend = function (e) {
        e.xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem('access_token'));
        //Callback to invoke before file send begins to customize the request such as adding headers.
    };
    DataManagementComponent.prototype.onBasicUpload = function (e) {
        console.log('Basic Upload:', e);
    };
    DataManagementComponent.prototype.onUpload = function (e) {
        var res = e['xhr']['response'];
        console.log(e);
        console.log(res.payload);
        this.getSelectedTableData();
        this.loadingService.hide();
        this.messageService.add({ severity: 'success', summary: 'Upload Successful', detail: 'Success' });
    };
    DataManagementComponent.prototype.onError = function (e) {
        console.log(JSON.parse(e['xhr']['response'])['message']);
        this.loadingService.hide();
        this.messageService.add({ severity: 'error', summary: 'Upload Failed', detail: JSON.parse(e['xhr']['response'])['message'] });
    };
    DataManagementComponent.prototype.getUrl = function () {
        return this.envConfig.getEnvVariable('endPoint') + this.selectedType + '/upload';
    };
    DataManagementComponent.prototype.exportXML = function () {
        var _this = this;
        this.loadingService.show();
        this.dataManagementService.getDataForXML(this.downloadType, this.startDate, this.endDate).subscribe(function (data) {
            var blob = new Blob([JSON.stringify(data)], { type: 'text/JSON' });
            var url = window.URL.createObjectURL(blob);
            var uri = _this.sanitizer.bypassSecurityTrustUrl(url);
            _this.safeUriExport = uri;
            var downloader = document.getElementById('downloader');
            _this.loadingService.hide();
            setTimeout(function () {
                downloader.click();
            }, 100);
        });
    };
    DataManagementComponent.prototype.clearDates = function () {
        this.startDate = undefined;
        this.endDate = undefined;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4_primeng_components_fileupload_fileupload__["FileUpload"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_primeng_components_fileupload_fileupload__["FileUpload"])
    ], DataManagementComponent.prototype, "fileUploader", void 0);
    DataManagementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-data-management',
            template: __webpack_require__("./src/app/admin/data-management/data-management.component.html"),
            styles: [__webpack_require__("./src/app/admin/data-management/data-management.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__shared_service_env_config_service__["a" /* EnvConfig */], __WEBPACK_IMPORTED_MODULE_7__data_management_service__["a" /* DataManagementService */], __WEBPACK_IMPORTED_MODULE_8_primeng_components_common_messageservice__["MessageService"],
            __WEBPACK_IMPORTED_MODULE_6__shared_service_loading_services__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_9__auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__["DomSanitizer"]])
    ], DataManagementComponent);
    return DataManagementComponent;
}());



/***/ }),

/***/ "./src/app/admin/data-management/data-management.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataManagementService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_env_config_service__ = __webpack_require__("./src/app/shared/service/env.config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DataManagementService = /** @class */ (function () {
    function DataManagementService(envConfig, http) {
        var _this = this;
        this.envConfig = envConfig;
        this.http = http;
        this.getTableData = function (uploadType) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + uploadType, options)
                .map(function (res) { return res.json().payload; });
        };
        this.getDataForXML = function (downloadType, startDate, endDate) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            var strStartDate = startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate();
            var strEndDate = endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate();
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'xml-export/' + downloadType + "?start-date=" + strStartDate + "&end-date=" + strEndDate, options)
                .map(function (res) { return res.json().payload; });
        };
    }
    DataManagementService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__shared_service_env_config_service__["a" /* EnvConfig */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], DataManagementService);
    return DataManagementService;
}());



/***/ }),

/***/ "./src/app/admin/esp-api-log/esp-api-log.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/esp-api-log/esp-api-log.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Energy Service Provider API Log</h2>\n<p-dataTable [value]=\"espApiLog\">\n  <p-column field=\"idEsp\" header=\"ESP ID\" sortable=\"true\"></p-column>\n  <p-column field=\"espCompanyName\" header=\"ESP Name\" sortable=\"true\"></p-column>\n  <p-column field=\"cdResult\" header=\"Result\" sortable=\"true\"></p-column>\n  <p-column field=\"loggedDate\" header=\"Date Logged\" sortable=\"true\">\n    <ng-template let-col let-esp=\"rowData\" pTemplate=\"body\">\n      <span>{{esp[col.field] | date:'dd/MM/yyyy'}}</span>\n    </ng-template>\n  </p-column>\n  <p-column field=\"nbrRecordCount\" header=\"Record Count\" sortable=\"true\">\n    <ng-template let-esp=\"rowData\" pTemplate=\"body\">\n        <span>{{esp.nbrRecordCount}}</span>\n    </ng-template>\n  </p-column>\n  <p-column field=\"textFailDescription\" header=\"Fail Description\" sortable=\"true\"></p-column>\n</p-dataTable>\n"

/***/ }),

/***/ "./src/app/admin/esp-api-log/esp-api-log.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EspApiLogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__esp_api_log_service__ = __webpack_require__("./src/app/admin/esp-api-log/esp-api-log.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_components_common_messageservice__ = __webpack_require__("./node_modules/primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_components_common_messageservice__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EspApiLogComponent = /** @class */ (function () {
    function EspApiLogComponent(espApiLogService, messageService) {
        this.espApiLogService = espApiLogService;
        this.messageService = messageService;
    }
    EspApiLogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.espApiLogService.getEspApiLog().subscribe(function (data) {
            _this.espApiLog = data;
        }, function (error) {
            _this.messageService.add({ severity: 'error', summary: 'Failed to fetch ESP API Log', detail: error });
        });
    };
    EspApiLogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-esp-api-log',
            template: __webpack_require__("./src/app/admin/esp-api-log/esp-api-log.component.html"),
            styles: [__webpack_require__("./src/app/admin/esp-api-log/esp-api-log.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__esp_api_log_service__["a" /* EspApiLogService */], __WEBPACK_IMPORTED_MODULE_2_primeng_components_common_messageservice__["MessageService"]])
    ], EspApiLogComponent);
    return EspApiLogComponent;
}());



/***/ }),

/***/ "./src/app/admin/esp-api-log/esp-api-log.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EspApiLogService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_env_config_service__ = __webpack_require__("./src/app/shared/service/env.config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EspApiLogService = /** @class */ (function () {
    function EspApiLogService(envConfig, http) {
        var _this = this;
        this.envConfig = envConfig;
        this.http = http;
        this.getEspApiLog = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'esp-api-log', options)
                .map(function (res) { return res.json().payload; });
        };
    }
    EspApiLogService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__shared_service_env_config_service__["a" /* EnvConfig */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], EspApiLogService);
    return EspApiLogService;
}());



/***/ }),

/***/ "./src/app/admin/esp-management/esp-management.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/esp-management/esp-management.component.html":
/***/ (function(module, exports) {

module.exports = "<h3>Energy Service Provider (ESP) Management</h3>\n<button pButton type=\"button\" label=\"Add ESP\" (click)=\"displayAddEsp()\"></button>\n<div class=\"divSmallSpacer\"></div>\n<p-dataTable *ngIf=\"visible\" [value]=\"esps\">\n  <p-column header=\"Edit\" [style]=\"{'width':'45px'}\">\n      <ng-template let-esp=\"rowData\" pTemplate=\"body\">\n          <button type=\"button\" pButton (click)=\"editEsp(esp)\" icon=\"fa-pencil-square-o\"></button>\n      </ng-template>\n  </p-column>\n  <p-column header=\"Delete\" [style]=\"{'width':'50px'}\">\n      <ng-template let-esp=\"rowData\" pTemplate=\"body\">\n          <button type=\"button\" pButton (click)=\"deleteEsp(esp)\" icon=\"fa-trash\"></button>\n      </ng-template>\n  </p-column>\n  <p-column field=\"id\" header=\"ESP ID\" sortable=\"true\"></p-column>\n  <p-column field=\"nameCompany\" header=\"ESP Name\" sortable=\"true\"></p-column>\n  <p-column field=\"textApiToken\" header=\"API Token\" sortable=\"true\"></p-column>\n  <p-column field=\"textNotificationEmail\" header=\"Notification Email(s)\" sortable=\"true\"></p-column>\n  <p-column field=\"isActive\" header=\"Active\" sortable=\"true\">\n    <ng-template let-esp=\"rowData\" pTemplate=\"body\">\n        <span>{{(esp.isActive) ? 'Yes' : 'No'}}</span>\n    </ng-template>\n  </p-column>\n  <p-column field=\"nameCompany\" header=\"Recent Pass\" sortable=\"true\">\n    <ng-template let-col let-esp=\"rowData\" pTemplate=\"body\">\n      <span>{{getMostRecentPass(esp[col.field]) | date:'dd/MM/yyyy'}}</span>\n    </ng-template>\n  </p-column>\n  <p-column field=\"nameCompany\" header=\"Recent Fail\" sortable=\"true\">\n    <ng-template let-col let-esp=\"rowData\" pTemplate=\"body\">\n      <span>{{getMostRecentFail(esp[col.field]) | date:'dd/MM/yyyy'}}</span>\n    </ng-template>\n  </p-column>\n</p-dataTable>\n<p-dialog header=\"Edit ESP\" [(visible)]=\"displayEdit\" modal=\"modal\" width=\"600\">\n  <div *ngIf=\"selectedEsp\">\n    <form (ngSubmit)=\"onSubmitEdits()\" #editForm=\"ngForm\">\n      <div class=\"form-group\">\n        <label>ESP ID</label>\n        <div style=\"display:inline-block;width:calc(60% - 5px);\">\n          <span>{{selectedEsp.extKey}}</span>\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"nameCompany\">Name</label>\n        <input type=\"text\" class=\"form-control\" id=\"nameCompany\" required\n              [(ngModel)]=\"selectedEsp.nameCompany\" name=\"nameCompany\" #nameCompany=\"ngModel\">\n        <div [hidden]=\"nameCompany.valid || nameCompany.pristine\" class=\"alert alert-danger\">\n          Name is required\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label>Token</label>\n        <div style=\"display:inline-block;width:calc(60% - 5px);\">\n          <span>{{selectedEsp.textApiToken}}</span>\n          <button pButton type=\"button\" label=\"Regenerate\" style=\"float:right;\" (click)=\"regenerateApiToken()\"></button>\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"textNotificationEmail\">Notification Email(s)</label>\n        <input type=\"text\" class=\"form-control\" id=\"textNotificationEmail\" required pattern=\"^(([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+)*$\"\n              [(ngModel)]=\"selectedEsp.textNotificationEmail\" name=\"textNotificationEmail\" #textNotificationEmail=\"ngModel\">\n        <div [hidden]=\"textNotificationEmail.valid || textNotificationEmail.pristine\" class=\"alert alert-danger\">\n          Notification Email(s) is required. Please use semicolon to delimite multiple emails.\n        </div>\n      </div>\n      <button pButton type=\"submit\" label=\"Save\" [disabled]=\"!editForm.form.valid\"></button>\n      <button *ngIf=\"selectedEsp.isActive\" pButton type=\"button\" label=\"Deactivate\" (click)=\"deactivateEsp()\"></button>\n      <button *ngIf=\"!selectedEsp.isActive\" pButton type=\"button\" label=\"Activate\" (click)=\"deactivateEsp()\"></button>\n    </form>\n  </div>\n</p-dialog>\n<p-dialog header=\"Add ESP\" [(visible)]=\"displayAdd\" modal=\"modal\" width=\"600\">\n  <div *ngIf=\"newEsp\">\n    <form (ngSubmit)=\"onSubmitNew()\" #addForm=\"ngForm\">\n      <div class=\"form-group\">\n        <label for=\"nameCompany\">Name</label>\n        <input type=\"text\" class=\"form-control\" id=\"nameCompany\" required\n              [(ngModel)]=\"newEsp.nameCompany\" name=\"nameCompany\" #nameCompany=\"ngModel\">\n        <div [hidden]=\"nameCompany.valid || nameCompany.pristine\" class=\"alert alert-danger\">\n          Name is required\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"textNotificationEmail\">Notification Email(s)</label>\n        <input type=\"text\" class=\"form-control\" id=\"textNotificationEmail\" required pattern=\"^(([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+)*$\"\n              [(ngModel)]=\"newEsp.textNotificationEmail\" name=\"textNotificationEmail\" #textNotificationEmail=\"ngModel\">\n        <div [hidden]=\"textNotificationEmail.valid || textNotificationEmail.pristine\" class=\"alert alert-danger\">\n          Notification Email(s) is required. Please use semicolon to delimite multiple emails.\n        </div>\n      </div>\n      <button pButton type=\"submit\" label=\"Add\" [disabled]=\"!addForm.form.valid\"></button>\n      <button pButton type=\"button\" label=\"Cancel\" (click)=\"cancelEsp()\"></button>\n    </form>\n  </div>\n</p-dialog>\n<p-confirmDialog></p-confirmDialog>\n"

/***/ }),

/***/ "./src/app/admin/esp-management/esp-management.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EspManagementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("./node_modules/primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__esp_management_service__ = __webpack_require__("./src/app/admin/esp-management/esp-management.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__esp_api_log_esp_api_log_service__ = __webpack_require__("./src/app/admin/esp-api-log/esp-api-log.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__ = __webpack_require__("./node_modules/primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_service_filter_service__ = __webpack_require__("./src/app/shared/service/filter.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var EspManagementComponent = /** @class */ (function () {
    function EspManagementComponent(confirmationService, espManagmentService, messageService, espApiLogService, filterService) {
        this.confirmationService = confirmationService;
        this.espManagmentService = espManagmentService;
        this.messageService = messageService;
        this.espApiLogService = espApiLogService;
        this.filterService = filterService;
        this.displayAdd = false;
        this.displayEdit = false;
        this.newEsp = { id: null, textApiToken: "1", isActive: true };
        this.visible = true;
    }
    EspManagementComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.espManagmentService.getAllEsp().subscribe(function (data) {
            _this.esps = data;
        }, function (error) {
            console.log(error);
        });
        this.getEspApiLogData();
    };
    EspManagementComponent.prototype.getEspApiLogData = function () {
        var _this = this;
        this.espApiLogService.getEspApiLog().subscribe(function (data) {
            _this.espApiLog = data;
        }, function (error) {
            console.log(error);
        });
    };
    EspManagementComponent.prototype.getMostRecentPass = function (espName) {
        var espApiRecords = __WEBPACK_IMPORTED_MODULE_1_underscore__["where"](this.espApiLog, { espCompanyName: espName, cdResult: 'Pass' });
        if (espApiRecords.length > 0) {
            return __WEBPACK_IMPORTED_MODULE_1_underscore__["max"](__WEBPACK_IMPORTED_MODULE_1_underscore__["pluck"](espApiRecords, 'loggedDate'));
        }
        else {
            return null;
        }
    };
    EspManagementComponent.prototype.getMostRecentFail = function (espName) {
        var espApiRecords = __WEBPACK_IMPORTED_MODULE_1_underscore__["where"](this.espApiLog, { espCompanyName: espName, cdResult: 'Fail' });
        if (espApiRecords.length > 0) {
            return __WEBPACK_IMPORTED_MODULE_1_underscore__["max"](__WEBPACK_IMPORTED_MODULE_1_underscore__["pluck"](espApiRecords, 'loggedDate'));
        }
        else {
            return null;
        }
    };
    //***Create Esp Functionality***
    EspManagementComponent.prototype.displayAddEsp = function () {
        this.displayAdd = true;
    };
    EspManagementComponent.prototype.onSubmitNew = function () {
        var _this = this;
        this.displayAdd = false;
        this.espManagmentService.createEsp(this.newEsp).subscribe(function (esp) {
            _this.esps.push(esp);
            _this.newEsp = { id: null, textApiToken: "1", isActive: true };
            _this.updateVisibility();
            _this.messageService.add({ severity: 'success', summary: 'Add Was Successful', detail: 'Successfully added ' + esp.nameCompany });
            _this.filterService.setEsps();
        }, function (error) {
            _this.messageService.add({ severity: 'error', summary: 'Add Esp Failed', detail: error });
        });
    };
    EspManagementComponent.prototype.cancelEsp = function () {
        this.newEsp = { id: null, textApiToken: "1", isActive: true };
        this.displayAdd = false;
    };
    //***Edit Esp Functionality***
    EspManagementComponent.prototype.editEsp = function (esp) {
        this.selectedEspIndex = this.esps.indexOf(esp);
        this.selectedEsp = Object.assign({}, esp);
        this.displayEdit = true;
    };
    EspManagementComponent.prototype.deactivateEsp = function () {
        this.selectedEsp.isActive = !this.selectedEsp.isActive;
    };
    EspManagementComponent.prototype.regenerateApiToken = function () {
        var _this = this;
        this.espManagmentService.generateApiToken().subscribe(function (token) {
            _this.selectedEsp.textApiToken = token;
        }, function (error) {
            console.log(error);
            _this.messageService.add({ severity: 'error', summary: 'Token Generation Failed', detail: error });
        });
    };
    EspManagementComponent.prototype.onSubmitEdits = function () {
        var _this = this;
        this.espManagmentService.saveEsp(this.selectedEsp).subscribe(function (esp) {
            _this.filterService.setEsps();
            _this.esps[_this.selectedEspIndex] = esp;
            _this.updateVisibility();
            _this.displayEdit = false;
            _this.messageService.add({ severity: 'success', summary: 'Update Was Successful', detail: 'Successfully updated ' + esp.nameCompany });
        }, function (error) {
            _this.messageService.add({ severity: 'error', summary: 'Upload Failed', detail: error });
        });
    };
    //***Delete Esp Functionality***
    EspManagementComponent.prototype.deleteEsp = function (esp) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.espManagmentService.deleteEsp(esp).subscribe(function (res) {
                    _this.filterService.setEsps();
                    _this.messageService.add({ severity: 'success', summary: 'Delete Was Successful', detail: 'Successfully deleted ' + esp.nameCompany });
                }, function (error) {
                    _this.messageService.add({ severity: 'error', summary: 'Upload Failed', detail: error });
                });
                if (__WEBPACK_IMPORTED_MODULE_1_underscore__["contains"](_this.esps, esp)) {
                    var index = _this.esps.indexOf(esp);
                    _this.esps.splice(index, 1);
                    _this.updateVisibility();
                }
            },
            reject: function () {
            }
        });
        console.log(this.esps);
    };
    //PrimeNG was having issue with Updating table content. Use this to refresh table
    EspManagementComponent.prototype.updateVisibility = function () {
        var _this = this;
        this.visible = false;
        setTimeout(function () { return _this.visible = true; }, 0);
    };
    EspManagementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-esp-management',
            template: __webpack_require__("./src/app/admin/esp-management/esp-management.component.html"),
            styles: [__webpack_require__("./src/app/admin/esp-management/esp-management.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"], __WEBPACK_IMPORTED_MODULE_3__esp_management_service__["a" /* EspManagementService */],
            __WEBPACK_IMPORTED_MODULE_5_primeng_components_common_messageservice__["MessageService"], __WEBPACK_IMPORTED_MODULE_4__esp_api_log_esp_api_log_service__["a" /* EspApiLogService */], __WEBPACK_IMPORTED_MODULE_6__shared_service_filter_service__["a" /* FilterService */]])
    ], EspManagementComponent);
    return EspManagementComponent;
}());



/***/ }),

/***/ "./src/app/admin/esp-management/esp-management.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EspManagementService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_env_config_service__ = __webpack_require__("./src/app/shared/service/env.config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EspManagementService = /** @class */ (function () {
    function EspManagementService(envConfig, http) {
        var _this = this;
        this.envConfig = envConfig;
        this.http = http;
        this.getAllEsp = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'esp', options)
                .map(function (res) { return res.json().payload; });
        };
        this.saveEsp = function (esp) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.put(_this.envConfig.getEnvVariable('endPoint') + 'esp', esp, options)
                .map(function (res) { return res.json().payload; });
        };
        this.createEsp = function (esp) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.post(_this.envConfig.getEnvVariable('endPoint') + 'esp', esp, options)
                .map(function (res) { return res.json().payload; });
        };
        this.generateApiToken = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'esp/api-token', options)
                .map(function (res) { return res.json().payload; });
        };
        this.deleteEsp = function (esp) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.delete(_this.envConfig.getEnvVariable('endPoint') + 'esp/' + esp.id, options)
                .map(function (res) { return res.json().payload; });
        };
    }
    EspManagementService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__shared_service_env_config_service__["a" /* EnvConfig */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], EspManagementService);
    return EspManagementService;
}());



/***/ }),

/***/ "./src/app/admin/user-management/user-management.component.css":
/***/ (function(module, exports) {

module.exports = "\n"

/***/ }),

/***/ "./src/app/admin/user-management/user-management.component.html":
/***/ (function(module, exports) {

module.exports = "<h3>User Management</h3>\n<button pButton type=\"button\" label=\"Add User\" (click)=\"displayAddUser()\"></button>\n<div class=\"divSmallSpacer\"></div>\n<p-dataTable *ngIf=\"visible\" [value]=\"users\">\n  <p-column header=\"Edit\" [style]=\"{'width':'45px'}\">\n      <ng-template let-user=\"rowData\" pTemplate=\"body\">\n          <button type=\"button\" pButton (click)=\"editUser(user)\" icon=\"fa-pencil-square-o\"></button>\n      </ng-template>\n  </p-column>\n  <p-column header=\"Delete\" [style]=\"{'width':'50px'}\">\n      <ng-template let-user=\"rowData\" pTemplate=\"body\">\n          <button type=\"button\" pButton (click)=\"deleteUser(user)\" icon=\"fa-trash\"></button>\n      </ng-template>\n  </p-column>\n  <p-column field=\"nameFirst\" header=\"First Name\" sortable=\"true\"></p-column>\n  <p-column field=\"nameLast\" header=\"Last Name\" sortable=\"true\"></p-column>\n  <p-column field=\"textEmail\" header=\"Email / User ID\" sortable=\"true\"></p-column>\n  <p-column field=\"textPhoneNumber\" header=\"Phone Number\" sortable=\"true\"></p-column>\n  <!-- <p-column field=\"hashPassword\" header=\"Password\" sortable=\"true\"></p-column> -->\n  <p-column field=\"cdRole\" header=\"Role\" sortable=\"true\" [style]=\"{'width':'60px'}\">\n    <ng-template let-user=\"rowData\" let-col pTemplate=\"body\">\n        <span [ngSwitch]=\"user[col.field]\">\n          <span *ngSwitchCase=\"'ROLE_ADMIN'\">Admin</span>\n          <span *ngSwitchCase=\"'ROLE_USER'\">Internal</span>\n          <span *ngSwitchDefault>Other</span>\n        </span>\n    </ng-template>\n  </p-column>\n  <p-column field=\"isActive\" header=\"Active\" sortable=\"true\" [style]=\"{'width':'70px'}\">\n    <ng-template let-user=\"rowData\" pTemplate=\"body\">\n        <span>{{(user.isActive) ? 'Yes' : 'No'}}</span>\n    </ng-template>\n  </p-column>\n</p-dataTable>\n<p-dialog header=\"Edit User\" [(visible)]=\"displayEdit\" modal=\"modal\" width=\"500\">\n  <div *ngIf=\"selectedUser && displayEdit\">\n    <form (ngSubmit)=\"onSubmitEdits()\" #editForm=\"ngForm\">\n      <div class=\"form-group\">\n        <label for=\"nameFirst\">First Name</label>\n        <input type=\"text\" class=\"form-control\" id=\"nameFirst\" required\n              [(ngModel)]=\"selectedUser.nameFirst\" name=\"nameFirst\" #nameFirst=\"ngModel\">\n        <div [hidden]=\"nameFirst.valid || nameFirst.pristine\" class=\"alert alert-danger\">\n          First Name is required\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"nameLast\">Last Name</label>\n        <input type=\"text\" class=\"form-control\" id=\"nameLast\" required\n              [(ngModel)]=\"selectedUser.nameLast\" name=\"nameLast\" #nameLast=\"ngModel\">\n        <div [hidden]=\"nameLast.valid || nameLast.pristine\" class=\"alert alert-danger\">\n          Last Name is required\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"textEmail\">Email / User ID</label>\n        <input type=\"text\" class=\"form-control\" id=\"textEmail\" required pattern=\"^([a-zA-Z0-9_.]{1,})+@+([a-zA-Z]{1,})+\\.+([a-zA-Z]{2,})$\"\n              [(ngModel)]=\"selectedUser.textEmail\" name=\"textEmail\" #textEmail=\"ngModel\">\n        <div [hidden]=\"!textEmail.hasError('required') || textEmail.pristine\" class=\"alert alert-danger\">Email is required</div>\n        <div [hidden]=\"!textEmail.hasError('pattern') || textEmail.pristine\" class=\"alert alert-danger\">Email format should be <small><b>joe@abc.com</b></small></div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"textPhoneNumber\">Phone Number</label>\n        <input type=\"text\" class=\"form-control\" id=\"textPhoneNumber\"\n              [(ngModel)]=\"selectedUser.textPhoneNumber\" name=\"textPhoneNumber\" #textPhoneNumber=\"ngModel\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"cdRole\">Role</label>\n        <p-dropdown [options]=\"roles\" id=\"cdRole\" required [style]=\"{'width':'150px'}\"\n              [(ngModel)]=\"selectedUser.cdRole\" name=\"cdRole\" #cdRole=\"ngModel\"></p-dropdown>\n        <div [hidden]=\"cdRole.valid || cdRole.pristine\" class=\"alert alert-danger\">\n          Role is required\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"hashPassword\">Password</label>\n        <input type=\"password\" class=\"form-control\" id=\"hashPassword\" required minlength=\"8\" validatePassword\n              [(ngModel)]=\"selectedUser.hashPassword\" name=\"hashPassword\" #hashPassword=\"ngModel\">\n          <div [hidden]=\"!hashPassword.hasError('required') || hashPassword.pristine\" class=\"alert alert-danger\">Password is required</div>\n          <div [hidden]=\"!hashPassword.hasError('validatePassword') || hashPassword.pristine\" class=\"alert alert-danger\">Password must have an uppercase, lowercase, numeric, and special character</div>\n          <div [hidden]=\"!hashPassword.hasError('minlength') || hashPassword.pristine\" class=\"alert alert-danger\">Password must be 8+ characters</div>\n      </div>\n      <div class=\"divSpacer\"></div>\n      <button pButton type=\"submit\" label=\"Save\" [disabled]=\"!editForm.form.valid\"></button>\n      <button *ngIf=\"selectedUser.isActive\" pButton type=\"button\" label=\"Deactivate\" (click)=\"deactivateUser()\"></button>\n      <button *ngIf=\"!selectedUser.isActive\" pButton type=\"button\" label=\"Activate\" (click)=\"deactivateUser()\"></button>\n    </form>\n  </div>\n</p-dialog>\n<p-dialog header=\"Add User\" [(visible)]=\"displayAdd\" modal=\"modal\" width=\"500\"  (onHide)=\"cancelUser()\">\n  <div *ngIf=\"newUser && displayAdd\">\n    <form (ngSubmit)=\"onSubmitAdd()\" #addForm=\"ngForm\">\n      <div class=\"form-group\">\n        <label for=\"nameFirst\">First Name</label>\n        <input type=\"text\" class=\"form-control\" id=\"nameFirst\" required\n              [(ngModel)]=\"newUser.nameFirst\" name=\"nameFirst\" #nameFirst=\"ngModel\">\n        <div [hidden]=\"nameFirst.valid || nameFirst.pristine\" class=\"alert alert-danger\">\n          First Name is required\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"nameLast\">Last Name</label>\n        <input type=\"text\" class=\"form-control\" id=\"nameLast\" required\n              [(ngModel)]=\"newUser.nameLast\" name=\"nameLast\" #nameLast=\"ngModel\">\n        <div [hidden]=\"nameLast.valid || nameLast.pristine\" class=\"alert alert-danger\">\n          Last Name is required\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"textEmail\">Email / User ID</label>\n        <input type=\"text\" class=\"form-control\" id=\"textEmail\" required pattern=\"^([a-zA-Z0-9_.]{1,})+@+([a-zA-Z]{1,})+\\.+([a-zA-Z]{2,})$\"\n              [(ngModel)]=\"newUser.textEmail\" name=\"textEmail\" #textEmail=\"ngModel\">\n        <div [hidden]=\"!textEmail.hasError('required') || textEmail.pristine\" class=\"alert alert-danger\">Email is required</div>\n        <div [hidden]=\"!textEmail.hasError('pattern') || textEmail.pristine\" class=\"alert alert-danger\">Email format should be <small><b>joe@abc.com</b></small></div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"textPhoneNumber\">Phone Number</label>\n        <input type=\"text\" class=\"form-control\" id=\"textPhoneNumber\"\n              [(ngModel)]=\"newUser.textPhoneNumber\" name=\"textPhoneNumber\" #textPhoneNumber=\"ngModel\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"cdRole\">Role</label>\n        <p-dropdown [options]=\"roles\" id=\"cdRole\" required [style]=\"{'width':'150px'}\"\n              [(ngModel)]=\"newUser.cdRole\" name=\"cdRole\" #cdRole=\"ngModel\"></p-dropdown>\n        <div [hidden]=\"cdRole.valid || cdRole.pristine\" class=\"alert alert-danger\">\n          Role is required\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"hashPassword\">Password</label>\n        <input type=\"password\" class=\"form-control\" id=\"hashPassword\" required minlength=\"8\" validatePassword\n              [(ngModel)]=\"newUser.hashPassword\" name=\"hashPassword\" #hashPassword=\"ngModel\">\n        <div [hidden]=\"!hashPassword.hasError('required') || hashPassword.pristine\" class=\"alert alert-danger\">Password is required</div>\n        <div [hidden]=\"!hashPassword.hasError('validatePassword') || hashPassword.pristine\" class=\"alert alert-danger\">Password must have an uppercase, lowercase, numeric, and special character</div>\n        <div [hidden]=\"!hashPassword.hasError('minlength') || hashPassword.pristine\" class=\"alert alert-danger\">Password must be 8+ characters</div>\n      </div>\n      <div class=\"divSpacer\"></div>\n      <button pButton type=\"submit\" label=\"Add\" [disabled]=\"!addForm.form.valid\"></button>\n      <button pButton type=\"button\" label=\"Cancel\" (click)=\"cancelUser()\"></button>\n    </form>\n  </div>\n</p-dialog>\n<p-confirmDialog></p-confirmDialog>\n"

/***/ }),

/***/ "./src/app/admin/user-management/user-management.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserManagementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng__ = __webpack_require__("./node_modules/primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_management_service__ = __webpack_require__("./src/app/admin/user-management/user-management.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice__ = __webpack_require__("./node_modules/primeng/components/common/messageservice.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserManagementComponent = /** @class */ (function () {
    function UserManagementComponent(confirmationService, userManagementService, messageService) {
        this.confirmationService = confirmationService;
        this.userManagementService = userManagementService;
        this.messageService = messageService;
        this.displayAdd = false;
        this.displayEdit = false;
        this.newUser = { isActive: true };
        this.visible = true;
        this.roles = [{ label: 'Select Role...', value: null }, { label: 'Admin', value: 'ROLE_ADMIN' }, { label: 'Internal', value: 'ROLE_USER' }];
        // this.roles = [{label:'Admin',value:'Admin'},{label:'Internal',value:'Internal'}];
    }
    UserManagementComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userManagementService.getAllUsers().subscribe(function (data) {
            _this.users = data;
        }, function (error) {
            console.log(error);
            _this.messageService.add({ severity: 'error', summary: 'Error Fetching Users', detail: error });
        });
    };
    //***Add User Funcationality***
    UserManagementComponent.prototype.displayAddUser = function () {
        this.displayAdd = true;
    };
    UserManagementComponent.prototype.onSubmitAdd = function () {
        var _this = this;
        console.log('Add');
        if (this.newUser.textPhoneNumber === "") {
            this.newUser.textPhoneNumber = null;
        }
        this.displayAdd = false;
        this.userManagementService.createUser(this.newUser).subscribe(function (user) {
            _this.users.push(user);
            _this.newUser = { isActive: true };
            _this.updateVisibility();
            _this.messageService.add({ severity: 'success', summary: 'Add Was Successful', detail: 'Successfully added ' + user.nameFirst + ' ' + user.nameLast });
        }, function (error) {
            console.log(error);
            _this.messageService.add({ severity: 'error', summary: 'Add User Failed', detail: error });
        });
    };
    UserManagementComponent.prototype.cancelUser = function () {
        this.newUser = { isActive: true };
        this.displayAdd = false;
    };
    //***Edit User Funcationality***
    UserManagementComponent.prototype.editUser = function (user) {
        this.selectedUserIndex = this.users.indexOf(user);
        this.selectedUser = Object.assign({}, user);
        this.displayEdit = true;
    };
    UserManagementComponent.prototype.deactivateUser = function () {
        this.selectedUser.isActive = !this.selectedUser.isActive;
    };
    UserManagementComponent.prototype.onSubmitEdits = function () {
        var _this = this;
        if (this.selectedUser.textPhoneNumber === "") {
            this.selectedUser.textPhoneNumber = null;
        }
        this.userManagementService.saveUser(this.selectedUser).subscribe(function (user) {
            _this.users[_this.selectedUserIndex] = user;
            _this.updateVisibility();
            _this.displayEdit = false;
            _this.messageService.add({ severity: 'success', summary: 'Update Was Successful', detail: 'Successfully updated ' + user.nameFirst + ' ' + user.nameLast });
        }, function (error) {
            console.log(error);
            _this.messageService.add({ severity: 'error', summary: 'Update Failed', detail: error });
        });
    };
    //***Delete User Funcationality***
    UserManagementComponent.prototype.deleteUser = function (user) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.userManagementService.deleteUser(user).subscribe(function (res) {
                    _this.messageService.add({ severity: 'success', summary: 'Delete Was Successful', detail: 'Successfully deleted ' + user.nameFirst + ' ' + user.nameLast });
                }, function (error) {
                    console.log(error);
                    _this.messageService.add({ severity: 'error', summary: 'Upload Failed', detail: error });
                });
                if (__WEBPACK_IMPORTED_MODULE_1_underscore__["contains"](_this.users, user)) {
                    var index = _this.users.indexOf(user);
                    _this.users.splice(index, 1);
                    _this.updateVisibility();
                }
            },
            reject: function () {
            }
        });
        console.log(this.users);
    };
    //PrimeNG was having issue with Updating table content. Use this to refresh table
    UserManagementComponent.prototype.updateVisibility = function () {
        var _this = this;
        this.visible = false;
        setTimeout(function () { return _this.visible = true; }, 0);
    };
    UserManagementComponent.prototype.passwordPattern = function (pwd) {
        var valid = true;
        var ucase = false;
        var lcase = false;
        var special = false;
        if ((/[a-z]/.test(pwd))) {
            console.log('lower');
            valid = false;
        }
        if ((/[A-Z]/.test(pwd))) {
            console.log('upper');
            ucase = true;
        }
        if ((/[0-9]/.test(pwd))) {
            console.log('number');
            lcase = true;
        }
        if ((/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(pwd))) {
            console.log('Special Char');
            special = true;
        }
        if (pwd.indexOf(' ') >= 0) {
            console.log('Space');
            valid = false;
        }
        if (valid && ucase && lcase && special) {
            return null;
        }
        else {
            return { validatePassword: { valid: false } };
        }
    };
    UserManagementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-user-management',
            template: __webpack_require__("./src/app/admin/user-management/user-management.component.html"),
            styles: [__webpack_require__("./src/app/admin/user-management/user-management.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_primeng_primeng__["ConfirmationService"], __WEBPACK_IMPORTED_MODULE_3__user_management_service__["a" /* UserManagementService */], __WEBPACK_IMPORTED_MODULE_4_primeng_components_common_messageservice__["MessageService"]])
    ], UserManagementComponent);
    return UserManagementComponent;
}());



/***/ }),

/***/ "./src/app/admin/user-management/user-management.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserManagementService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_env_config_service__ = __webpack_require__("./src/app/shared/service/env.config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserManagementService = /** @class */ (function () {
    function UserManagementService(envConfig, http) {
        var _this = this;
        this.envConfig = envConfig;
        this.http = http;
        this.getAllUsers = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            console.log(_this.envConfig.getEnvVariable('endPoint') + 'user');
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'user', options)
                .map(function (res) { return res.json().payload; });
        };
        this.saveUser = function (user) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.put(_this.envConfig.getEnvVariable('endPoint') + 'user', user, options)
                .map(function (res) { return res.json().payload; });
        };
        this.createUser = function (user) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            console.log(user);
            return _this.http.post(_this.envConfig.getEnvVariable('endPoint') + 'user', user, options)
                .map(function (res) { return res.json().payload; });
        };
        this.deleteUser = function (user) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.delete(_this.envConfig.getEnvVariable('endPoint') + 'user/' + user.id, options)
                .map(function (res) { return res.json().payload; });
        };
    }
    UserManagementService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__shared_service_env_config_service__["a" /* EnvConfig */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], UserManagementService);
    return UserManagementService;
}());



/***/ }),

/***/ "./src/app/shared/validator/password.validator.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordValidator; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PasswordValidator = /** @class */ (function () {
    function PasswordValidator() {
    }
    PasswordValidator_1 = PasswordValidator;
    PasswordValidator.prototype.validate = function (c) {
        return this.validator(c.value, c.pristine);
    };
    PasswordValidator.prototype.validator = function (pwd, pristine) {
        var valid = true;
        var ucase = false;
        var lcase = false;
        var hasnumber = false;
        var special = false;
        if (pwd) {
            if ((/[a-z]/.test(pwd))) {
                lcase = true;
            }
            if ((/[A-Z]/.test(pwd))) {
                ucase = true;
            }
            if ((/[0-9]/.test(pwd))) {
                hasnumber = true;
            }
            if ((/[^A-Za-z0-9 ]/g.test(pwd))) {
                special = true;
            }
            if (pwd.indexOf(' ') >= 0) {
                valid = false;
            }
        }
        //console.log('Upper: ',ucase,' Lower: ',lcase,' Number: ',hasnumber,' Special: ',special,' Valid: ', valid);
        if ((valid && ucase && lcase && special && hasnumber) || pristine) {
            return null;
        }
        else {
            return { validatePassword: { valid: false } };
        }
    };
    PasswordValidator = PasswordValidator_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[validatePassword][ngModel],[validatePassword][formControl]',
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["NG_VALIDATORS"], useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["forwardRef"])(function () { return PasswordValidator_1; }), multi: true }
            ]
        }),
        __metadata("design:paramtypes", [])
    ], PasswordValidator);
    return PasswordValidator;
    var PasswordValidator_1;
}());



/***/ })

});
//# sourceMappingURL=admin.module.chunk.js.map