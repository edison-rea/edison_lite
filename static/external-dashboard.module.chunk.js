webpackJsonp(["external-dashboard.module"],{

/***/ "./src/app/external-dashboard/climate/climate.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/external-dashboard/climate/climate.component.html":
/***/ (function(module, exports) {

module.exports = "<edison-subDashboard *ngIf=\"plotlyData\"\n  subDashboardTitle=\"Climate: Number of kg of CO2 mitigated annually\"\n  [dataFilters]=\"dataFilters\"\n  [rawData]=\"rawData\"\n  [parentDashboard]=\"'bgfz-impact'\"\n  [plotlyData1]=\"plotlyData\"\n  [plotlyLayout1]=\"plotlyLayout\"\n  [plotlyData2]=\"plotlyData2\"\n  [plotlyLayout2]=\"plotlyLayout2\"\n  [showDataTab]=false\n  [dataDownLoad]=false\n  [chartInfo]=\"chartInfo\"\n  [chartInfoLinkExt]=\"chartInfoLink\"\n  zambiaLegendTitle=\"Number of Kg of Co2 Mitigated\"\n  (onDataFilter)=\"setChartData($event)\"\n  (onPlotlyClick)=\"onPlotlyClick($event)\">\n</edison-subDashboard>\n"

/***/ }),

/***/ "./src/app/external-dashboard/climate/climate.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClimateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__ = __webpack_require__("./src/app/shared/service/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_underscore__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ClimateComponent = /** @class */ (function () {
    //zambiaData: Array<any>;
    function ClimateComponent(router, appService, filterService, externalDashboardService, loadingService) {
        this.router = router;
        this.appService = appService;
        this.filterService = filterService;
        this.externalDashboardService = externalDashboardService;
        this.loadingService = loadingService;
        this.dataFilters = [];
        this.chartInfo = 'This chart shows the estimated annual carbon mitigation impact of ESS deployed under BGFZ. For a detailed description of the assumptions and methodologies used to calculate these figures please visit ';
        this.chartInfoLink = { route: 'https://www.reeep.org/bgfz_emissions_reductions_calculation_model', text: 'https://www.reeep.org/bgfz_emissions_reductions_calculation_model' };
    }
    ClimateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadingService.show();
        //REMOVED FOR DEMO, can add back after demo
        // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
        this.dataFilters.push(this.filterService.getTierFilter());
        //REMOVED FOR DEMO
        // this.dataFilters.push(this.filterService.getCustomerTypeFilter());
        this.externalDashboardService.getClimateData().subscribe(function (data) {
            _this.rawData = data;
            _this.setChartData(data);
            _this.loadingService.hide();
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
        this.plotlyLayout = {
            title: 'Kg of Co2 Mitigated Annually by Tier',
            showlegend: true,
            autosize: true,
            // xaxis: { title: 'Province' },
            yaxis: { title: 'Kg of Co2 Mitigated' },
            barmode: 'stack',
            hovermode: 'closest'
        };
        this.plotlyLayout2 = {
            title: 'Total by Tier',
            annotations: [
                {
                    font: {
                        size: 14
                    },
                    showarrow: false,
                    text: 'Co2',
                    x: 0.5,
                    y: 0.5
                }
            ],
            margin: { l: 10, r: 10, b: 30, t: 50 },
            showlegend: false
        };
    };
    ClimateComponent.prototype.onPlotlyClick = function (e) {
        console.log(e);
    };
    ClimateComponent.prototype.setChartData = function (data) {
        var _this = this;
        //removed for demo
        // let tiers = [1,2,3,4,5,6];
        var tiers = [1, 2, 3];
        this.plotlyData = [];
        var pieTotals = [];
        var text = [];
        var _loop_1 = function (i) {
            var tierData = __WEBPACK_IMPORTED_MODULE_6_underscore__["where"](data, { tierLevel: tiers[i] });
            var provinceTier = {
                Copperbelt: 0,
                Southern: 0,
                Eastern: 0,
                Northern: 0,
                Muchinga: 0,
                Luapula: 0,
                'North-Western': 0,
                Western: 0,
                Lusaka: 0,
                Central: 0,
                Total: 0
            };
            __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](tierData, function (row) {
                provinceTier[row['province']] += row['co2Coefficient'];
                provinceTier['Total'] += row['co2Coefficient'];
                //zambiaTotal[row['province']] += row['co2Coefficient'];
            });
            var trace = {
                x: ['Copperbelt', 'Southern', 'Eastern', 'Northern', 'Muchinga', 'Luapula', 'North-Western', 'Western', 'Lusaka', 'Central', 'Total'],
                y: [
                    provinceTier['Copperbelt'],
                    provinceTier['Southern'],
                    provinceTier['Eastern'],
                    provinceTier['Northern'],
                    provinceTier['Muchinga'],
                    provinceTier['Luapula'],
                    provinceTier['North-Western'],
                    provinceTier['Western'],
                    provinceTier['Lusaka'],
                    provinceTier['Central']
                ],
                name: 'Tier ' + tiers[i].toString(),
                type: 'bar'
            };
            pieTotals.push(provinceTier['Total']);
            text.push(provinceTier['Total'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            this_1.plotlyData.push(trace);
        };
        var this_1 = this;
        for (var i in tiers) {
            _loop_1(i);
        }
        var tierLabels = [];
        __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](tiers, function (tier) { tierLabels.push('Tier ' + tier.toString()); });
        this.plotlyData2 = [{
                values: pieTotals,
                labels: tierLabels,
                type: 'pie',
                hole: 0.3,
                text: text,
                textposition: 'inside',
                hoverinfo: 'label',
                textfont: {
                    color: 'white'
                },
                marker: {
                    colors: this.filterService.getPlotlyColors()
                }
            }];
    };
    ClimateComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-climate',
            template: __webpack_require__("./src/app/external-dashboard/climate/climate.component.html"),
            styles: [__webpack_require__("./src/app/external-dashboard/climate/climate.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__app_service__["a" /* AppService */], __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__["a" /* FilterService */],
            __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__["a" /* ExternalDashboardService */], __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__["a" /* LoadingService */]])
    ], ClimateComponent);
    return ClimateComponent;
}());



/***/ }),

/***/ "./src/app/external-dashboard/education/education.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/external-dashboard/education/education.component.html":
/***/ (function(module, exports) {

module.exports = "<edison-subDashboard *ngIf=\"plotlyData\"\n  [subDashboardTitle]=\"'Education: Number of Schools connected'\"\n  [dataFilters]=\"dataFilters\"\n  [rawData]=\"rawData\"\n  [parentDashboard]=\"'bgfz-impact'\"\n  [plotlyData1]=\"plotlyData\"\n  [plotlyLayout1]=\"plotlyLayout\"\n  [plotlyData2]=\"plotlyData2\"\n  [plotlyLayout2]=\"plotlyLayout2\"\n  [showDataTab]=false\n  [dataDownLoad]=false\n  [chartInfo]=\"chartInfo\"\n  (onDataFilter)=\"setChartData($event)\"\n  (onPlotlyClick)=\"onPlotlyClick($event)\">\n</edison-subDashboard>\n"

/***/ }),

/***/ "./src/app/external-dashboard/education/education.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EducationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__ = __webpack_require__("./src/app/shared/service/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_underscore__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var EducationComponent = /** @class */ (function () {
    function EducationComponent(router, appService, filterService, externalDashboardService, loadingService) {
        this.router = router;
        this.appService = appService;
        this.filterService = filterService;
        this.externalDashboardService = externalDashboardService;
        this.loadingService = loadingService;
        this.dataFilters = [];
        this.chartInfo = 'This chart shows the number and breakdown of schools connected to modern energy under BGFZ';
    }
    EducationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadingService.show();
        //REMOVED FOR DEMO, can add back after demo
        // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
        this.dataFilters.push(this.filterService.getTierFilter());
        this.plotlyLayout = {
            title: 'Schools Connected by Tier',
            showlegend: true,
            autosize: true,
            yaxis: { title: 'Schools Connected' },
            barmode: 'stack',
            hovermode: 'closest'
        };
        this.plotlyLayout2 = {
            title: 'Total by Tier',
            annotations: [
                {
                    font: {
                        size: 14
                    },
                    showarrow: false,
                    text: 'Schools',
                    x: 0.5,
                    y: 0.5
                }
            ],
            margin: { l: 10, r: 10, b: 30, t: 50 },
            showlegend: false
        };
        this.externalDashboardService.getSchoolData().subscribe(function (data) {
            console.log(data);
            _this.rawData = data;
            _this.setChartData(data);
            _this.loadingService.hide();
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
    };
    EducationComponent.prototype.onPlotlyClick = function (e) {
        console.log(e);
    };
    EducationComponent.prototype.setChartData = function (data) {
        var _this = this;
        //removed for demo
        // let tiers = [1,2,3,4,5,6];
        var tiers = [1, 2, 3];
        this.plotlyData = [];
        var pieTotals = [];
        var text = [];
        for (var i in tiers) {
            var tierData = __WEBPACK_IMPORTED_MODULE_6_underscore__["where"](data, { tierLevel: tiers[i] });
            var provinceTier = __WEBPACK_IMPORTED_MODULE_6_underscore__["countBy"](__WEBPACK_IMPORTED_MODULE_6_underscore__["pluck"](tierData, 'province'), function (row) { return row; });
            var trace = {
                x: ['Copperbelt', 'Southern', 'Eastern', 'Northern', 'Muchinga', 'Luapula', 'North-Western', 'Western', 'Lusaka', 'Central', 'Total'],
                y: [
                    provinceTier['Copperbelt'],
                    provinceTier['Southern'],
                    provinceTier['Eastern'],
                    provinceTier['Northern'],
                    provinceTier['Muchinga'],
                    provinceTier['Luapula'],
                    provinceTier['North-Western'],
                    provinceTier['Western'],
                    provinceTier['Lusaka'],
                    provinceTier['Central']
                ],
                name: 'Tier ' + tiers[i].toString(),
                type: 'bar'
            };
            this.plotlyData.push(trace);
            pieTotals.push(tierData.length);
            text.push(tierData.length.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }
        var tierLabels = [];
        __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](tiers, function (tier) { tierLabels.push('Tier ' + tier.toString()); });
        this.plotlyData2 = [{
                values: pieTotals,
                labels: tierLabels,
                type: 'pie',
                hole: 0.3,
                text: text,
                textposition: 'inside',
                hoverinfo: 'label',
                textfont: {
                    color: 'white'
                },
                marker: {
                    colors: this.filterService.getPlotlyColors()
                }
            }];
    };
    EducationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-education',
            template: __webpack_require__("./src/app/external-dashboard/education/education.component.html"),
            styles: [__webpack_require__("./src/app/external-dashboard/education/education.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__app_service__["a" /* AppService */], __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__["a" /* FilterService */],
            __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__["a" /* ExternalDashboardService */], __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__["a" /* LoadingService */]])
    ], EducationComponent);
    return EducationComponent;
}());



/***/ }),

/***/ "./src/app/external-dashboard/external-connections/external-connections.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/external-dashboard/external-connections/external-connections.component.html":
/***/ (function(module, exports) {

module.exports = "<edison-subDashboard *ngIf=\"plotlyData\"\n  [subDashboardTitle]=\"'Connections: Number of ESS'\"\n  [dataFilters]=\"dataFilters\"\n  [rawData]=\"rawData\"\n  [parentDashboard]=\"'bgfz-impact'\"\n  [plotlyData1]=\"plotlyData\"\n  [plotlyLayout1]=\"plotlyLayout\"\n  [plotlyData2]=\"plotlyData2\"\n  [plotlyLayout2]=\"plotlyLayout2\"\n  [showDataTab]=false\n  [dataDownLoad]=false\n  [chartInfo]=\"chartInfo\"\n  [chartInfoLink]=\"chartInfoLink\"\n  [chartInfoAfter]=\"chartInfoAfter\"\n  (onDataFilter)=\"setChartData($event)\"\n  (onPlotlyClick)=\"onPlotlyClick($event)\">\n</edison-subDashboard>\n"

/***/ }),

/***/ "./src/app/external-dashboard/external-connections/external-connections.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExternalConnectionsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__ = __webpack_require__("./src/app/shared/service/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_underscore__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ExternalConnectionsComponent = /** @class */ (function () {
    function ExternalConnectionsComponent(router, appService, filterService, externalDashboardService, loadingService) {
        this.router = router;
        this.appService = appService;
        this.filterService = filterService;
        this.externalDashboardService = externalDashboardService;
        this.loadingService = loadingService;
        this.dataFilters = [];
        this.chartInfo = 'An Energy Service Subscription (ESS) is a modern, high-quality energy connection meeting minimum criteria on quality, warranty, service and technical performance. A given ESS is allotted a “Tier” in accordance with the BGFZ Multi-Tier Matrix (see ';
        this.chartInfoLink = { route: '/info', text: 'Information' };
        this.chartInfoAfter = ' for more on the tier system)';
    }
    ExternalConnectionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadingService.show();
        this.dataFilters.push(this.filterService.getCustomerGenderFilter());
        //REMOVED FOR DEMO, can add back after demo
        // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
        this.dataFilters.push(this.filterService.getTierFilter());
        //Removed for demo
        // this.dataFilters.push(this.filterService.getCustomerTypeFilter());
        //note: this was already commented out before demo!
        //this.dataFilters.push(this.filterService.getESPFilter());
        this.plotlyLayout = {
            title: 'Connections by Tier',
            showlegend: true,
            autosize: true,
            // xaxis: { title: 'Province' },
            yaxis: { title: 'Connections' },
            barmode: 'stack',
            hovermode: 'closest'
        };
        this.plotlyLayout2 = {
            title: 'Total by Tier',
            annotations: [
                {
                    font: {
                        size: 14
                    },
                    showarrow: false,
                    text: 'ESS',
                    x: 0.5,
                    y: 0.5
                }
            ],
            margin: { l: 10, r: 10, b: 30, t: 50 },
            showlegend: false //,
            //legend: {"orientation": "h"}
        };
        this.externalDashboardService.getConnectionData().subscribe(function (data) {
            _this.rawData = data;
            _this.setChartData(_this.rawData);
            _this.loadingService.hide();
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
    };
    ExternalConnectionsComponent.prototype.onPlotlyClick = function (e) {
        console.log(e);
    };
    ExternalConnectionsComponent.prototype.setChartData = function (data) {
        var _this = this;
        //removed for demo
        // let tiers = [1,2,3,4,5,6]
        var tiers = [1, 2, 3];
        this.plotlyData = [];
        var pieTotals = [];
        var text = [];
        for (var i in tiers) {
            var tierData = __WEBPACK_IMPORTED_MODULE_6_underscore__["where"](data, { tierLevel: tiers[i] });
            var provinceTier = __WEBPACK_IMPORTED_MODULE_6_underscore__["countBy"](__WEBPACK_IMPORTED_MODULE_6_underscore__["pluck"](tierData, 'province'), function (row) { return row; });
            var trace = {
                x: ['Copperbelt', 'Southern', 'Eastern', 'Northern', 'Muchinga', 'Luapula', 'North-Western', 'Western', 'Lusaka', 'Central'],
                y: [
                    provinceTier['Copperbelt'],
                    provinceTier['Southern'],
                    provinceTier['Eastern'],
                    provinceTier['Northern'],
                    provinceTier['Muchinga'],
                    provinceTier['Luapula'],
                    provinceTier['North-Western'],
                    provinceTier['Western'],
                    provinceTier['Lusaka'],
                    provinceTier['Central']
                ],
                name: 'Tier ' + tiers[i].toString(),
                type: 'bar'
            };
            pieTotals.push(tierData.length);
            text.push(tierData.length.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            this.plotlyData.push(trace);
        }
        var tierLabels = [];
        __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](tiers, function (tier) { tierLabels.push('Tier ' + tier.toString()); });
        this.plotlyData2 = [{
                values: pieTotals,
                labels: tierLabels,
                type: 'pie',
                hole: 0.3,
                text: text,
                textposition: 'inside',
                hoverinfo: 'label',
                textfont: {
                    color: 'white'
                },
                marker: {
                    colors: this.filterService.getPlotlyColors()
                }
            }];
    };
    ExternalConnectionsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-external-connections',
            template: __webpack_require__("./src/app/external-dashboard/external-connections/external-connections.component.html"),
            styles: [__webpack_require__("./src/app/external-dashboard/external-connections/external-connections.component.css")],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__app_service__["a" /* AppService */], __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__["a" /* FilterService */],
            __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__["a" /* ExternalDashboardService */], __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__["a" /* LoadingService */]])
    ], ExternalConnectionsComponent);
    return ExternalConnectionsComponent;
}());



/***/ }),

/***/ "./src/app/external-dashboard/external-dashboard-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExternalDashboardRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__external_dashboard_component__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__external_connections_external_connections_component__ = __webpack_require__("./src/app/external-dashboard/external-connections/external-connections.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__external_gender_external_gender_component__ = __webpack_require__("./src/app/external-dashboard/external-gender/external-gender.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__light_light_component__ = __webpack_require__("./src/app/external-dashboard/light/light.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__people_people_component__ = __webpack_require__("./src/app/external-dashboard/people/people.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__jobs_jobs_component__ = __webpack_require__("./src/app/external-dashboard/jobs/jobs.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__climate_climate_component__ = __webpack_require__("./src/app/external-dashboard/climate/climate.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__financials_financials_component__ = __webpack_require__("./src/app/external-dashboard/financials/financials.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__power_power_component__ = __webpack_require__("./src/app/external-dashboard/power/power.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__education_education_component__ = __webpack_require__("./src/app/external-dashboard/education/education.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__productive_productive_component__ = __webpack_require__("./src/app/external-dashboard/productive/productive.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var appRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__external_dashboard_component__["a" /* ExternalDashboardComponent */]
    },
    {
        path: 'bgfz-impact',
        component: __WEBPACK_IMPORTED_MODULE_2__external_dashboard_component__["a" /* ExternalDashboardComponent */]
    },
    {
        path: 'connections',
        component: __WEBPACK_IMPORTED_MODULE_3__external_connections_external_connections_component__["a" /* ExternalConnectionsComponent */]
    },
    {
        path: 'gender',
        component: __WEBPACK_IMPORTED_MODULE_4__external_gender_external_gender_component__["a" /* ExternalGenderComponent */]
    },
    {
        path: 'light',
        component: __WEBPACK_IMPORTED_MODULE_5__light_light_component__["a" /* LightComponent */]
    },
    {
        path: 'people',
        component: __WEBPACK_IMPORTED_MODULE_6__people_people_component__["a" /* PeopleComponent */]
    },
    {
        path: 'jobs',
        component: __WEBPACK_IMPORTED_MODULE_7__jobs_jobs_component__["a" /* JobsComponent */]
    },
    {
        path: 'climate',
        component: __WEBPACK_IMPORTED_MODULE_8__climate_climate_component__["a" /* ClimateComponent */]
    },
    {
        path: 'financials',
        component: __WEBPACK_IMPORTED_MODULE_9__financials_financials_component__["a" /* FinancialsComponent */]
    },
    {
        path: 'power',
        component: __WEBPACK_IMPORTED_MODULE_10__power_power_component__["a" /* PowerComponent */]
    },
    {
        path: 'education',
        component: __WEBPACK_IMPORTED_MODULE_11__education_education_component__["a" /* EducationComponent */]
    },
    {
        path: 'productive',
        component: __WEBPACK_IMPORTED_MODULE_12__productive_productive_component__["a" /* ProductiveComponent */]
    }
];
var ExternalDashboardRoutingModule = /** @class */ (function () {
    function ExternalDashboardRoutingModule() {
    }
    ExternalDashboardRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"].forChild(appRoutes)
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"]
            ],
            providers: []
        }),
        __metadata("design:paramtypes", [])
    ], ExternalDashboardRoutingModule);
    return ExternalDashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/external-dashboard/external-dashboard.component.css":
/***/ (function(module, exports) {

module.exports = ".reset-text{\n  text-align:left;\n  display:none;\n  font-size:12px;\n  padding:0px 2.5px;\n}\n\n.reset-button{\n  display:block;\n  text-align: left;\n}\n\n.reset-button>button{\n  width:100px !important;\n}\n\n@media screen and (max-width: 670px){\n  .reset-button{\n    display:block;\n  }\n  .reset-text{\n    display:none;\n  }\n}\n"

/***/ }),

/***/ "./src/app/external-dashboard/external-dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div [ngStyle]=\"{'display':(showPrinterFriendly)?'none':'block'}\" style=\"position:relative;\">\n  <div class=\"divSmallSpacer\"></div>\n  <div class=\"divSpacer\"></div>\n  <div class=\"ui-grid ui-grid-responsive ui-fluid edison-dashboard\">\n    <div class=\"print-button\">\n      <button pButton type=\"button\" label=\"Print\" (click)=\"showPFModal()\"></button>\n    </div>\n    <div style=\"margin:0 auto;text-align:center;\" class=\"ui-grid-row\">\n      <div [ngClass]=\"getNgClassMap()\" class=\"external-zambia\">\n        <div *ngIf=\"chartData && rawData && authService.isAdmin\" class=\"ui-grid-col-2 vertical-filters\" style=\"vertical-align:top;text-align:left;\">\n          <h2>Filters:</h2>\n          <div class=\"indent\">\n            <div>\n              <h3>ESP</h3>\n              <div *ngFor=\"let esp of espFilter.filters\" class=\"indent\">\n                <p-checkbox name=\"esps\" [value]=\"esp\" [label]=\"esp\" [(ngModel)]=\"selectedESPs\" [inputId]=\"esp\" (onChange)=\"processFilter($event)\"></p-checkbox>\n                <div class=\"divSmallSpacer\"></div>\n              </div>\n            </div>\n            <div>\n              <h3>Time</h3>\n              <div>\n                <p-calendar [(ngModel)]=\"startDate\" [readonlyInput]=\"true\" placeholder=\"Beginning of Time\" dateFormat=\"dd.mm.yy\"\n                   showButtonBar=\"true\" (onSelect)=\"processFilter($event)\" (onClearClick)=\"processFilter($event)\"></p-calendar>\n              </div>\n              <div style=\"margin:0 auto; text-align:center; font-size:16px; font-weight:bold;\">to</div>\n              <div>\n                <p-calendar [(ngModel)]=\"endDate\" [readonlyInput]=\"true\"  placeholder=\"End of Time\" dateFormat=\"dd.mm.yy\"\n                   showButtonBar=\"true\" (onSelect)=\"processFilter($event)\" (onClearClick)=\"processFilter($event)\"></p-calendar>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div style=\"display:inline-block;\">\n          <h3 style=\"text-align:left;margin:2.5px 0px\">Selected Province: {{selectedProvince}}</h3>\n          <span class=\"reset-text\">*Double Click to Reset</span>\n          <div class=\"reset-button\">\n            <button pButton label=\"Reset\" icon=\"fa-undo\" (click)=\"provinceClicked('All')\"></button>\n          </div>\n          <!-- <app-d3 #zambiaChart id=\"d3Zambia\" *ngIf=\"chartData && rawData\" [data]=\"chartData\" [name]=\"'d3Zambia'\" [pointCoords]=\"pointCoords\" (dblclick)=\"provinceClicked('All')\" -->\n          <app-d3 #zambiaChart id=\"d3Zambia\" *ngIf=\"chartData\" [data]=\"chartData\" [name]=\"'d3Zambia'\" [pointCoords]=\"pointCoords\"></app-d3>\n        </div>\n      </div>\n      <div  [ngClass]=\"getNgClassCells()\">\n        <!-- style=\"display:inline-block;vertical-align:top;\" -->\n        <div class=\"divSpacer\"></div>\n        <table class=\"dashboard-stats\">\n          <tr>\n            <td (click)=\"router.navigate(['bfgz-impact/connections'])\" class=\"dashboard-cell\">\n              <h3>Connections</h3>\n              <h2>{{connections | number:'1.0-0'}}</h2>\n              <h5>Energy Service Subscriptions</h5>\n            </td>\n            <td (click)=\"router.navigate(['bfgz-impact/light'])\" class=\"dashboard-cell\">\n              <h3>Light Service</h3>\n              <h2>{{light | number:'1.0-0'}}</h2>\n              <h5>Candles/Lamps Displaced</h5>\n            </td>\n            <td (click)=\"router.navigate(['bfgz-impact/people'])\" class=\"dashboard-cell\">\n              <h3>People</h3>\n              <h2>{{people | number:'1.0-0'}}</h2>\n              <h5>Beneficiaries</h5>\n            </td>\n          </tr>\n          <tr>\n            <td (click)=\"router.navigate(['bfgz-impact/gender'])\" class=\"dashboard-cell\">\n              <h3>Gender</h3>\n              <h2>{{womenCustomers | number:'1.0-0'}}</h2>\n              <h5>Women Primary Customers</h5>\n            </td>\n            <td (click)=\"router.navigate(['bfgz-impact/jobs'])\" class=\"dashboard-cell\">\n              <!-- class=\"multi-dashboard-cell\" -->\n              <h3>Jobs</h3>\n              <h2 *ngIf=\"installedPower\">\n                {{ 370 + 1294 | number:'1.0-0'}}\n                <!-- {{((jobs['Contracted'] + jobs['Commission Based'])) | number:'1.0-0'}} -->\n                <!-- {{(jobs['Contracted'] / 1000) | number:'1.0-0'}}   |\n                {{(jobs['Commission Based'] / 1000) | number:'1.0-0'}} -->\n              </h2>\n              <!-- <h2 *ngIf=\"installedPower\">{{(jobs['Contracted'] / 1000) | number:'1.0-0'}}</h2>\n              <h2 *ngIf=\"installedPower\">{{(jobs['Commission Based'] / 1000) | number:'1.0-0'}}</h2> -->\n              <!-- <h2 *ngIf=\"installedPower\">{{(jobs['Contracted'] / 1000) | number:'1.0-0'}} <span>Contracted</span></h2>\n              <h2 *ngIf=\"installedPower\">{{(jobs['Commission Based'] / 1000) | number:'1.0-0'}} <span>On Commission</span></h2> -->\n              <!-- <h5>Contract | Agents</h5> -->\n              <h5>Full & Part Time Jobs</h5>\n              <!-- <h2>{{jobs | number:'1.0-0'}}</h2> -->\n              <!-- <h5>Commission-Based</h5> -->\n            </td>\n            <td (click)=\"router.navigate(['bfgz-impact/climate'])\" class=\"dashboard-cell\">\n              <h3>Climate</h3>\n              <h2>{{co2Mitigated | number:'1.0-0'}}</h2>\n              <h5>Kg of C0<sub>2</sub> Mitigated Annually</h5>\n            </td>\n          </tr>\n          <tr>\n            <td (click)=\"router.navigate(['bfgz-impact/financials'])\" class=\"dashboard-cell\">\n              <h3>Finance</h3>\n              <!-- Note: Data for Investment will arrive before rest. Checking installedPower as this data will arive with arrive with rest of data-->\n              <h2 *ngIf=\"installedPower\">{{(investment / 1000000) | number:'1.1-1'}} M</h2>\n              <h5>Additional USD Invested</h5>\n            </td>\n            <td (click)=\"router.navigate(['bfgz-impact/power'])\" class=\"dashboard-cell\">\n              <h3>Power</h3>\n              <h2>{{(installedPower / 1000) | number:'1.0-0'}}</h2>\n              <h5>in 000's Watts</h5>\n            </td>\n            <!--<td (click)=\"router.navigate(['bfgz-impact/education'])\" class=\"dashboard-cell\">-->\n              <!--<h3>Education</h3>-->\n              <!--<h2>{{schoolsConnected | number:'1.0-0'}}</h2>-->\n              <!--<h5>Schools Connected</h5>-->\n            <!--</td>-->\n            <td (click)=\"router.navigate(['bfgz-impact/productive'])\" class=\"dashboard-cell\">\n              <h3>Productive</h3>\n              <h2>{{productivesConnected | number:'1.0-0'}}</h2>\n              <h5>Businesses/<br>Institutions</h5>\n            </td>\n          </tr>\n        </table>\n        <p class=\"information-paragraph\">*Click cells that light up as green for more information.</p>\n      </div>\n    </div>\n  </div>\n</div>\n<div [ngStyle]=\"{'display':(showPrinterFriendly)?'block':'none'}\" id=\"myModal\" class=\"modal\">\n  <div class=\"print-modal-content\">\n    <div class=\"print-page\">\n      <button pButton type=\"button\" class=\"no-print\" label=\"Close\" (click)=\"hidePFModal()\"></button>\n      <div class=\"divSpacerSmall no-print\"></div>\n      <div style=\"white-space: nowrap; overflow: hidden;\">\n        <div *ngIf=\"chartData && rawData && authService.isAdmin\" style=\"display:inline-block; vertical-align:top; text-align:left; width:200px;\">\n          <h2>Filters:</h2>\n          <div class=\"indent\">\n            <div>\n              <h3>ESP</h3>\n              <div *ngFor=\"let esp of espFilter.filters\" class=\"indent\">\n                <p-checkbox name=\"esps\" [value]=\"esp\" [label]=\"esp\" [(ngModel)]=\"selectedESPs\" [inputId]=\"esp\" (onChange)=\"processFilter($event)\"></p-checkbox>\n                <div class=\"divSmallSpacer\"></div>\n              </div>\n            </div>\n            <div>\n              <h3>Time</h3>\n              <div>\n                <p-calendar [(ngModel)]=\"startDate\" [readonlyInput]=\"true\" placeholder=\"Beginning of Time\" dateFormat=\"dd.mm.yy\"\n                   showButtonBar=\"true\" (onSelect)=\"processFilter($event)\" (onClearClick)=\"processFilter($event)\"></p-calendar>\n              </div>\n              <div style=\"margin:0 auto; text-align:center; font-size:16px; font-weight:bold;\">to</div>\n              <div>\n                <p-calendar [(ngModel)]=\"endDate\" [readonlyInput]=\"true\"  placeholder=\"End of Time\" dateFormat=\"dd.mm.yy\"\n                   showButtonBar=\"true\" (onSelect)=\"processFilter($event)\" (onClearClick)=\"processFilter($event)\"></p-calendar>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div style=\"display:inline-block;\">\n          <h3 style=\"text-align:left;margin:2.5px 0px\">Selected Province: {{selectedProvince}}</h3>\n          <span class=\"reset-text\">*Double Click to Reset Province</span>\n          <div class=\"reset-button\">\n            <button pButton label=\"Reset\" icon=\"fa-undo\" (click)=\"provinceClicked('All')\"></button>\n          </div>\n          <div [ngStyle]=\"{'min-width':'400px','height':'380px','display':'inline-block'}\">\n            <app-d3 #zambiaChartPrint id=\"d3ZambiaPrint\" *ngIf=\"chartData && rawData && showPrinterFriendly\" [data]=\"chartData\"\n              [name]=\"'d3ZambiaPrint'\" [pointCoords]=\"pointCoords\" [height]=\"414\" [width]=\"450\"  ></app-d3>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div>\n      <table class=\"dashboard-stats\">\n        <tr>\n          <td (click)=\"router.navigate(['bfgz-impact/connections'])\" class=\"dashboard-cell\">\n            <h3>Connections</h3>\n            <h2>{{connections | number:'1.0-0'}}</h2>\n            <h5>Energy Service Subscriptions</h5>\n          </td>\n          <td (click)=\"router.navigate(['bfgz-impact/light'])\" class=\"dashboard-cell\">\n            <h3>Light Service</h3>\n            <h2>{{light | number:'1.0-0'}}</h2>\n            <h5>Candles/Lamps Displaced</h5>\n          </td>\n          <td (click)=\"router.navigate(['bfgz-impact/people'])\" class=\"dashboard-cell\">\n            <h3>People</h3>\n            <h2>{{people | number:'1.0-0'}}</h2>\n            <h5>Beneficiaries</h5>\n          </td>\n        </tr>\n        <tr>\n          <td (click)=\"router.navigate(['bfgz-impact/gender'])\" class=\"dashboard-cell\">\n            <h3>Gender</h3>\n            <h2>{{womenCustomers | number:'1.0-0'}}</h2>\n            <h5>Women Primary Customers</h5>\n          </td>\n          <td (click)=\"router.navigate(['bfgz-impact/jobs'])\" class=\"dashboard-cell\">\n            <!-- class=\"multi-dashboard-cell\" -->\n            <h3>Jobs</h3>\n            <h2 *ngIf=\"installedPower\">\n              <!-- {{((jobs['Contracted'] + jobs['Commission Based'])) | number:'1.0-0'}} -->\n              {{ 370 + 1294 | number:'1.0-0'}}\n            </h2>\n            <h5>Full & Part Time Jobs</h5>\n          </td>\n          <td (click)=\"router.navigate(['bfgz-impact/climate'])\" class=\"dashboard-cell\">\n            <h3>Climate</h3>\n            <h2>{{co2Mitigated | number:'1.0-0'}}</h2>\n            <h5>Kg of C0<sub>2</sub> Mitigated Annually</h5>\n          </td>\n        </tr>\n        <tr>\n          <td (click)=\"router.navigate(['bfgz-impact/financials'])\" class=\"dashboard-cell\">\n            <h3>Finance</h3>\n            <!-- Note: Data for Investment will arrive before rest. Checking installedPower as this data will arive with arrive with rest of data-->\n            <h2 *ngIf=\"installedPower\">{{(investment / 1000000) | number:'1.1-1'}} M</h2>\n            <h5>Additional USD Invested</h5>\n          </td>\n          <td (click)=\"router.navigate(['bfgz-impact/power'])\" class=\"dashboard-cell\">\n            <h3>Power</h3>\n            <h2>{{(installedPower / 1000) | number:'1.0-0'}}</h2>\n            <h5>in 000's Watts</h5>\n          </td>\n          <!--<td (click)=\"router.navigate(['bfgz-impact/education'])\" class=\"dashboard-cell\">-->\n            <!--<h3>Education</h3>-->\n            <!--<h2>{{schoolsConnected | number:'1.0-0'}}</h2>-->\n            <!--<h5>Schools Connected</h5>-->\n          <!--</td>-->\n          <td (click)=\"router.navigate(['bfgz-impact/productive'])\" class=\"dashboard-cell\">\n            <h3>Productive</h3>\n            <h2>{{productivesConnected | number:'1.0-0'}}</h2>\n            <h5>Businesses/<br>Institutions</h5>\n          </td>\n        </tr>\n      </table>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/external-dashboard/external-dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExternalDashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_d3_d3_component__ = __webpack_require__("./src/app/shared/d3/d3.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_service_filter_service__ = __webpack_require__("./src/app/shared/service/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__auth_service__ = __webpack_require__("./src/app/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shared_service_pull_config_js__ = __webpack_require__("./src/app/shared/service/pull.config.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__shared_service_dev_metric_json__ = __webpack_require__("./src/app/shared/service/dev-metric.json");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__shared_service_dev_metric_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__shared_service_dev_metric_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__shared_service_prod_metric_json__ = __webpack_require__("./src/app/shared/service/prod-metric.json");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__shared_service_prod_metric_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__shared_service_prod_metric_json__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var ExternalDashboardComponent = /** @class */ (function () {
    function ExternalDashboardComponent(appService, router, externalDashboardService, loadingService, filterService, authService) {
        this.appService = appService;
        this.router = router;
        this.externalDashboardService = externalDashboardService;
        this.loadingService = loadingService;
        this.filterService = filterService;
        this.authService = authService;
        this.selectedProvince = "All";
        this.investment = 54000000;
        this.showPrinterFriendly = false;
        this.chartData = [
            { name: "Copperbelt", value: 0 },
            { name: "Southern", value: 0 },
            { name: "Eastern", value: 0 },
            { name: "Northern", value: 0 },
            { name: "Muchinga", value: 0 },
            { name: "Luapula", value: 0 },
            { name: "North-Western", value: 0 },
            { name: "Western", value: 0 },
            { name: "Lusaka", value: 0 },
            { name: "Central", value: 0 }
        ];
    }
    ExternalDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadingService.show();
        this.externalDashboardService.getFinancialLeverageData().subscribe(function (data) {
            _this.financialLeverageData = data;
            _this.loadInvestmentData(data);
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
        this.externalDashboardService.getJobsData().subscribe(function (data) {
            _this.loadJobsData(data);
            _this.loadingService.hide();
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
        // this.externalDashboardService.getBgfzImpactData()
        // .subscribe((data:any[])=>{
        //     this.loadingService.hide();
        //     this.rawData = data;
        //     this.generateData(this.rawData);
        //     this.setDashboardCells(this.rawData);
        //   },
        //   error=>{
        //     console.log(error);
        //     this.loadingService.hide();
        //   }
        // )
        // new call from endpoint
        this.externalDashboardService.getBgfzImpactDataMetrics()
            .subscribe(function (data) {
            _this.loadingService.hide();
            // this.rawData = data;
            _this.demoSetDashboardCells(data);
            _this.demogenerateData(data);
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
    };
    ExternalDashboardComponent.prototype.ngAfterViewInit = function () {
        this.espFilter = this.filterService.getESPFilter();
        this.selectedESPs = this.espFilter.filters;
    };
    ExternalDashboardComponent.prototype.generateData = function (inputData) {
        this.chartData = [
            { name: "Copperbelt", value: 0 },
            { name: "Southern", value: 0 },
            { name: "Eastern", value: 0 },
            { name: "Northern", value: 0 },
            { name: "Muchinga", value: 0 },
            { name: "Luapula", value: 0 },
            { name: "North-Western", value: 0 },
            { name: "Western", value: 0 },
            { name: "Lusaka", value: 0 },
            { name: "Central", value: 0 }
        ];
        var _this = this;
        var data = __WEBPACK_IMPORTED_MODULE_4_underscore__["countBy"](__WEBPACK_IMPORTED_MODULE_4_underscore__["pluck"](inputData, 'province'), function (prvnc) {
            return prvnc;
        });
        __WEBPACK_IMPORTED_MODULE_4_underscore__["each"](this.chartData, function (val) {
            if (data[val.name]) {
                val.value = data[val.name];
            }
            else {
                val.value = 0;
            }
        });
    };
    ExternalDashboardComponent.prototype.demogenerateData = function (inputData) {
        var province = inputData['province'];
        // this.chartData = [
        //   {name: "Copperbelt", value: province['Copperbelt'] || 16933},
        //   {name: "Southern", value: province['Southern'] || 8909},
        //   {name: "Eastern", value: province['Eastern'] || 14665},
        //   {name: "Northern", value: province['Northern'] || 5137},
        //   {name: "Muchinga", value: province['Muchinga'] || 4156},
        //   {name: "Luapula", value: province['Luapula'] || 4495},
        //   {name: "North-Western", value: province['"North-Western"'] || 8338},
        //   {name: "Western", value: province['Western'] || 4277},
        //   {name: "Lusaka", value: province['Lusaka'] || 7909},
        //   {name: "Central", value: province['Central'] || 15309}
        // ];
        this.chartData = [
            { name: "Copperbelt", value: 16933 },
            { name: "Southern", value: 8909 },
            { name: "Eastern", value: 14665 },
            { name: "Northern", value: 5137 },
            { name: "Muchinga", value: 4156 },
            { name: "Luapula", value: 4495 },
            { name: "North-Western", value: 8338 },
            { name: "Western", value: 4277 },
            { name: "Lusaka", value: 7909 },
            { name: "Central", value: 15309 }
        ];
    };
    ExternalDashboardComponent.prototype.setDashboardCells = function (data) {
        var _this = this;
        if (data) {
            this.connections = data.length;
            var customerTypes = __WEBPACK_IMPORTED_MODULE_4_underscore__["groupBy"](data, function (row) {
                return row.productUse;
            });
            this.people = 0;
            this.light = 0;
            this.co2Mitigated = 0;
            this.installedPower = 0;
            __WEBPACK_IMPORTED_MODULE_4_underscore__["each"](data, function (row) {
                _this.people += row['beneficiaryCoefficient'];
                _this.light += row['lightCoefficient'];
                _this.co2Mitigated += row['co2Coefficient'];
                _this.installedPower += row['wattCoefficient'];
            });
            this.installedPower;
            //this.contractedJobs = customerTypes['Institutional'].length;
            var genders = __WEBPACK_IMPORTED_MODULE_4_underscore__["groupBy"](data, function (row) {
                return row.customerGender;
            });
            if (genders['F']) {
                this.womenCustomers = genders['F'].length;
            }
            else {
                this.womenCustomers = 0;
            }
            var customerSubTypes = __WEBPACK_IMPORTED_MODULE_4_underscore__["groupBy"](data, function (row) {
                if (row.productSubUse) {
                    return row.productSubUse;
                }
                else {
                    return 'N/A';
                }
            });
            if (customerSubTypes['School']) {
                this.schoolsConnected = customerSubTypes['School'].length;
            }
            else {
                this.schoolsConnected = 0;
            }
            //hardcoded value, totals are being manually reported by REEEP, once ESPs send us this data, we can get these numbers pragmattically. Adding the value here vs the html
            //because then it loads at the same time as the rest of the values
            this.productivesConnected = 1461;
        }
    };
    ExternalDashboardComponent.prototype.demoSetDashboardCells = function (data) {
        var res = data[0];
        var _this = this;
        if (data) {
            // console.log(res)
            this.connections = res.connections || 90130;
            this.people = Math.floor(res.beneficiaryCoefficient) || 468676;
            this.light = Math.floor(res.lightCoefficient) || 238635;
            this.co2Mitigated = Math.floor(res.co2Coefficient) || 1577250;
            this.installedPower = Math.floor(res.wattCoefficient) || 1309925;
            // this.womenCustomers = data['genderCount']['F'] || 24470;
            this.womenCustomers = 24470;
            //hardcoded value, totals are being manually reported by REEEP, once ESPs send us this data, we can get these numbers pragmattically. Adding the value here vs the html
            //because then it loads at the same time as the rest of the values
            this.productivesConnected = 1461;
        }
    };
    ExternalDashboardComponent.prototype.loadInvestmentData = function (data) {
        var _this = this;
        this.investment = 0;
        __WEBPACK_IMPORTED_MODULE_4_underscore__["each"](data, function (row) {
            _this.investment += row['amtFunding'];
        });
    };
    ExternalDashboardComponent.prototype.loadJobsData = function (data) {
        var _this = this;
        this.jobs = {
            'Contracted': 0,
            "Commission Based": 0
        };
        __WEBPACK_IMPORTED_MODULE_4_underscore__["each"](data, function (row) {
            _this.jobs[row['textJobType']] += row['nbrJobsCreated'];
        });
    };
    ExternalDashboardComponent.prototype.provinceClicked = function (e) {
        if (e != 'All') {
            this.selectedProvince = e.province.properties.CAPTION;
            this.processFilter(e);
        }
        else {
            this.selectedProvince = 'All';
            this.processFilter(e);
        }
    };
    ExternalDashboardComponent.prototype.processFilter = function (e) {
        var filteredData = this.rawData;
        if (this.authService.isAdmin) {
            var _this_1 = this;
            filteredData = __WEBPACK_IMPORTED_MODULE_4_underscore__["filter"](filteredData, function (row) { return __WEBPACK_IMPORTED_MODULE_4_underscore__["contains"](_this_1.selectedESPs, row['espCompanyName']); });
            if (this.startDate) {
                filteredData = __WEBPACK_IMPORTED_MODULE_4_underscore__["filter"](filteredData, function (row) {
                    var rowDate = new Date(row['dtAcquisition']);
                    return rowDate >= _this_1.startDate;
                });
            }
            if (this.endDate) {
                filteredData = __WEBPACK_IMPORTED_MODULE_4_underscore__["filter"](filteredData, function (row) {
                    var rowDate = new Date(row['dtAcquisition']);
                    return rowDate <= _this_1.endDate;
                });
            }
            this.generateData(filteredData);
            this.zambiaChart.updateChart();
            if (this.zambiaChartPrint) {
                this.zambiaChartPrint.updateChart();
            }
        }
        if (this.selectedProvince != 'All') {
            filteredData = __WEBPACK_IMPORTED_MODULE_4_underscore__["where"](filteredData, { province: this.selectedProvince });
        }
        // this.setDashboardCells(filteredData);
    };
    ExternalDashboardComponent.prototype.showPFModal = function () {
        this.showPrinterFriendly = true;
    };
    ExternalDashboardComponent.prototype.hidePFModal = function () {
        this.showPrinterFriendly = false;
    };
    ExternalDashboardComponent.prototype.getNgClassMap = function () {
        if (this.authService.isAdmin) {
            return 'ui-grid-col-7';
        }
        else {
            return 'ui-grid-col-6';
        }
    };
    ExternalDashboardComponent.prototype.getNgClassCells = function () {
        if (this.authService.isAdmin) {
            return 'ui-grid-col-5 dashboard-grid';
        }
        else {
            return 'ui-grid-col-6 dashboard-grid';
        }
    };
    ExternalDashboardComponent.prototype.subMetricPull = function () {
        // STEPS
        // need to add a flat file for prod and live version fo the API
        // but all calls will be directed to the dev server
        // if config  is  prod,  return prod metric data
        // if config  is dev,  return dev metric data
        var devMet = __WEBPACK_IMPORTED_MODULE_10__shared_service_dev_metric_json__;
        var prodMet = __WEBPACK_IMPORTED_MODULE_11__shared_service_prod_metric_json__;
        // console.log(testDev)
        // console.log(prodDev)
        var metricData;
        __WEBPACK_IMPORTED_MODULE_9__shared_service_pull_config_js__["a" /* PULL_CONFIG */].ENV.name === "dev" ? metricData = devMet : metricData = prodMet;
        this.demogenerateData(metricData);
        this.demoSetDashboardCells(metricData);
        this.loadingService.hide();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('zambiaChart'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__shared_d3_d3_component__["a" /* D3Component */])
    ], ExternalDashboardComponent.prototype, "zambiaChart", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('zambiaChartPrint'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__shared_d3_d3_component__["a" /* D3Component */])
    ], ExternalDashboardComponent.prototype, "zambiaChartPrint", void 0);
    ExternalDashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-external-dashboard',
            template: __webpack_require__("./src/app/external-dashboard/external-dashboard.component.html"),
            styles: [__webpack_require__("./src/app/external-dashboard/external-dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__app_service__["a" /* AppService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_5__external_dashboard_service__["a" /* ExternalDashboardService */],
            __WEBPACK_IMPORTED_MODULE_6__shared_service_loading_services__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_7__shared_service_filter_service__["a" /* FilterService */], __WEBPACK_IMPORTED_MODULE_8__auth_service__["a" /* AuthService */]])
    ], ExternalDashboardComponent);
    return ExternalDashboardComponent;
}());



/***/ }),

/***/ "./src/app/external-dashboard/external-dashboard.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExternalDashboardModule", function() { return ExternalDashboardModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__external_dashboard_component__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__external_dashboard_routing_module__ = __webpack_require__("./src/app/external-dashboard/external-dashboard-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__external_connections_external_connections_component__ = __webpack_require__("./src/app/external-dashboard/external-connections/external-connections.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__external_gender_external_gender_component__ = __webpack_require__("./src/app/external-dashboard/external-gender/external-gender.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__light_light_component__ = __webpack_require__("./src/app/external-dashboard/light/light.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__people_people_component__ = __webpack_require__("./src/app/external-dashboard/people/people.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__jobs_jobs_component__ = __webpack_require__("./src/app/external-dashboard/jobs/jobs.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__climate_climate_component__ = __webpack_require__("./src/app/external-dashboard/climate/climate.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__financials_financials_component__ = __webpack_require__("./src/app/external-dashboard/financials/financials.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__power_power_component__ = __webpack_require__("./src/app/external-dashboard/power/power.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__education_education_component__ = __webpack_require__("./src/app/external-dashboard/education/education.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__productive_productive_component__ = __webpack_require__("./src/app/external-dashboard/productive/productive.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var ExternalDashboardModule = /** @class */ (function () {
    function ExternalDashboardModule() {
    }
    ExternalDashboardModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_4__external_dashboard_routing_module__["a" /* ExternalDashboardRoutingModule */], __WEBPACK_IMPORTED_MODULE_3__shared_shared_module__["a" /* SharedModule */].forRoot()
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__external_dashboard_component__["a" /* ExternalDashboardComponent */], __WEBPACK_IMPORTED_MODULE_5__external_connections_external_connections_component__["a" /* ExternalConnectionsComponent */], __WEBPACK_IMPORTED_MODULE_6__external_gender_external_gender_component__["a" /* ExternalGenderComponent */], __WEBPACK_IMPORTED_MODULE_7__light_light_component__["a" /* LightComponent */], __WEBPACK_IMPORTED_MODULE_8__people_people_component__["a" /* PeopleComponent */], __WEBPACK_IMPORTED_MODULE_9__jobs_jobs_component__["a" /* JobsComponent */], __WEBPACK_IMPORTED_MODULE_10__climate_climate_component__["a" /* ClimateComponent */], __WEBPACK_IMPORTED_MODULE_11__financials_financials_component__["a" /* FinancialsComponent */], __WEBPACK_IMPORTED_MODULE_12__power_power_component__["a" /* PowerComponent */], __WEBPACK_IMPORTED_MODULE_13__education_education_component__["a" /* EducationComponent */], __WEBPACK_IMPORTED_MODULE_15__productive_productive_component__["a" /* ProductiveComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_14__external_dashboard_service__["a" /* ExternalDashboardService */]]
        })
    ], ExternalDashboardModule);
    return ExternalDashboardModule;
}());



/***/ }),

/***/ "./src/app/external-dashboard/external-dashboard.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExternalDashboardService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_env_config_service__ = __webpack_require__("./src/app/shared/service/env.config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ExternalDashboardService = /** @class */ (function () {
    function ExternalDashboardService(envConfig, http) {
        var _this = this;
        this.envConfig = envConfig;
        this.http = http;
        this.getTierData = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Origin': '/' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'tier', options)
                .map(function (res) { return res.json().payload; });
        };
        // TODO: switch out the api call with a new data source
        this.getBgfzImpactData = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact', options)
                .map(function (res) { return res.json().payload; });
        };
        this.getBgfzImpactDataMetrics = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getDemoVariable('endPoint') + 'bgfz-impact')
                .map(function (res) { return res.json(); });
        };
        this.getConnectionData = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/connection', options)
                .map(function (res) { return res.json().payload; });
        };
        this.getFinancialLeverageData = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'financial-leverage', options)
                .map(function (res) { return res.json().payload; });
        };
        this.getLightData = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/light', options)
                .map(function (res) { return res.json().payload; });
        };
        this.getPeopleData = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/people', options)
                .map(function (res) { return res.json().payload; });
        };
        this.getGenderData = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/gender', options)
                .map(function (res) { return res.json().payload; });
        };
        this.getClimateData = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/climate', options)
                .map(function (res) { return res.json().payload; });
        };
        this.getJobsData = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'jobs', options)
                .map(function (res) { return res.json().payload; });
        };
        this.getPowerData = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/power', options)
                .map(function (res) { return res.json().payload; });
        };
        this.getSchoolData = function () {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            return _this.http.get(_this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/school', options)
                .map(function (res) { return res.json().payload; });
        };
    }
    ExternalDashboardService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__shared_service_env_config_service__["a" /* EnvConfig */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], ExternalDashboardService);
    return ExternalDashboardService;
}());



/***/ }),

/***/ "./src/app/external-dashboard/external-gender/external-gender.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/external-dashboard/external-gender/external-gender.component.html":
/***/ (function(module, exports) {

module.exports = "<edison-subDashboard *ngIf=\"plotlyData\"\n  [subDashboardTitle]=\"'Gender: Number of women primary customers'\"\n  [dataFilters]=\"dataFilters\"\n  [rawData]=\"rawData\"\n  [parentDashboard]=\"'bgfz-impact'\"\n  [plotlyData1]=\"plotlyData\"\n  [plotlyLayout1]=\"plotlyLayout\"\n  [showDataTab]=false\n  [dataDownLoad]=flase\n  [chartInfo]=\"chartInfo\"\n  (onDataFilter)=\"setPlotlyData($event)\"\n  (onPlotlyClick)=\"onPlotlyClick($event)\">\n</edison-subDashboard>\n"

/***/ }),

/***/ "./src/app/external-dashboard/external-gender/external-gender.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExternalGenderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__ = __webpack_require__("./src/app/shared/service/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_underscore__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ExternalGenderComponent = /** @class */ (function () {
    function ExternalGenderComponent(router, appService, filterService, externalDashboardService, loadingService) {
        this.router = router;
        this.appService = appService;
        this.filterService = filterService;
        this.externalDashboardService = externalDashboardService;
        this.loadingService = loadingService;
        this.dataFilters = [];
        this.chartInfo = 'This chart is looking at the breakdown by gender of primary customers (not beneficiaries), which refers solely to the individual person under whose name the ESS has been/is being purchased. Note: Vitalite systems are not yet configured to supply gender, this data will be coming soon.';
    }
    ExternalGenderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadingService.show();
        this.dataFilters.push(this.filterService.getProvinceFilter());
        //REMOVED FOR DEMO, can add back after demo
        // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
        this.dataFilters.push(this.filterService.getTierFilter());
        //Removed for demo
        // this.dataFilters.push(this.filterService.getCustomerTypeFilter());
        this.externalDashboardService.getGenderData().subscribe(function (data) {
            _this.rawData = data;
            _this.setPlotlyData(data);
            _this.loadingService.hide();
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
        var plotlyMargin;
        var plotlyOrientation;
        ;
        if (window.innerWidth > 610) {
            plotlyMargin = { l: 50, r: 50, b: 10, t: 20 };
            plotlyOrientation = "w";
        }
        else {
            plotlyMargin = { l: 5, r: 5, b: 10, t: 20 };
            plotlyOrientation = "h";
        }
        this.plotlyLayout = {
            annotations: [
                {
                    font: {
                        size: 14
                    },
                    showarrow: false,
                    text: 'Gender',
                    x: 0.5,
                    y: 0.5
                }
            ],
            margin: plotlyMargin,
            legend: { "orientation": plotlyOrientation }
        };
    };
    ExternalGenderComponent.prototype.onPlotlyClick = function (e) {
        console.log(e);
    };
    ExternalGenderComponent.prototype.setPlotlyData = function (data) {
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                if (data[i]['customerGender'] == null) {
                    data[i]['customerGender'] = 'Not Provided';
                }
            }
            var genderData = __WEBPACK_IMPORTED_MODULE_6_underscore__["countBy"](__WEBPACK_IMPORTED_MODULE_6_underscore__["pluck"](data, 'customerGender', function (gender) { return gender; }));
            var text = [];
            if (genderData['M'] > 0) {
                text.push(genderData['M'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' ESS');
            }
            if (genderData['F'] > 0) {
                text.push(genderData['F'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' ESS');
            }
            if (genderData[null] > 0) {
                text.push(genderData['Not Provided'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' ESS');
            }
            this.plotlyData = [{
                    values: [genderData['M'], genderData['F'], genderData['Not Provided']],
                    labels: ['Male', 'Female', 'Not Provided'],
                    type: 'pie',
                    hole: 0.4,
                    text: text,
                    textposition: 'inside',
                    hoverinfo: 'label',
                    // marker: {
                    //   colors: ['#8C2A2F','#009944']
                    // },
                    textfont: {
                        color: 'white'
                    }
                }];
        }
        else {
            this.plotlyData = [];
        }
    };
    ExternalGenderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-external-gender',
            template: __webpack_require__("./src/app/external-dashboard/external-gender/external-gender.component.html"),
            styles: [__webpack_require__("./src/app/external-dashboard/external-gender/external-gender.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__app_service__["a" /* AppService */], __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__["a" /* FilterService */],
            __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__["a" /* ExternalDashboardService */], __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__["a" /* LoadingService */]])
    ], ExternalGenderComponent);
    return ExternalGenderComponent;
}());



/***/ }),

/***/ "./src/app/external-dashboard/financials/financials.component.css":
/***/ (function(module, exports) {

module.exports = ".row {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.column {\n  -webkit-box-flex: 9.09%;\n      -ms-flex: 9.09%;\n          flex: 9.09%;\n  padding: 5px;\n}\n\n* {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n\n.logos{\n  padding-left: 15%;\n  padding-right: 15%;\n}\n\n.logosSecondRow{\n   padding-left: 12%;\n   padding-right: 12%;\n }\n\n"

/***/ }),

/***/ "./src/app/external-dashboard/financials/financials.component.html":
/***/ (function(module, exports) {

module.exports = "<edison-subDashboard *ngIf=\"plotlyData\"\n                     [subDashboardTitle]=\"'Finance: Additional investment leveraged in USD'\"\n                     [rawData]=\"rawData\"\n                     [parentDashboard]=\"'bgfz-impact'\"\n                     [plotlyData1]=\"plotlyData\"\n                     [plotlyLayout1]=\"plotlyLayout\"\n                     [showDataTab]=false\n                     [dataDownLoad]=flase\n                     [chartInfo]=\"chartInfo\"\n                     (onDataFilter)=\"setPlotlyData($event)\"\n                     (onPlotlyClick)=\"onPlotlyClick($event)\">\n</edison-subDashboard>\n<div class=\"logos\">\n  <div class=\"row\">\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/USTDA-logo.png\" alt=\"USTDA\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/USAID-logo.png\" alt=\"USAID\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/TRINE-logo.png\" alt=\"TRINE\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/DOEN-logo.png\" alt=\"DOEN Foundation\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/InfraCo-logo.png\" alt=\"InfraCo Africa\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/kiva-logo.png\" alt=\"kiva\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/AECF-logo.png\" alt=\"AECF\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/SIMA-logo.png\" alt=\"SIMA\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/Swedbank-logo.png\" alt=\"Swedbank\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/lendahand-logo.png\" alt=\"lendahand\" style=\"width:100%\">\n    </div>\n  </div>\n  <div class=\"logosSecondRow\">\n  <div class=\"row\">\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/GSMA-logo.png\" alt=\"GSMA\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/RUFEP-logo.png\" alt=\"RUFEP\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/fosera-logo.png\" alt=\"fosera\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/TOTAL-logo.png\" alt=\"TOTAL\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/mimimoto-logo.png\" alt=\"Mimi Moto\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/SNV-logo.png\" alt=\"SNV\" style=\"width:100%\">\n    </div>\n    <div class=\"column\">\n      <img src=\"./assets/images/cofunders/SunFunder-logo.png\" alt=\"SunFunder\" style=\"width:100%\">\n    </div>\n  </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/external-dashboard/financials/financials.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FinancialsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_underscore__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FinancialsComponent = /** @class */ (function () {
    function FinancialsComponent(router, appService, externalDashboardService, loadingService) {
        this.router = router;
        this.appService = appService;
        this.externalDashboardService = externalDashboardService;
        this.loadingService = loadingService;
        this.chartInfo = 'This chart displays overall financial inflows into BGFZ Energy Service Provider (ESP) operations in Zambia, as well as rough breakdown by type.';
    }
    FinancialsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadingService.show();
        this.externalDashboardService.getFinancialLeverageData().subscribe(function (data) {
            _this.rawData = data;
            _this.setPlotlyData(data);
            _this.loadingService.hide();
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
        var plotlyMargin;
        var plotlyOrientation;
        ;
        if (window.innerWidth > 610) {
            plotlyMargin = { l: 50, r: 50, b: 10, t: 20 };
            plotlyOrientation = "w";
        }
        else {
            plotlyMargin = { l: 5, r: 5, b: 10, t: 20 };
            plotlyOrientation = "h";
        }
        this.plotlyLayout = {
            annotations: [
                {
                    font: {
                        size: 14
                    },
                    showarrow: false,
                    text: 'Investment',
                    x: 0.5,
                    y: 0.5
                }
            ],
            margin: plotlyMargin,
            legend: { "orientation": plotlyOrientation }
        };
    };
    FinancialsComponent.prototype.onPlotlyClick = function (e) {
        console.log(e);
    };
    FinancialsComponent.prototype.setPlotlyData = function (data) {
        if (data.length > 0) {
            var financeTypes_1 = {
                Equity: 0,
                Debt: 0,
                Grant: 0,
                Other: 0
            };
            __WEBPACK_IMPORTED_MODULE_5_underscore__["each"](data, function (row) {
                financeTypes_1[row['financeType']] += row['amtFunding'];
            });
            console.log(financeTypes_1);
            this.plotlyData = [{
                    values: [financeTypes_1['Equity'], financeTypes_1['Debt'], financeTypes_1['Grant'], financeTypes_1['Other']],
                    labels: ['Equity', 'Debt', 'Grant', 'Other'],
                    type: 'pie',
                    hole: 0.4,
                    text: ['$' + financeTypes_1['Equity'].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
                        '$' + financeTypes_1['Debt'].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
                        '$' + financeTypes_1['Grant'].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
                        '$' + financeTypes_1['Other'].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")],
                    //.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
                    textposition: 'inside',
                    hoverinfo: 'label',
                    textfont: {
                        color: 'white'
                    }
                }];
        }
        else {
            this.plotlyData = [];
        }
    };
    FinancialsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-financials',
            template: __webpack_require__("./src/app/external-dashboard/financials/financials.component.html"),
            styles: [__webpack_require__("./src/app/external-dashboard/financials/financials.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__app_service__["a" /* AppService */], __WEBPACK_IMPORTED_MODULE_3__external_dashboard_service__["a" /* ExternalDashboardService */],
            __WEBPACK_IMPORTED_MODULE_4__shared_service_loading_services__["a" /* LoadingService */]])
    ], FinancialsComponent);
    return FinancialsComponent;
}());



/***/ }),

/***/ "./src/app/external-dashboard/jobs/jobs.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/external-dashboard/jobs/jobs.component.html":
/***/ (function(module, exports) {

module.exports = "<edison-subDashboard *ngIf=\"plotlyData\"\n  [subDashboardTitle]=\"'Jobs: Number of jobs created'\"\n  [rawData]=\"rawData\"\n  [parentDashboard]=\"'bgfz-impact'\"\n  [plotlyData1]=\"plotlyData\"\n  [plotlyLayout1]=\"plotlyLayout\"\n  [showDataTab]=false\n  [dataDownLoad]=flase\n  [chartInfo]=\"chartInfo\"\n  (onDataFilter)=\"setPlotlyData($event)\"\n  (onPlotlyClick)=\"onPlotlyClick($event)\">\n</edison-subDashboard>\n"

/***/ }),

/***/ "./src/app/external-dashboard/jobs/jobs.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JobsComponent = /** @class */ (function () {
    function JobsComponent(router, appService, externalDashboardService, loadingService) {
        this.router = router;
        this.appService = appService;
        this.externalDashboardService = externalDashboardService;
        this.loadingService = loadingService;
        this.chartInfo = 'This chart is looking at the breakdown by employment type of overall jobs created by Energy Service Providers as part of BGFZ. The breakdown indicates the importance of commission-based employees (often known as “agents”) in serving rural populations in markets such as Zambia.';
    }
    JobsComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('page size:', window.innerWidth);
        this.loadingService.show();
        var plotlyMargin;
        var plotlyOrientation;
        ;
        if (window.innerWidth > 610) {
            plotlyMargin = { l: 50, r: 50, b: 10, t: 20 };
            plotlyOrientation = "w";
        }
        else {
            plotlyMargin = { l: 5, r: 5, b: 10, t: 20 };
            plotlyOrientation = "h";
        }
        this.plotlyLayout = {
            annotations: [
                {
                    font: {
                        size: 14
                    },
                    showarrow: false,
                    text: 'Jobs',
                    x: 0.5,
                    y: 0.5
                }
            ],
            margin: plotlyMargin,
            legend: { "orientation": plotlyOrientation }
        };
        this.externalDashboardService.getJobsData().subscribe(function (data) {
            _this.rawData = data;
            _this.setPlotlyData(data);
            _this.loadingService.hide();
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
    };
    JobsComponent.prototype.onPlotlyClick = function (e) {
        console.log(e);
    };
    JobsComponent.prototype.setPlotlyData = function (data) {
        if (data.length > 0) {
            //let genderData = _.countBy(_.pluck(data,'customerGender',function(gender){return gender;}));
            var text = [data[0]['nbrJobsCreated'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Jobs', data[1]['nbrJobsCreated'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Jobs'];
            this.plotlyData = [{
                    values: [data[0]['nbrJobsCreated'], data[1]['nbrJobsCreated']],
                    labels: [data[0]['textJobType'], data[1]['textJobType']],
                    type: 'pie',
                    hole: 0.4,
                    text: text,
                    textposition: 'inside',
                    hoverinfo: 'label',
                    // marker: {
                    //   colors: ['#8C2A2F','#009944']
                    // },
                    textfont: {
                        color: 'white'
                    }
                }];
        }
        else {
            this.plotlyData = [];
        }
    };
    JobsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-jobs',
            template: __webpack_require__("./src/app/external-dashboard/jobs/jobs.component.html"),
            styles: [__webpack_require__("./src/app/external-dashboard/jobs/jobs.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__app_service__["a" /* AppService */], __WEBPACK_IMPORTED_MODULE_3__external_dashboard_service__["a" /* ExternalDashboardService */],
            __WEBPACK_IMPORTED_MODULE_4__shared_service_loading_services__["a" /* LoadingService */]])
    ], JobsComponent);
    return JobsComponent;
}());



/***/ }),

/***/ "./src/app/external-dashboard/light/light.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/external-dashboard/light/light.component.html":
/***/ (function(module, exports) {

module.exports = "<edison-subDashboard *ngIf=\"plotlyData\"\n  subDashboardTitle=\"Light: Number of candles/lamps displaced\"\n  [dataFilters]=\"dataFilters\"\n  [rawData]=\"rawData\"\n  [parentDashboard]=\"'bgfz-impact'\"\n  [plotlyData1]=\"plotlyData\"\n  [plotlyLayout1]=\"plotlyLayout\"\n  [plotlyData2]=\"plotlyData2\"\n  [plotlyLayout2]=\"plotlyLayout2\"\n  [showDataTab]=false\n  [dataDownLoad]=false\n  [chartInfo]=\"chartInfo\"\n  zambiaLegendTitle=\"Number of Lights\"\n  (onDataFilter)=\"setChartData($event)\"\n  (onPlotlyClick)=\"onPlotlyClick($event)\">\n</edison-subDashboard>\n"

/***/ }),

/***/ "./src/app/external-dashboard/light/light.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LightComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__ = __webpack_require__("./src/app/shared/service/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_underscore__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LightComponent = /** @class */ (function () {
    function LightComponent(router, appService, filterService, externalDashboardService, loadingService) {
        this.router = router;
        this.appService = appService;
        this.filterService = filterService;
        this.externalDashboardService = externalDashboardService;
        this.loadingService = loadingService;
        this.dataFilters = [];
        this.chartInfo = 'Through modern energy service subscriptions provided via solar home systems and micro grid connectivity, BGFZ is helping Zambians replace traditional candles and lamps with high-output LED lights.';
    }
    LightComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadingService.show();
        //REMOVED FOR DEMO, can add back after demo
        // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
        this.dataFilters.push(this.filterService.getTierFilter());
        //removed for demo
        // this.dataFilters.push(this.filterService.getCustomerTypeFilter());
        this.externalDashboardService.getLightData().subscribe(function (data) {
            _this.rawData = data;
            _this.setChartData(data);
            _this.loadingService.hide();
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
        this.plotlyLayout = {
            title: 'Candles/Lamps Displaced by Tier',
            showlegend: true,
            autosize: true,
            // xaxis: { title: 'Province' },
            yaxis: { title: 'Candles/Lamps Displaced' },
            barmode: 'stack',
            hovermode: 'closest'
        };
        this.plotlyLayout2 = {
            title: 'Total by Tier',
            annotations: [
                {
                    font: {
                        size: 14
                    },
                    showarrow: false,
                    text: 'Displaced',
                    x: 0.5,
                    y: 0.5
                }
            ],
            margin: { l: 10, r: 10, b: 30, t: 50 },
            showlegend: false
        };
    };
    LightComponent.prototype.onPlotlyClick = function (e) {
        console.log(e);
    };
    LightComponent.prototype.setChartData = function (data) {
        var _this = this;
        //removed for demo
        // let tiers = [1,2,3,4,5,6];
        var tiers = [1, 2, 3];
        this.plotlyData = [];
        var pieTotals = [];
        var text = [];
        var _loop_1 = function (i) {
            var tierData = __WEBPACK_IMPORTED_MODULE_6_underscore__["where"](data, { tierLevel: tiers[i] });
            var provinceTier = {
                Copperbelt: 0,
                Southern: 0,
                Eastern: 0,
                Northern: 0,
                Muchinga: 0,
                Luapula: 0,
                'North-Western': 0,
                Western: 0,
                Lusaka: 0,
                Central: 0,
                Total: 0
            };
            __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](tierData, function (row) {
                provinceTier[row['province']] += row['lightCoefficient'];
                provinceTier['Total'] += row['lightCoefficient'];
            });
            var trace = {
                x: ['Copperbelt', 'Southern', 'Eastern', 'Northern', 'Muchinga', 'Luapula', 'North-Western', 'Western', 'Lusaka', 'Central', 'Total'],
                y: [
                    provinceTier['Copperbelt'],
                    provinceTier['Southern'],
                    provinceTier['Eastern'],
                    provinceTier['Northern'],
                    provinceTier['Muchinga'],
                    provinceTier['Luapula'],
                    provinceTier['North-Western'],
                    provinceTier['Western'],
                    provinceTier['Lusaka'],
                    provinceTier['Central']
                ],
                name: 'Tier ' + tiers[i].toString(),
                type: 'bar'
            };
            this_1.plotlyData.push(trace);
            pieTotals.push(provinceTier['Total']);
            text.push(provinceTier['Total'].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        };
        var this_1 = this;
        for (var i in tiers) {
            _loop_1(i);
        }
        var tierLabels = [];
        __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](tiers, function (tier) { tierLabels.push('Tier ' + tier.toString()); });
        this.plotlyData2 = [{
                values: pieTotals,
                labels: tierLabels,
                type: 'pie',
                hole: 0.3,
                text: text,
                textposition: 'inside',
                hoverinfo: 'label',
                textfont: {
                    color: 'white'
                },
                marker: {
                    colors: this.filterService.getPlotlyColors()
                }
            }];
    };
    LightComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-light',
            template: __webpack_require__("./src/app/external-dashboard/light/light.component.html"),
            styles: [__webpack_require__("./src/app/external-dashboard/light/light.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__app_service__["a" /* AppService */], __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__["a" /* FilterService */],
            __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__["a" /* ExternalDashboardService */], __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__["a" /* LoadingService */]])
    ], LightComponent);
    return LightComponent;
}());



/***/ }),

/***/ "./src/app/external-dashboard/people/people.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/external-dashboard/people/people.component.html":
/***/ (function(module, exports) {

module.exports = "<edison-subDashboard *ngIf=\"plotlyData\"\n  subDashboardTitle=\"People: Number of beneficiaries\"\n  [dataFilters]=\"dataFilters\"\n  [rawData]=\"rawData\"\n  [parentDashboard]=\"'bgfz-impact'\"\n  [plotlyData1]=\"plotlyData\"\n  [plotlyLayout1]=\"plotlyLayout\"\n  [plotlyData2]=\"plotlyData2\"\n  [plotlyLayout2]=\"plotlyLayout2\"\n  [showDataTab]=false\n  [dataDownLoad]=false\n  [chartInfo]=\"chartInfo\"\n  zambiaLegendTitle=\"Number of Beneficiaries\"\n  (onDataFilter)=\"setChartData($event)\"\n  (onPlotlyClick)=\"onPlotlyClick($event)\">\n</edison-subDashboard>\n"

/***/ }),

/***/ "./src/app/external-dashboard/people/people.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PeopleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__ = __webpack_require__("./src/app/shared/service/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_underscore__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PeopleComponent = /** @class */ (function () {
    function PeopleComponent(router, appService, filterService, externalDashboardService, loadingService) {
        this.router = router;
        this.appService = appService;
        this.filterService = filterService;
        this.externalDashboardService = externalDashboardService;
        this.loadingService = loadingService;
        this.dataFilters = [];
        this.chartInfo = 'A beneficiary is defined as an individual person directly or indirectly benefitting from the Energy Service Subscription in question. For households, the average Zambian household size of 5.2 has been used to calculate ESSh. For institutions (such as schools and health clinics), a default value of 10 beneficiaries has been used. For productive ESS, only 1 beneficiary is calculated in the absence of more robust data, which the BGFZ team will be collecting and assessing during implementation.';
    }
    PeopleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadingService.show();
        //REMOVED FOR DEMO, can add back after demo
        // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
        this.dataFilters.push(this.filterService.getTierFilter());
        //removed for demo
        // this.dataFilters.push(this.filterService.getCustomerTypeFilter());
        this.externalDashboardService.getPeopleData().subscribe(function (data) {
            _this.rawData = data;
            console.log(data);
            _this.setChartData(data);
            _this.loadingService.hide();
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
        this.plotlyLayout = {
            title: 'Beneficiaries by Tier',
            showlegend: true,
            autosize: true,
            // xaxis: { title: 'Province' },
            yaxis: { title: 'People' },
            barmode: 'stack',
            hovermode: 'closest'
        };
        this.plotlyLayout2 = {
            title: 'Total by Tier',
            annotations: [
                {
                    font: {
                        size: 14
                    },
                    showarrow: false,
                    text: 'People',
                    x: 0.5,
                    y: 0.5
                }
            ],
            margin: { l: 10, r: 10, b: 30, t: 50 },
            showlegend: false
        };
    };
    PeopleComponent.prototype.onPlotlyClick = function (e) {
        console.log(e);
    };
    PeopleComponent.prototype.setChartData = function (data) {
        var _this = this;
        //removed for demo
        // let tiers = [1,2,3,4,5,6];
        var tiers = [1, 2, 3];
        this.plotlyData = [];
        var pieTotals = [];
        var text = [];
        var _loop_1 = function (i) {
            var tierData = __WEBPACK_IMPORTED_MODULE_6_underscore__["where"](data, { tierLevel: tiers[i] });
            var provinceTier = {
                Copperbelt: 0,
                Southern: 0,
                Eastern: 0,
                Northern: 0,
                Muchinga: 0,
                Luapula: 0,
                'North-Western': 0,
                Western: 0,
                Lusaka: 0,
                Central: 0,
                Total: 0
            };
            __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](tierData, function (row) {
                provinceTier[row['province']] += row['beneficiaryCoefficient'];
                provinceTier['Total'] += row['beneficiaryCoefficient'];
            });
            var trace = {
                x: ['Copperbelt', 'Southern', 'Eastern', 'Northern', 'Muchinga', 'Luapula', 'North-Western', 'Western', 'Lusaka', 'Central', 'Total'],
                y: [
                    provinceTier['Copperbelt'],
                    provinceTier['Southern'],
                    provinceTier['Eastern'],
                    provinceTier['Northern'],
                    provinceTier['Muchinga'],
                    provinceTier['Luapula'],
                    provinceTier['North-Western'],
                    provinceTier['Western'],
                    provinceTier['Lusaka'],
                    provinceTier['Central']
                ],
                name: 'Tier ' + tiers[i].toString(),
                type: 'bar'
            };
            this_1.plotlyData.push(trace);
            pieTotals.push(provinceTier['Total']);
            text.push(provinceTier['Total'].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        };
        var this_1 = this;
        for (var i in tiers) {
            _loop_1(i);
        }
        var tierLabels = [];
        __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](tiers, function (tier) { tierLabels.push('Tier ' + tier.toString()); });
        this.plotlyData2 = [{
                values: pieTotals,
                labels: tierLabels,
                type: 'pie',
                hole: 0.3,
                text: text,
                textposition: 'inside',
                hoverinfo: 'label',
                textfont: {
                    color: 'white'
                },
                marker: {
                    colors: this.filterService.getPlotlyColors()
                }
            }];
    };
    PeopleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-people',
            template: __webpack_require__("./src/app/external-dashboard/people/people.component.html"),
            styles: [__webpack_require__("./src/app/external-dashboard/people/people.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__app_service__["a" /* AppService */], __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__["a" /* FilterService */],
            __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__["a" /* ExternalDashboardService */], __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__["a" /* LoadingService */]])
    ], PeopleComponent);
    return PeopleComponent;
}());



/***/ }),

/***/ "./src/app/external-dashboard/power/power.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/external-dashboard/power/power.component.html":
/***/ (function(module, exports) {

module.exports = "<edison-subDashboard *ngIf=\"plotlyData\"\n  subDashboardTitle=\"Power: Number of Watts of installed capacity\"\n  [dataFilters]=\"dataFilters\"\n  [rawData]=\"rawData\"\n  [parentDashboard]=\"'bgfz-impact'\"\n  [plotlyData1]=\"plotlyData\"\n  [plotlyLayout1]=\"plotlyLayout\"\n  [zambiaData]=\"zambiaData\"\n  [showDataTab]=false\n  [dataDownLoad]=false\n  [chartInfo]=\"chartInfo\"\n  zambiaLegendTitle=\"Number of Watts\"\n  (onDataFilter)=\"setChartData($event)\"\n  (onPlotlyClick)=\"onPlotlyClick($event)\">\n</edison-subDashboard>\n"

/***/ }),

/***/ "./src/app/external-dashboard/power/power.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PowerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__ = __webpack_require__("./src/app/shared/service/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_underscore__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PowerComponent = /** @class */ (function () {
    function PowerComponent(router, appService, filterService, externalDashboardService, loadingService) {
        this.router = router;
        this.appService = appService;
        this.filterService = filterService;
        this.externalDashboardService = externalDashboardService;
        this.loadingService = loadingService;
        this.dataFilters = [];
        this.chartInfo = 'This chart shows the estimated total power capacity in Watt (peak) of the ESS deployed under BGFZ';
    }
    PowerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadingService.show();
        //REMOVED FOR DEMO, can add back after demo
        // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
        this.dataFilters.push(this.filterService.getTierFilter());
        //removed for demo
        // this.dataFilters.push(this.filterService.getCustomerTypeFilter());
        this.externalDashboardService.getPowerData().subscribe(function (data) {
            console.log(data);
            _this.rawData = data;
            _this.setChartData(data);
            _this.loadingService.hide();
        }, function (error) {
            console.log(error);
            _this.loadingService.hide();
        });
        this.plotlyLayout = {
            title: 'Watts by Tier',
            showlegend: false,
            autosize: true,
            // xaxis: { title: 'Province' },
            yaxis: { title: 'Watts', zeroline: true },
            barmode: 'stack',
            hovermode: 'closest'
        };
    };
    PowerComponent.prototype.onPlotlyClick = function (e) {
        console.log(e);
    };
    PowerComponent.prototype.setChartData = function (data) {
        var _this = this;
        this.zambiaData = [
            { name: "Copperbelt", value: 0 },
            { name: "Southern", value: 0 },
            { name: "Eastern", value: 0 },
            { name: "Northern", value: 0 },
            { name: "Muchinga", value: 0 },
            { name: "Luapula", value: 0 },
            { name: "North-Western", value: 0 },
            { name: "Western", value: 0 },
            { name: "Lusaka", value: 0 },
            { name: "Central", value: 0 }
        ];
        var zambiaTotal = {
            Copperbelt: 0,
            Southern: 0,
            Eastern: 0,
            Northern: 0,
            Muchinga: 0,
            Luapula: 0,
            'North-Western': 0,
            Western: 0,
            Lusaka: 0,
            Central: 0,
            Total: 0
        };
        //let tiers = [1,2,3,4,5,6]
        //this.plotlyData = []
        var xData = ['Copperbelt', 'Southern', 'Eastern', 'Northern', 'Muchinga', 'Luapula', 'North-Western', 'Western', 'Lusaka', 'Central', 'Total'];
        //for(let i in tiers){
        //let tierData = data;
        var provinceTier = {
            Copperbelt: 0,
            Southern: 0,
            Eastern: 0,
            Northern: 0,
            Muchinga: 0,
            Luapula: 0,
            'North-Western': 0,
            Western: 0,
            Lusaka: 0,
            Central: 0,
            Total: 0
        };
        __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](data, function (row) {
            provinceTier[row['province']] += row['wattCoefficient'];
            provinceTier['Total'] += row['wattCoefficient'];
            zambiaTotal[row['province']] += row['wattCoefficient'];
        });
        var yBase = [(provinceTier['Total'] - provinceTier['Copperbelt'])];
        __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](xData, function (province) {
            if (province != 'Copperbelt' && province != 'Total') {
                var yBaseVal = yBase[yBase.length - 1] - provinceTier[province];
                yBase.push(yBaseVal);
            }
        });
        var yTop = [];
        __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](xData, function (province) {
            yTop.push(provinceTier[province]);
        });
        var traceBase = {
            x: xData,
            y: yBase,
            name: 'Base ',
            type: 'bar',
            marker: {
                color: 'rgba(1,1,1,0.0)'
            },
        };
        var traceTop = {
            x: xData,
            y: yTop,
            name: 'Top ',
            type: 'bar',
            marker: {
                color: 'rgba(55,128,191,0.7)',
                line: {
                    color: 'rgba(55,128,191,1.0)',
                    width: 2
                }
            }
        };
        this.plotlyData = [traceBase, traceTop];
        __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](this.zambiaData, function (row) {
            row.value = zambiaTotal[row.name];
        });
    };
    PowerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-power',
            template: __webpack_require__("./src/app/external-dashboard/power/power.component.html"),
            styles: [__webpack_require__("./src/app/external-dashboard/power/power.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__app_service__["a" /* AppService */], __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__["a" /* FilterService */],
            __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__["a" /* ExternalDashboardService */], __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__["a" /* LoadingService */]])
    ], PowerComponent);
    return PowerComponent;
}());



/***/ }),

/***/ "./src/app/external-dashboard/productive/productive.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/external-dashboard/productive/productive.component.html":
/***/ (function(module, exports) {

module.exports = "<edison-subDashboard *ngIf=\"plotlyData\"\n                     [subDashboardTitle]=\"'Productive: Number of Businesses/Institutions connected'\"\n                     [dataFilters]=\"dataFilters\"\n                     [rawData]=\"rawData\"\n                     [parentDashboard]=\"'bgfz-impact'\"\n                     [plotlyData1]=\"plotlyData\"\n                     [plotlyLayout1]=\"plotlyLayout\"\n                     [plotlyData2]=\"plotlyData2\"\n                     [plotlyLayout2]=\"plotlyLayout2\"\n                     [showDataTab]=false\n                     [dataDownLoad]=false\n                     [chartInfo]=\"chartInfo\"\n                     (onDataFilter)=\"setChartData($event)\"\n                     (onPlotlyClick)=\"onPlotlyClick($event)\">\n</edison-subDashboard>\n"

/***/ }),

/***/ "./src/app/external-dashboard/productive/productive.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductiveComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__ = __webpack_require__("./src/app/shared/service/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__ = __webpack_require__("./src/app/external-dashboard/external-dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__ = __webpack_require__("./src/app/shared/service/loading-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_underscore__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProductiveComponent = /** @class */ (function () {
    function ProductiveComponent(router, appService, filterService, externalDashboardService, loadingService) {
        this.router = router;
        this.appService = appService;
        this.filterService = filterService;
        this.externalDashboardService = externalDashboardService;
        this.loadingService = loadingService;
        this.dataFilters = [];
        this.chartInfo = 'This chart shows the number and breakdown of businesses/institutions connected to modern energy under BGFZ';
    }
    ProductiveComponent.prototype.ngOnInit = function () {
        this.loadingService.show();
        //REMOVED FOR DEMO, can add back after demo
        // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
        this.dataFilters.push(this.filterService.getTierFilter());
        this.plotlyLayout = {
            title: 'Businesses/Institutions Connected by Tier',
            showlegend: true,
            autosize: true,
            yaxis: { title: 'Businesses/Institutions Connected' },
            barmode: 'stack',
            hovermode: 'closest'
        };
        this.plotlyLayout2 = {
            title: 'Total by Tier',
            annotations: [
                {
                    font: {
                        size: 14
                    },
                    showarrow: false,
                    text: 'Businesses/<br>Institutions',
                    x: 0.5,
                    y: 0.5
                }
            ],
            margin: { l: 10, r: 10, b: 30, t: 50 },
            showlegend: false
        };
        var data = [{ 'province': 'Central', 'tierLevel': 1, 'count': 204 },
            { 'province': 'Central', 'tierLevel': 2, 'count': 222 },
            { 'province': 'Copperbelt', 'tierLevel': 1, 'count': 91 },
            { 'province': 'Copperbelt', 'tierLevel': 2, 'count': 153 },
            { 'province': 'Eastern', 'tierLevel': 1, 'count': 88 },
            { 'province': 'Eastern', 'tierLevel': 2, 'count': 211 },
            { 'province': 'Luapula', 'tierLevel': 1, 'count': 14 },
            { 'province': 'Luapula', 'tierLevel': 2, 'count': 22 },
            { 'province': 'Lusaka', 'tierLevel': 1, 'count': 47 },
            { 'province': 'Lusaka', 'tierLevel': 2, 'count': 123 },
            { 'province': 'Muchinga', 'tierLevel': 1, 'count': 22 },
            { 'province': 'Muchinga', 'tierLevel': 2, 'count': 19 },
            { 'province': 'North-Western', 'tierLevel': 1, 'count': 12 },
            { 'province': 'North-Western', 'tierLevel': 2, 'count': 32 },
            { 'province': 'Northern', 'tierLevel': 1, 'count': 8 },
            { 'province': 'Northern', 'tierLevel': 2, 'count': 9 },
            { 'province': 'Southern', 'tierLevel': 1, 'count': 50 },
            { 'province': 'Southern', 'tierLevel': 2, 'count': 71 },
            { 'province': 'Western', 'tierLevel': 1, 'count': 24 },
            { 'province': 'Western', 'tierLevel': 2, 'count': 39 }];
        this.rawData = data;
        this.setChartData(data);
        this.loadingService.hide();
    };
    ProductiveComponent.prototype.onPlotlyClick = function (e) {
        console.log(e);
    };
    ProductiveComponent.prototype.setChartData = function (data) {
        var _this = this;
        //removed for demo
        // let tiers = [1,2,3,4,5,6];
        var tiers = [1, 2];
        this.plotlyData = [];
        var pieTotals = [];
        var text = [];
        for (var i in tiers) {
            var tierData = __WEBPACK_IMPORTED_MODULE_6_underscore__["where"](data, { tierLevel: tiers[i] });
            var res = tierData.reduce(function (acc, curr) {
                acc[curr.province] = curr.count;
                return acc;
            }, {});
            var sum = tierData.reduce(function (s, f) { return s + f.count; }, 0);
            var trace = {
                x: ['Copperbelt', 'Southern', 'Eastern', 'Northern', 'Muchinga', 'Luapula', 'North-Western', 'Western', 'Lusaka', 'Central', 'Total'],
                y: [
                    res['Copperbelt'],
                    res['Southern'],
                    res['Eastern'],
                    res['Northern'],
                    res['Muchinga'],
                    res['Luapula'],
                    res['North-Western'],
                    res['Western'],
                    res['Lusaka'],
                    res['Central']
                ],
                name: 'Tier ' + tiers[i].toString(),
                type: 'bar'
            };
            this.plotlyData.push(trace);
            pieTotals.push(sum);
            text.push(sum.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }
        var tierLabels = [];
        __WEBPACK_IMPORTED_MODULE_6_underscore__["each"](tiers, function (tier) { tierLabels.push('Tier ' + tier.toString()); });
        this.plotlyData2 = [{
                values: pieTotals,
                labels: tierLabels,
                type: 'pie',
                hole: 0.3,
                text: text,
                textposition: 'inside',
                hoverinfo: 'label',
                textfont: {
                    color: 'white'
                },
                marker: {
                    colors: this.filterService.getPlotlyColors()
                }
            }];
    };
    ProductiveComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-productive',
            template: __webpack_require__("./src/app/external-dashboard/productive/productive.component.html"),
            styles: [__webpack_require__("./src/app/external-dashboard/productive/productive.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__app_service__["a" /* AppService */], __WEBPACK_IMPORTED_MODULE_3__shared_service_filter_service__["a" /* FilterService */],
            __WEBPACK_IMPORTED_MODULE_4__external_dashboard_service__["a" /* ExternalDashboardService */], __WEBPACK_IMPORTED_MODULE_5__shared_service_loading_services__["a" /* LoadingService */]])
    ], ProductiveComponent);
    return ProductiveComponent;
}());



/***/ }),

/***/ "./src/app/shared/service/dev-metric.json":
/***/ (function(module, exports) {

module.exports = [{"beneficiaryCoefficient":468676.0000000002,"lightCoefficient":238635,"co2Coefficient":1577250.0499999998,"wattCoefficient":1309925,"genderCount":{"M":65660,"F":24470},"province":{"Copperbelt":16933,"Central":15309,"Eastern":14665,"Southern":8909,"North-Western":8338,"Lusaka":7909,"Northern":5137,"Luapula":4495,"Western":4277,"Muchinga":4156},"connections":90130}]

/***/ }),

/***/ "./src/app/shared/service/prod-metric.json":
/***/ (function(module, exports) {

module.exports = [{"beneficiaryCoefficient":805703.6000000003,"lightCoefficient":399477,"co2Coefficient":2602705.03,"wattCoefficient":2219077,"genderCount":{"M":103267,"F":36841},"province":{"Eastern":27933,"Central":27737,"Copperbelt":25470,"Lusaka":15702,"Southern":14573,"North-Western":11653,"Northern":10529,"Muchinga":7914,"Luapula":7254,"Western":6176},"connections":154943}]

/***/ }),

/***/ "./src/app/shared/service/pull.config.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const PULL_CONFIG = {
    ENV: {
        // name: 'prod',
        name: 'dev'
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = PULL_CONFIG;


/***/ })

});
//# sourceMappingURL=external-dashboard.module.chunk.js.map