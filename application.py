import os
import json
from flask import Flask, Response, jsonify, render_template
import pandas as pd
from flask_cors import CORS
from apscheduler.schedulers.background import BackgroundScheduler

application = Flask(__name__, static_folder='static', template_folder='static')

CORS(application)

@application.route('/')
def index():
    """Render home page."""
    return render_template('index.html') 

def save_dev_metric():
    df = pd.read_json('http://ec2-18-194-220-86.eu-central-1.compute.amazonaws.com:8080/edison-dev/bgfz-impact') # dev
    # df = pd.read_json('http://ec2-18-196-141-170.eu-central-1.compute.amazonaws.com:8080/edison-prod/bgfz-impact') # prod
    df = pd.DataFrame(df['payload'].values.tolist())

    bgfz_impact_data = [{
            "beneficiaryCoefficient": df['beneficiaryCoefficient'].sum(),
            "lightCoefficient": df['lightCoefficient'].sum(),
            "co2Coefficient": df['co2Coefficient'].sum(),
            "wattCoefficient": df['wattCoefficient'].sum(),
            # "genderCount": df['customerGender'].value_counts().to_dict(),
            # "province": df['province'].value_counts().to_dict(),
            "connections": df.shape[0]
    }]
    impact_data = pd.DataFrame(bgfz_impact_data)
    impact_data.to_csv('./data/dev_impact.csv', index=False)

"""
Periodic save dashboard metrics save
"""
sched = BackgroundScheduler(daemon=True)
sched.add_job(save_dev_metric,'interval', minutes=60)


"""
bgfz-impact endpoint
https://stackoverflow.com/questions/13081532/return-json-response-from-flask-view
"""
@application.route("/bgfz-impact")
def bgfz_impact():
    """
    Route returns bgfz trends data
    """
    res = pd.read_csv('./data/dev_impact.csv')
    return jsonify(json.loads(res.to_json(orient='records')))


@application.route("/bgfz-impact/connection")
@application.route("/bgfz-impact/light")
@application.route("/bgfz-impact/people")
@application.route("/bgfz-impact/gender")
@application.route("/bgfz-impact/climate")
@application.route("/bgfz-impact/power")
def bgfz_combined():
    """
    Route combined

    Productionlising the data pipeline to generate the json file below

    """
    res = pd.read_json('./data/api_like_json.json')
    return jsonify(json.loads(res.to_json()))



if __name__ == '__main__':
    application.run(debug=True)