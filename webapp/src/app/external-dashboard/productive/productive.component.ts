import { Component, OnInit } from '@angular/core';
import {Filter} from "../../shared/class/filter.obj";
import {Router} from "@angular/router";
import {AppService} from "../../app.service";
import {FilterService} from "../../shared/service/filter.service";
import {ExternalDashboardService} from "../external-dashboard.service";
import {LoadingService} from "../../shared/service/loading-services";
import * as _ from 'underscore';
import {forEach} from "@angular/router/src/utils/collection";
import {current} from "codelyzer/util/syntaxKind";

@Component({
  selector: 'app-productive',
  templateUrl: './productive.component.html',
  styleUrls: ['./productive.component.css']
})
export class ProductiveComponent implements OnInit {
  dataFilters: Filter[]=[];
  rawData:any[];
  plotlyData:any[];
  plotlyLayout:any;
  plotlyData2:any[];
  plotlyLayout2:any;
  chartInfo:string = 'This chart shows the number and breakdown of businesses/institutions connected to modern energy under BGFZ';

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
              private externalDashboardService:ExternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    //REMOVED FOR DEMO, can add back after demo
    // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
    this.dataFilters.push(this.filterService.getTierFilter());

    this.plotlyLayout = {
      title: 'Businesses/Institutions Connected by Tier',
      showlegend: true,
      autosize: true,
      yaxis: { title: 'Businesses/Institutions Connected' },
      barmode: 'stack',
      hovermode:'closest'
    };
    this.plotlyLayout2 = {
      title: 'Total by Tier',
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: 'Businesses/<br>Institutions',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: { l: 10, r: 10, b: 30, t: 50 },
      showlegend:false
    };

    let data =
       [{'province': 'Central', 'tierLevel': 1, 'count': 204},
        {'province': 'Central', 'tierLevel': 2, 'count': 222},
        {'province': 'Copperbelt', 'tierLevel': 1, 'count': 91},
        {'province': 'Copperbelt', 'tierLevel': 2, 'count': 153},
        {'province': 'Eastern', 'tierLevel': 1, 'count': 88},
        {'province': 'Eastern', 'tierLevel': 2, 'count': 211},
        {'province': 'Luapula', 'tierLevel': 1, 'count': 14},
        {'province': 'Luapula', 'tierLevel': 2, 'count': 22},
        {'province': 'Lusaka', 'tierLevel': 1, 'count': 47},
        {'province': 'Lusaka', 'tierLevel': 2, 'count': 123},
        {'province': 'Muchinga', 'tierLevel': 1, 'count': 22},
        {'province': 'Muchinga', 'tierLevel': 2, 'count': 19},
        {'province': 'North-Western', 'tierLevel': 1, 'count': 12},
        {'province': 'North-Western', 'tierLevel': 2, 'count': 32},
        {'province': 'Northern', 'tierLevel': 1, 'count': 8},
        {'province': 'Northern', 'tierLevel': 2, 'count': 9},
        {'province': 'Southern', 'tierLevel': 1, 'count': 50},
        {'province': 'Southern', 'tierLevel': 2, 'count': 71},
        {'province': 'Western', 'tierLevel': 1, 'count': 24},
        {'province': 'Western', 'tierLevel': 2, 'count': 39}];

    this.rawData=data;
    this.setChartData(data);
    this.loadingService.hide();
  }

  onPlotlyClick(e){
    console.log(e);
  }

  setChartData(data){
    let _this = this;
    //removed for demo
    // let tiers = [1,2,3,4,5,6];
    let tiers = [1,2];
    this.plotlyData = [];
    let pieTotals = [];
    let text = [];

    for(let i in tiers){
      let tierData = _.where(data,{tierLevel:tiers[i]});

      let res = tierData.reduce((acc, curr) =>
      {
        acc[curr.province] = curr.count;
        return acc;
      }, {});

      let sum = tierData.reduce((s, f) => s + f.count, 0);

      let trace = {
        x:['Copperbelt','Southern','Eastern','Northern','Muchinga','Luapula','North-Western','Western','Lusaka','Central','Total'],
        y:[
          res['Copperbelt'],
          res['Southern'],
          res['Eastern'],
          res['Northern'],
          res['Muchinga'],
          res['Luapula'],
          res['North-Western'],
          res['Western'],
          res['Lusaka'],
          res['Central']
        ],
        name: 'Tier ' + tiers[i].toString(),
        type: 'bar'
      };
      this.plotlyData.push(trace);

      pieTotals.push(sum);
      text.push(sum.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    }

    let tierLabels = [];
    _.each(tiers,function(tier){tierLabels.push('Tier ' + tier.toString())});

    this.plotlyData2 = [{
      values: pieTotals,
      labels: tierLabels,
      type: 'pie',
      hole: 0.3,
      text: text,
      textposition: 'inside',
      hoverinfo: 'label',
      textfont: {
        color: 'white'
      },
      marker: {
        colors: this.filterService.getPlotlyColors()
      }
    }];
  }
}
