import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExternalDashboardComponent } from './external-dashboard.component';
import {SharedModule} from '../shared/shared.module';

import {ExternalDashboardRoutingModule} from './external-dashboard-routing.module';
import { ExternalConnectionsComponent } from './external-connections/external-connections.component';
import { ExternalGenderComponent } from './external-gender/external-gender.component';
import { LightComponent } from './light/light.component';
import { PeopleComponent } from './people/people.component';
import { JobsComponent } from './jobs/jobs.component';
import { ClimateComponent } from './climate/climate.component';
import { FinancialsComponent } from './financials/financials.component';
import { PowerComponent } from './power/power.component';
import { EducationComponent } from './education/education.component';
import {ExternalDashboardService} from './external-dashboard.service';
import { ProductiveComponent } from './productive/productive.component';

@NgModule({
  imports: [
    CommonModule,ExternalDashboardRoutingModule,SharedModule.forRoot()
  ],
  declarations: [ExternalDashboardComponent, ExternalConnectionsComponent, ExternalGenderComponent, LightComponent, PeopleComponent, JobsComponent, ClimateComponent, FinancialsComponent, PowerComponent, EducationComponent, ProductiveComponent],
  providers: [ExternalDashboardService]
})
export class ExternalDashboardModule { }
