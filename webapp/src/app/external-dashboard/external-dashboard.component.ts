import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {Router} from '@angular/router';
import { CurrencyPipe,PercentPipe,DecimalPipe } from '@angular/common';
import {AppService} from '../app.service';
import {D3Component} from '../shared/d3/d3.component';
import * as _ from 'underscore';
import {MenuItem} from 'primeng/primeng';
import {ExternalDashboardService} from './external-dashboard.service';
import {Tier} from '../shared/class/tier.obj';
import {LoadingService} from '../shared/service/loading-services';
import {FilterService} from '../shared/service/filter.service';
import {Filter} from '../shared/class/filter.obj';
import {AuthService} from '../auth.service';
import { PULL_CONFIG } from '../shared/service/pull.config.js';
import  *  as  devMetric  from  '../shared/service/dev-metric.json';
import  *  as  prodMetric  from  '../shared/service/prod-metric.json';

@Component({
  selector: 'app-external-dashboard',
  templateUrl: './external-dashboard.component.html',
  styleUrls: ['./external-dashboard.component.css']
})
export class ExternalDashboardComponent implements OnInit {
  pointCoords:[number[],number[]];
  selectedProvince:string = "All";
  connections:number;
  light:number;
  people:number;
  womenCustomers:number;
  jobs:any;// = {contracted:0,commission:0};
  // contractedJobs:number;
  // commisionJobs:number;
  co2Mitigated:number;
  investment:number = 54000000;
  installedPower:number;
  schoolsConnected:number;
  productivesConnected:number;

  tierParent:any;
  showPrinterFriendly:boolean = false;

  @ViewChild('zambiaChart') private zambiaChart: D3Component;
  @ViewChild('zambiaChartPrint') private zambiaChartPrint:D3Component;
  chartData: Array<any> = [
    {name: "Copperbelt", value: 0},
    {name: "Southern", value: 0},
    {name: "Eastern", value: 0},
    {name: "Northern", value: 0},
    {name: "Muchinga", value: 0},
    {name: "Luapula", value: 0},
    {name: "North-Western", value: 0},
    {name: "Western", value: 0},
    {name: "Lusaka", value: 0},
    {name: "Central", value: 0}
  ];
  rawData:any[];
  financialLeverageData:any[];

  espFilter:Filter;
  selectedESPs:string[];
  startDate:Date;
  endDate:Date;

  constructor(private appService:AppService, public router:Router, private externalDashboardService:ExternalDashboardService,
    private loadingService:LoadingService, private filterService:FilterService, public authService:AuthService) {

  }

  ngOnInit() {

    this.loadingService.show();
    this.externalDashboardService.getFinancialLeverageData().subscribe((data:any[])=>{
        this.financialLeverageData = data;
        this.loadInvestmentData(data);
      
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )
    this.externalDashboardService.getJobsData().subscribe((data:any[])=>{
        this.loadJobsData(data);
        this.loadingService.hide();

      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )

    // this.externalDashboardService.getBgfzImpactData()
    // .subscribe((data:any[])=>{
    //     this.loadingService.hide();
    //     this.rawData = data;
    //     this.generateData(this.rawData);
    //     this.setDashboardCells(this.rawData);
    //   },

    //   error=>{
    //     console.log(error);
    //     this.loadingService.hide();
    //   }
    // )

    // new call from endpoint


    this.externalDashboardService.getBgfzImpactDataMetrics()
    .subscribe((data:any[])=>{

        this.loadingService.hide();
        // this.rawData = data;

        this.demoSetDashboardCells(data);
        this.demogenerateData(data);
      },

      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )

  }

  ngAfterViewInit(){
    this.espFilter = this.filterService.getESPFilter();
    this.selectedESPs = this.espFilter.filters;
  }

  generateData(inputData:any[]) {
    this.chartData = [
      {name: "Copperbelt", value: 0},
      {name: "Southern", value: 0},
      {name: "Eastern", value: 0},
      {name: "Northern", value: 0},
      {name: "Muchinga", value: 0},
      {name: "Luapula", value: 0},
      {name: "North-Western", value: 0},
      {name: "Western", value: 0},
      {name: "Lusaka", value: 0},
      {name: "Central", value: 0}
    ]
    let _this = this
    let data = _.countBy(_.pluck(inputData,'province'),function(prvnc){
      return prvnc;
    })

    _.each(this.chartData,function(val){
      if(data[val.name]){
        val.value = data[val.name];
      }else
      {
        val.value = 0;
      }
    })
  }

  demogenerateData(inputData:any[]) {
    const province = inputData['province'];

    // this.chartData = [
    //   {name: "Copperbelt", value: province['Copperbelt'] || 16933},
    //   {name: "Southern", value: province['Southern'] || 8909},
    //   {name: "Eastern", value: province['Eastern'] || 14665},
    //   {name: "Northern", value: province['Northern'] || 5137},
    //   {name: "Muchinga", value: province['Muchinga'] || 4156},
    //   {name: "Luapula", value: province['Luapula'] || 4495},
    //   {name: "North-Western", value: province['"North-Western"'] || 8338},
    //   {name: "Western", value: province['Western'] || 4277},
    //   {name: "Lusaka", value: province['Lusaka'] || 7909},
    //   {name: "Central", value: province['Central'] || 15309}
    // ];

    this.chartData = [
      {name: "Copperbelt", value:  16933},
      {name: "Southern", value:  8909},
      {name: "Eastern", value: 14665},
      {name: "Northern", value:  5137},
      {name: "Muchinga", value:  4156},
      {name: "Luapula", value:  4495},
      {name: "North-Western", value: 8338},
      {name: "Western", value:  4277},
      {name: "Lusaka", value: 7909},
      {name: "Central", value:  15309}
    ];


  }

  setDashboardCells(data:any[]){
    let _this = this;
    if(data){
      this.connections = data.length;
      let customerTypes = _.groupBy(data,function(row){
        return row.productUse;
      });

      this.people = 0;
      this.light = 0;
      this.co2Mitigated = 0;
      this.installedPower = 0;
      _.each(data,function(row){
        _this.people += row['beneficiaryCoefficient'];
        _this.light += row['lightCoefficient'];
        _this.co2Mitigated += row['co2Coefficient'];
        _this.installedPower += row['wattCoefficient'];
      })

      this.installedPower;

      //this.contractedJobs = customerTypes['Institutional'].length;

      let genders = _.groupBy(data,function(row){
        return row.customerGender;
      });

      if(genders['F']){
        this.womenCustomers = genders['F'].length;
      }else{
        this.womenCustomers = 0;
      }

      let customerSubTypes = _.groupBy(data,function(row){
        if(row.productSubUse){
          return row.productSubUse;
        }else{
          return 'N/A';
        }
      })

      if(customerSubTypes['School']){
        this.schoolsConnected = customerSubTypes['School'].length;
      }else{
        this.schoolsConnected = 0;
      }

      //hardcoded value, totals are being manually reported by REEEP, once ESPs send us this data, we can get these numbers pragmattically. Adding the value here vs the html
      //because then it loads at the same time as the rest of the values
      this.productivesConnected=1461;
    }
  }

  demoSetDashboardCells(data:any[]){
    const res:any = data[0]
    
    
    let _this = this;
    if(data){
      // console.log(res)
      this.connections = res.connections || 90130;
      this.people = Math.floor(res.beneficiaryCoefficient) || 468676;
      this.light = Math.floor(res.lightCoefficient) || 238635;
      this.co2Mitigated = Math.floor(res.co2Coefficient) || 1577250;
      this.installedPower = Math.floor(res.wattCoefficient) || 1309925;
      // this.womenCustomers = data['genderCount']['F'] || 24470;
      this.womenCustomers =  24470;
      //hardcoded value, totals are being manually reported by REEEP, once ESPs send us this data, we can get these numbers pragmattically. Adding the value here vs the html
      //because then it loads at the same time as the rest of the values
      this.productivesConnected=1461;
    }
  }

  
  loadInvestmentData(data:any[]){
    let _this = this;
    this.investment = 0;
    _.each(data,function(row){
      _this.investment += row['amtFunding'];
    })
  }

  loadJobsData(data:any[]){
    let _this = this;
    this.jobs = {
      'Contracted':0,
      "Commission Based":0
    };
    _.each(data,function(row){
      _this.jobs[row['textJobType']] += row['nbrJobsCreated'];
    })
  }


  provinceClicked(e){
    if(e!='All'){
      this.selectedProvince = e.province.properties.CAPTION;
      this.processFilter(e);
    }else{
      this.selectedProvince = 'All';
      this.processFilter(e);
    }
  }

  processFilter(e){
    let filteredData = this.rawData;
    if(this.authService.isAdmin){
      let _this = this
      filteredData = _.filter(filteredData,function(row){return _.contains(_this.selectedESPs,row['espCompanyName'])});

      if(this.startDate){
        filteredData = _.filter(filteredData,function(row){
          let rowDate = new Date(row['dtAcquisition']);
          return rowDate >=_this.startDate;
        })
      }
      if(this.endDate){
        filteredData = _.filter(filteredData,function(row){
          let rowDate = new Date(row['dtAcquisition']);
          return rowDate<=_this.endDate;
        })
      }
      this.generateData(filteredData);
      this.zambiaChart.updateChart();
      if(this.zambiaChartPrint){
        this.zambiaChartPrint.updateChart();
      }
    }

    if(this.selectedProvince != 'All'){
      filteredData = _.where(filteredData,{province:this.selectedProvince});
    }

    // this.setDashboardCells(filteredData);

  }

  showPFModal(){
    this.showPrinterFriendly = true;
  }
  hidePFModal(){
    this.showPrinterFriendly = false;
  }

  getNgClassMap(){
    if(this.authService.isAdmin){
      return 'ui-grid-col-7'
    }else{
      return 'ui-grid-col-6'
    }
  }
  getNgClassCells(){
    if(this.authService.isAdmin){
      return 'ui-grid-col-5 dashboard-grid'
    }else{
      return 'ui-grid-col-6 dashboard-grid'
    }
  }

  subMetricPull(){
    // STEPS
    // need to add a flat file for prod and live version fo the API
    // but all calls will be directed to the dev server
    // if config  is  prod,  return prod metric data
    // if config  is dev,  return dev metric data
    const devMet = (devMetric)[0];
    const prodMet = (prodMetric)[0];

    // console.log(testDev)
    // console.log(prodDev)

    let metricData;
    PULL_CONFIG.ENV.name === "dev" ? metricData = devMet : metricData = prodMet;

    this.demogenerateData(metricData);
    this.demoSetDashboardCells(metricData);
    this.loadingService.hide();

  }
}
