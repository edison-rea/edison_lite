import { Component, OnInit } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import {AppService} from '../../app.service';
import {ExternalDashboardService} from '../external-dashboard.service';
import {TableColumn} from '../../shared/class/table.column.obj';
import {LoadingService} from '../../shared/service/loading-services';
import * as _ from 'underscore';

@Component({
  selector: 'app-financials',
  templateUrl: './financials.component.html',
  styleUrls: ['./financials.component.css']
})
export class FinancialsComponent implements OnInit {
  rawData:any[];
  plotlyData:any[];
  plotlyLayout:any;
  chartInfo:string = 'This chart displays overall financial inflows into BGFZ Energy Service Provider (ESP) operations in Zambia, as well as rough breakdown by type.'

  constructor(public router: Router, private appService:AppService, private externalDashboardService:ExternalDashboardService,
     private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    this.externalDashboardService.getFinancialLeverageData().subscribe((data:any[])=>{
        this.rawData = data;
        this.setPlotlyData(data);
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )

    let plotlyMargin;
    let plotlyOrientation;;
    if(window.innerWidth > 610){
      plotlyMargin = { l: 50, r: 50, b: 10, t: 20 };
      plotlyOrientation = "w";
    }else{
      plotlyMargin = { l: 5, r: 5, b: 10, t: 20 };
      plotlyOrientation = "h";
    }

    this.plotlyLayout = {
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: 'Investment',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: plotlyMargin,
      legend: {"orientation": plotlyOrientation}
    }
  }

  onPlotlyClick(e){
    console.log(e);
  }

  setPlotlyData(data){
    if(data.length>0){
      let financeTypes = {
        Equity:0,
        Debt:0,
        Grant:0,
        Other:0
      };

      _.each(data,function(row){
        financeTypes[row['financeType']] += row['amtFunding'];
      })
      console.log(financeTypes);

      this.plotlyData = [{
        values: [financeTypes['Equity'], financeTypes['Debt'],financeTypes['Grant'],financeTypes['Other']],
        labels: ['Equity', 'Debt','Grant','Other'],
        type: 'pie',
        hole: 0.4,
        text: ['$' + financeTypes['Equity'].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
              '$' + financeTypes['Debt'].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
              '$' + financeTypes['Grant'].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
              '$' + financeTypes['Other'].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")],
        //.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
        textposition: 'inside',
        hoverinfo: 'label',
        textfont: {
          color: 'white'
        }
      }]
    }else{
      this.plotlyData = []
    }
  }
}
