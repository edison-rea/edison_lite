import {NgModule} from '@angular/core';
import {RouterModule,Routes} from '@angular/router';

import {ExternalDashboardComponent} from './external-dashboard.component';
import { ExternalConnectionsComponent } from './external-connections/external-connections.component';
import { ExternalGenderComponent } from './external-gender/external-gender.component';
import { LightComponent } from './light/light.component';
import { PeopleComponent } from './people/people.component';
import { JobsComponent } from './jobs/jobs.component';
import { ClimateComponent } from './climate/climate.component';
import { FinancialsComponent } from './financials/financials.component';
import { PowerComponent } from './power/power.component';
import { EducationComponent } from './education/education.component';
import { ProductiveComponent} from "./productive/productive.component";

const appRoutes: Routes = [
  {
    path: '',
    component: ExternalDashboardComponent
  },
  {
    path: 'bgfz-impact',
    component: ExternalDashboardComponent
  },
  {
    path: 'connections',
    component: ExternalConnectionsComponent
  },
  {
    path: 'gender',
    component: ExternalGenderComponent
  },
  {
    path: 'light',
    component: LightComponent
  },
  {
    path: 'people',
    component: PeopleComponent
  },
  {
    path: 'jobs',
    component: JobsComponent
  },
  {
    path: 'climate',
    component: ClimateComponent
  },
  {
    path: 'financials',
    component: FinancialsComponent
  },
  {
    path: 'power',
    component: PowerComponent
  },
  {
    path: 'education',
    component: EducationComponent
  },
  {
    path: 'productive',
    component: ProductiveComponent
  }
];

@NgModule({
  imports:[
    RouterModule.forChild(
      appRoutes
    )
  ],
  exports:[
    RouterModule
  ],
  providers:[

  ]
})

export class ExternalDashboardRoutingModule{
  constructor(){

  }
}
