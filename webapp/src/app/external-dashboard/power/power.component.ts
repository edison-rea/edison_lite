import { Component, OnInit } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import {AppService} from '../../app.service';
import {Filter} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {ExternalDashboardService} from '../external-dashboard.service';
import {LoadingService} from '../../shared/service/loading-services';
import * as _ from 'underscore';

@Component({
  selector: 'app-power',
  templateUrl: './power.component.html',
  styleUrls: ['./power.component.css']
})
export class PowerComponent implements OnInit {
  dataFilters: Filter[]=[];
  rawData:any[];
  plotlyData:any[];
  plotlyLayout:any;
  chartInfo:string = 'This chart shows the estimated total power capacity in Watt (peak) of the ESS deployed under BGFZ';
  zambiaData: Array<any>;

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
      private externalDashboardService:ExternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    //REMOVED FOR DEMO, can add back after demo
    // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
    this.dataFilters.push(this.filterService.getTierFilter());
    //removed for demo
    // this.dataFilters.push(this.filterService.getCustomerTypeFilter());

    this.externalDashboardService.getPowerData().subscribe((data:any[])=>{
        console.log(data)
        this.rawData = data;
        this.setChartData(data);
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    );

    this.plotlyLayout = {
      title: 'Watts by Tier',
      showlegend: false,
      autosize: true,
      // xaxis: { title: 'Province' },
      yaxis: { title: 'Watts',zeroline: true },
      barmode: 'stack',
      hovermode:'closest'
      };
  }

  onPlotlyClick(e){
    console.log(e);
  }

  setChartData(data){
    let _this = this;

    this.zambiaData = [
      {name: "Copperbelt", value: 0},
      {name: "Southern", value: 0},
      {name: "Eastern", value: 0},
      {name: "Northern", value: 0},
      {name: "Muchinga", value: 0},
      {name: "Luapula", value: 0},
      {name: "North-Western", value: 0},
      {name: "Western", value: 0},
      {name: "Lusaka", value: 0},
      {name: "Central", value: 0}
    ];
    let zambiaTotal = {
      Copperbelt:0,
      Southern:0,
      Eastern:0,
      Northern:0,
      Muchinga:0,
      Luapula:0,
      'North-Western':0,
      Western:0,
      Lusaka:0,
      Central:0,
      Total:0
    };

    //let tiers = [1,2,3,4,5,6]
    //this.plotlyData = []
    let xData = ['Copperbelt','Southern','Eastern','Northern','Muchinga','Luapula','North-Western','Western','Lusaka','Central','Total'];

    //for(let i in tiers){
    //let tierData = data;
    let provinceTier = {
      Copperbelt:0,
      Southern:0,
      Eastern:0,
      Northern:0,
      Muchinga:0,
      Luapula:0,
      'North-Western':0,
      Western:0,
      Lusaka:0,
      Central:0,
      Total:0
    };

    _.each(data,function(row){
      provinceTier[row['province']] += row['wattCoefficient'];
      provinceTier['Total'] += row['wattCoefficient'];
      zambiaTotal[row['province']] += row['wattCoefficient'];
    })

    let yBase = [(provinceTier['Total'] - provinceTier['Copperbelt'])];

    _.each(xData,function(province){
      if(province != 'Copperbelt' && province != 'Total'){
        let yBaseVal = yBase[yBase.length - 1] - provinceTier[province];
        yBase.push(yBaseVal);
      }
    })

    let yTop = [];

    _.each(xData,function(province){
      yTop.push(provinceTier[province]);
    })

    let traceBase = {
      x:xData,
      y:yBase,
      name: 'Base ',
      type: 'bar',
      marker: {
        color: 'rgba(1,1,1,0.0)'
      },
    }

    let traceTop = {
      x:xData,
      y:yTop,
      name: 'Top ',
      type: 'bar',
      marker: {
        color: 'rgba(55,128,191,0.7)',
        line: {
          color: 'rgba(55,128,191,1.0)',
          width: 2
        }
      }
    }
    this.plotlyData=[traceBase,traceTop];


    _.each(this.zambiaData,function(row){
      row.value = zambiaTotal[row.name]
    })
  }

  // setChartData(data){
  //   let _this = this;

  //   this.zambiaData = [
  //     {name: "Copperbelt", value: 0},
  //     {name: "Southern", value: 0},
  //     {name: "Eastern", value: 0},
  //     {name: "Northern", value: 0},
  //     {name: "Muchinga", value: 0},
  //     {name: "Luapula", value: 0},
  //     {name: "North-Western", value: 0},
  //     {name: "Western", value: 0},
  //     {name: "Lusaka", value: 0},
  //     {name: "Central", value: 0}
  //   ];
  //   let zambiaTotal = {
  //     Copperbelt:0,
  //     Southern:0,
  //     Eastern:0,
  //     Northern:0,
  //     Muchinga:0,
  //     Luapula:0,
  //     'North-Western':0,
  //     Western:0,
  //     Lusaka:0,
  //     Central:0,
  //     Total:0
  //   };

  //   //let tiers = [1,2,3,4,5,6]
  //   //this.plotlyData = []
  //   let xData = ['Copperbelt','Southern','Eastern','Northern','Muchinga','Luapula','North-Western','Western','Lusaka','Central','Total'];

  //   //for(let i in tiers){
  //   //let tierData = data;
  //   let provinceTier = {
  //     Copperbelt:0,
  //     Southern:0,
  //     Eastern:0,
  //     Northern:0,
  //     Muchinga:0,
  //     Luapula:0,
  //     'North-Western':0,
  //     Western:0,
  //     Lusaka:0,
  //     Central:0,
  //     Total:0
  //   };

  //   _.each(data,function(row){
  //     provinceTier[row['province']] += row['wattCoefficient'];
  //     provinceTier['Total'] += row['wattCoefficient'];
  //     zambiaTotal[row['province']] += row['wattCoefficient'];
  //   })

  //   let yBase = [(provinceTier['Total'] - provinceTier['Copperbelt'])];

  //   _.each(xData,function(province){
  //     if(province != 'Copperbelt' && province != 'Total'){
  //       let yBaseVal = yBase[yBase.length - 1] - provinceTier[province];
  //       yBase.push(yBaseVal);
  //     }
  //   })

  //   let yTop = [];

  //   _.each(xData,function(province){
  //     yTop.push(provinceTier[province]);
  //   })

  //   let traceBase = {
  //     x:xData,
  //     y:yBase,
  //     name: 'Base ',
  //     type: 'bar',
  //     marker: {
  //       color: 'rgba(1,1,1,0.0)'
  //     },
  //   }

  //   let traceTop = {
  //     x:xData,
  //     y:yTop,
  //     name: 'Top ',
  //     type: 'bar',
  //     marker: {
  //       color: 'rgba(55,128,191,0.7)',
  //       line: {
  //         color: 'rgba(55,128,191,1.0)',
  //         width: 2
  //       }
  //     }
  //   }
  //   this.plotlyData=[traceBase,traceTop];


  //   _.each(this.zambiaData,function(row){
  //     row.value = zambiaTotal[row.name]
  //   })
  // }

  }
