import { Component, OnInit } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import {AppService} from '../../app.service';
import {Filter} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {ExternalDashboardService} from '../external-dashboard.service';
import {Tier} from '../../shared/class/tier.obj';
import {LoadingService} from '../../shared/service/loading-services';
import * as _ from 'underscore';

@Component({
  selector: 'app-light',
  templateUrl: './light.component.html',
  styleUrls: ['./light.component.css']
})
export class LightComponent implements OnInit {
  dataFilters: Filter[]=[];
  rawData:any[];
  plotlyData:any[];
  plotlyLayout:any;
  plotlyData2:any[];
  plotlyLayout2:any;
  chartInfo:string = 'Through modern energy service subscriptions provided via solar home systems and micro grid connectivity, BGFZ is helping Zambians replace traditional candles and lamps with high-output LED lights.';
  tierData:Tier[];
  tierParent:any;

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
      private externalDashboardService:ExternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    //REMOVED FOR DEMO, can add back after demo
    // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
    this.dataFilters.push(this.filterService.getTierFilter());
    //removed for demo
    // this.dataFilters.push(this.filterService.getCustomerTypeFilter());

    this.externalDashboardService.getLightData().subscribe((data:any[])=>{

      console.log(data)
      
        this.rawData = data;
        this.setChartData(data);
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )

    this.plotlyLayout = {
      title: 'Candles/Lamps Displaced by Tier',
      showlegend: true,
      autosize: true,
      // xaxis: { title: 'Province' },
      yaxis: { title: 'Candles/Lamps Displaced' },
      barmode: 'stack',
      hovermode:'closest'
      };
      this.plotlyLayout2 = {
        title: 'Total by Tier',
        annotations: [
          {
            font: {
              size: 14
            },
            showarrow: false,
            text: 'Displaced',
            x: 0.5,
            y: 0.5
          }
        ],
        margin: { l: 10, r: 10, b: 30, t: 50 },
        showlegend:false
      };
  }

  onPlotlyClick(e){
    console.log(e);
  }

  setChartData(data){
    let _this = this;

    //removed for demo
    // let tiers = [1,2,3,4,5,6];
    let tiers = [1,2,3];
    this.plotlyData = [];
    let pieTotals = [];
    let text = [];

    for(let i in tiers){
        let tierData = _.where(data,{tierLevel:tiers[i]});
        let provinceTier = {
          Copperbelt:0,
          Southern:0,
          Eastern:0,
          Northern:0,
          Muchinga:0,
          Luapula:0,
          'North-Western':0,
          Western:0,
          Lusaka:0,
          Central:0,
          Total:0
        };

        _.each(tierData,function(row){
          provinceTier[row['province']] += row['lightCoefficient'];
          provinceTier['Total'] += row['lightCoefficient'];
        })

        let trace = {
          x:['Copperbelt','Southern','Eastern','Northern','Muchinga','Luapula','North-Western','Western','Lusaka','Central','Total'],
          y:[
            provinceTier['Copperbelt'],
            provinceTier['Southern'],
            provinceTier['Eastern'],
            provinceTier['Northern'],
            provinceTier['Muchinga'],
            provinceTier['Luapula'],
            provinceTier['North-Western'],
            provinceTier['Western'],
            provinceTier['Lusaka'],
            provinceTier['Central']
          ],
          name: 'Tier ' + tiers[i].toString(),
          type: 'bar'
        }
        this.plotlyData.push(trace);
        pieTotals.push(provinceTier['Total']);
        text.push(provinceTier['Total'].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    }

    let tierLabels = [];
    _.each(tiers,function(tier){tierLabels.push('Tier ' + tier.toString())});

    this.plotlyData2 = [{
      values: pieTotals,
      labels: tierLabels,
      type: 'pie',
      hole: 0.3,
      text: text,
      textposition: 'inside',
      hoverinfo: 'label',
      textfont: {
        color: 'white'
      },
      marker: {
        colors: this.filterService.getPlotlyColors()
      }
    }];
  }
}
