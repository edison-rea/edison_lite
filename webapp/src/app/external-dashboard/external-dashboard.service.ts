import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EnvConfig } from '../shared/service/env.config.service';
import { Tier } from '../shared/class/tier.obj';

@Injectable()
export class ExternalDashboardService {

  constructor(private envConfig: EnvConfig, private http: Http) { }

  public getTierData = (): Observable<Tier[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Origin': '/' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'tier', options)
      .map((res: Response) => <Tier[]>res.json().payload);
  }

  // TODO: switch out the api call with a new data source
  public getBgfzImpactData = (): Observable<any[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    
    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact', options)
      .map((res: Response) => <any[]>res.json().payload);
  }

  public getBgfzImpactDataMetrics = (): Observable<any[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.envConfig.getDemoVariable('endPoint') + 'bgfz-impact')
      .map((res: Response) => <any[]>res.json());
  }

  public getConnectionData = (): Observable<any[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    // return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/connection', options)
    return this.http.get(this.envConfig.getDemoVariable('endPoint') + 'bgfz-impact/connection', options)
      .map((res: Response) => <any[]>res.json().payload);
  }

  public getFinancialLeverageData = (): Observable<any[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'financial-leverage', options)
      .map((res: Response) => <any[]>res.json().payload);
  }

  
  public getLightData = (): Observable<any[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    // return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/light', options)
    return this.http.get(this.envConfig.getDemoVariable('endPoint') + 'bgfz-impact/light', options)
      .map((res: Response) => <any[]>res.json().payload);
  }

  public getPeopleData = (): Observable<any[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    // return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/people', options)
    return this.http.get(this.envConfig.getDemoVariable('endPoint') + 'bgfz-impact/people', options)
      .map((res: Response) => <any[]>res.json().payload);
  }

  public getGenderData = (): Observable<any[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    // return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/gender', options)
    return this.http.get(this.envConfig.getDemoVariable('endPoint') + 'bgfz-impact/gender', options)
      .map((res: Response) => <any[]>res.json().payload);
  }

  public getClimateData = (): Observable<any[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    // return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/climate', options)
    return this.http.get(this.envConfig.getDemoVariable('endPoint') + 'bgfz-impact/climate', options)
      .map((res: Response) => <any[]>res.json().payload);
  }

  public getJobsData = (): Observable<any[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'jobs', options)
      .map((res: Response) => <any[]>res.json().payload);
  }

  public getPowerData = (): Observable<any[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    // return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/power', options)
    return this.http.get(this.envConfig.getDemoVariable('endPoint') + 'bgfz-impact/power', options)
      .map((res: Response) => <any[]>res.json().payload);
  }

  public getSchoolData = (): Observable<any[]> => {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'bgfz-impact/school', options)
      .map((res: Response) => <any[]>res.json().payload);
  }

}
