import { Component, OnInit } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import {AppService} from '../../app.service';
import {Filter} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {ExternalDashboardService} from '../external-dashboard.service';
import {LoadingService} from '../../shared/service/loading-services';
import * as _ from 'underscore';

@Component({
  selector: 'app-external-connections',
  templateUrl: './external-connections.component.html',
  styleUrls: ['./external-connections.component.css'],
})
export class ExternalConnectionsComponent implements OnInit {
  dataFilters: Filter[]=[];
  rawData:any[];
  plotlyData:any[];
  plotlyData2:any[];
  plotlyLayout:any;
  plotlyLayout2:any;
  chartInfo:string = 'An Energy Service Subscription (ESS) is a modern, high-quality energy connection meeting minimum criteria on quality, warranty, service and technical performance. A given ESS is allotted a “Tier” in accordance with the BGFZ Multi-Tier Matrix (see ';
  chartInfoLink:any = {route:'/info',text:'Information'}
  chartInfoAfter:string = ' for more on the tier system)'

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
    private externalDashboardService:ExternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    this.dataFilters.push(this.filterService.getCustomerGenderFilter());
    //REMOVED FOR DEMO, can add back after demo
    // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
    this.dataFilters.push(this.filterService.getTierFilter())
    //Removed for demo
    // this.dataFilters.push(this.filterService.getCustomerTypeFilter());
    //note: this was already commented out before demo!
    //this.dataFilters.push(this.filterService.getESPFilter());

    this.plotlyLayout = {
      title: 'Connections by Tier',
      showlegend: true,
      autosize: true,
      // xaxis: { title: 'Province' },
      yaxis: { title: 'Connections' },
      barmode: 'stack',
      hovermode:'closest'
      };
    this.plotlyLayout2 = {
      title: 'Total by Tier',
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: 'ESS',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: { l: 10, r: 10, b: 30, t: 50 },
      showlegend:false//,
      //legend: {"orientation": "h"}
    };


    this.externalDashboardService.getConnectionData().subscribe((data:any[])=>{
        this.rawData = data;

        console.log(data);
        

        this.setChartData(this.rawData);
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )
  }

  onPlotlyClick(e){
    console.log(e);
  }

  setChartData(data){
    let _this = this;
    //removed for demo
    // let tiers = [1,2,3,4,5,6]
    let tiers = [1,2,3]
    this.plotlyData = []
    let pieTotals = [];
    let text = [];

    for(let i in tiers){
        let tierData = _.where(data,{tierLevel:tiers[i]});
        let provinceTier = _.countBy(_.pluck(tierData,'province'),function(row){return row;});
        let trace = {
          x:['Copperbelt','Southern','Eastern','Northern','Muchinga','Luapula','North-Western','Western','Lusaka','Central'],
          y:[
            provinceTier['Copperbelt'],
            provinceTier['Southern'],
            provinceTier['Eastern'],
            provinceTier['Northern'],
            provinceTier['Muchinga'],
            provinceTier['Luapula'],
            provinceTier['North-Western'],
            provinceTier['Western'],
            provinceTier['Lusaka'],
            provinceTier['Central']
          ],
          name: 'Tier ' + tiers[i].toString(),
          type: 'bar'
        }
        pieTotals.push(tierData.length);
        text.push(tierData.length.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        this.plotlyData.push(trace);
    }

    let tierLabels = [];
    _.each(tiers,function(tier){tierLabels.push('Tier ' + tier.toString())});

    this.plotlyData2 = [{
      values: pieTotals,
      labels: tierLabels,
      type: 'pie',
      hole: 0.3,
      text: text,
      textposition: 'inside',//'inside',
      hoverinfo: 'label',
      textfont: {
        color: 'white'
      },
      marker: {
        colors: this.filterService.getPlotlyColors()
      }
    }];
  }
}
