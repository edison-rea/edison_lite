import { TestBed, inject } from '@angular/core/testing';

import { ExternalDashboardService } from './external-dashboard.service';

describe('ExternalDashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExternalDashboardService]
    });
  });

  it('should be created', inject([ExternalDashboardService], (service: ExternalDashboardService) => {
    expect(service).toBeTruthy();
  }));
});
