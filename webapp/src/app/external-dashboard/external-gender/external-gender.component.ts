import { Component, OnInit } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import {AppService} from '../../app.service';
import {Filter} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {ExternalDashboardService} from '../external-dashboard.service';
import {LoadingService} from '../../shared/service/loading-services';
import * as _ from 'underscore';

@Component({
  selector: 'app-external-gender',
  templateUrl: './external-gender.component.html',
  styleUrls: ['./external-gender.component.css']
})
export class ExternalGenderComponent implements OnInit {
  dataFilters: Filter[]=[];
  rawData:any[];
  plotlyData:any[];
  plotlyLayout:any;
  chartInfo:string = 'This chart is looking at the breakdown by gender of primary customers (not beneficiaries), which refers solely to the individual person under whose name the ESS has been/is being purchased. Note: Vitalite systems are not yet configured to supply gender, this data will be coming soon.'

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
      private externalDashboardService: ExternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    this.dataFilters.push(this.filterService.getProvinceFilter());
    //REMOVED FOR DEMO, can add back after demo
    // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
    this.dataFilters.push(this.filterService.getTierFilter());
    //Removed for demo
    // this.dataFilters.push(this.filterService.getCustomerTypeFilter());

    this.externalDashboardService.getGenderData().subscribe((data:any[])=>{
        this.rawData = data;
        this.setPlotlyData(data);
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )

    let plotlyMargin;
    let plotlyOrientation;;
    if(window.innerWidth > 610){
      plotlyMargin = { l: 50, r: 50, b: 10, t: 20 };
      plotlyOrientation = "w";
    }else{
      plotlyMargin = { l: 5, r: 5, b: 10, t: 20 };
      plotlyOrientation = "h";
    }

    this.plotlyLayout = {
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: 'Gender',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: plotlyMargin,
      legend: {"orientation": plotlyOrientation}
    }
  }

  onPlotlyClick(e){
    console.log(e);
  }

  setPlotlyData(data){
    if(data.length>0){

      for(let i = 0; i<data.length; i++){
        if(data[i]['customerGender']==null){
          data[i]['customerGender']='Not Provided';
        }
      }

      let genderData = _.countBy(_.pluck(data,'customerGender',function(gender){return gender;}));

      let text=[];
      if(genderData['M'] > 0) {
        text.push(genderData['M'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' ESS')
      }
      if (genderData['F'] > 0) {
        text.push(genderData['F'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' ESS');
      }
      if (genderData[null] > 0) {
        text.push(genderData['Not Provided'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' ESS');
      }
      
      this.plotlyData = [{
        values: [genderData['M'], genderData['F'], genderData['Not Provided']],
        labels: ['Male', 'Female', 'Not Provided'],
        type: 'pie',
        hole: 0.4,
        text: text,
        textposition: 'inside',//'inside',
        hoverinfo: 'label',
        // marker: {
        //   colors: ['#8C2A2F','#009944']
        // },
        textfont: {
          color: 'white'
        }
      }]
    }else{
      this.plotlyData = []
    }
  }
}
