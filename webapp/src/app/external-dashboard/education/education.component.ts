import { Component, OnInit } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import {AppService} from '../../app.service';
import {Filter} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {ExternalDashboardService} from '../external-dashboard.service';
import {LoadingService} from '../../shared/service/loading-services';
import * as _ from 'underscore';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {
  dataFilters: Filter[]=[];
  rawData:any[];
  plotlyData:any[];
  plotlyLayout:any;
  plotlyData2:any[];
  plotlyLayout2:any;
  chartInfo:string = 'This chart shows the number and breakdown of schools connected to modern energy under BGFZ';

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
    private externalDashboardService:ExternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    //REMOVED FOR DEMO, can add back after demo
    // this.dataFilters.push(this.filterService.getESSTechnologyFilter());
    this.dataFilters.push(this.filterService.getTierFilter());

    this.plotlyLayout = {
      title: 'Schools Connected by Tier',
      showlegend: true,
      autosize: true,
      yaxis: { title: 'Schools Connected' },
      barmode: 'stack',
      hovermode:'closest'
      };
      this.plotlyLayout2 = {
        title: 'Total by Tier',
        annotations: [
          {
            font: {
              size: 14
            },
            showarrow: false,
            text: 'Schools',
            x: 0.5,
            y: 0.5
          }
        ],
        margin: { l: 10, r: 10, b: 30, t: 50 },
        showlegend:false
      };
    this.externalDashboardService.getSchoolData().subscribe((data:any[])=>{
        console.log(data);
        this.rawData = data;
        this.setChartData(data);
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )
  }

  onPlotlyClick(e){
    console.log(e);
  }

  setChartData(data){
    let _this = this;
    //removed for demo
    // let tiers = [1,2,3,4,5,6];
    let tiers = [1,2,3];
    this.plotlyData = [];
    let pieTotals = [];
    let text = [];

    for(let i in tiers){
        let tierData = _.where(data,{tierLevel:tiers[i]});
        let provinceTier = _.countBy(_.pluck(tierData,'province'),function(row){return row;});
        let trace = {
          x:['Copperbelt','Southern','Eastern','Northern','Muchinga','Luapula','North-Western','Western','Lusaka','Central','Total'],
          y:[
            provinceTier['Copperbelt'],
            provinceTier['Southern'],
            provinceTier['Eastern'],
            provinceTier['Northern'],
            provinceTier['Muchinga'],
            provinceTier['Luapula'],
            provinceTier['North-Western'],
            provinceTier['Western'],
            provinceTier['Lusaka'],
            provinceTier['Central']
          ],
          name: 'Tier ' + tiers[i].toString(),
          type: 'bar'
        }
        this.plotlyData.push(trace);
        pieTotals.push(tierData.length);
        text.push(tierData.length.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    }

    let tierLabels = [];
    _.each(tiers,function(tier){tierLabels.push('Tier ' + tier.toString())});

    this.plotlyData2 = [{
      values: pieTotals,
      labels: tierLabels,
      type: 'pie',
      hole: 0.3,
      text: text,
      textposition: 'inside',
      hoverinfo: 'label',
      textfont: {
        color: 'white'
      },
      marker: {
        colors: this.filterService.getPlotlyColors()
      }
    }];
  }
}
