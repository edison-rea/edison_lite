import { Component,OnInit, OnChanges, AfterViewChecked, AfterViewInit } from '@angular/core';
import {Router,Params,ActivatedRoute, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/distinctUntilChanged';

import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
import {MenuItem} from 'primeng/components/common/api'
import { AuthService } from './auth.service';
import {FilterService} from './shared/service/filter.service';

import * as _ from 'underscore';

// This still has to be declared
declare var gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnChanges, AfterViewChecked, AfterViewInit {
  title = 'EDISON';
  showCookieLaw:boolean=true;

  msgs:Message[];
  items: MenuItem[];
  bgfzItem:MenuItem = {
    label: 'BGFZ Impact',
    icon: 'fa fa-gears',
    command: (click) => {this.router.navigate(['bfgz-impact']);}
  };
  infoItem:MenuItem = {
    label: 'Info',
    icon: 'fa fa-info-circle',
    command: (click) => {this.router.navigate(['info']);}
  };
  portfolioMonitoring: MenuItem = {
    label: 'Portfolio Monitoring',
    icon: 'fa fa-line-chart',
    command: (click) => {this.router.navigate(['portfolio-monitoring']);}
  }
  internalDashItem: MenuItem = {
    label: 'Risk Analysis',
    icon: 'fa fa-bar-chart',
    command: (click) => {this.router.navigate(['analytics']);}
  };
  adminItem: MenuItem = {
    label: 'Admin',
    icon: 'fa-database',
    items:[
      {
        label: 'User Management',
        command: (click) => {this.router.navigate(['admin/user-management']);}
      },
      {
        label: 'ESP Management',
        command: (click) => {this.router.navigate(['admin/esp-management']);}
      },
      {
        label: 'Data Management',
        command: (click) => {this.router.navigate(['admin/data-management']);}
      },
      {
        label: 'External API Log',
        command: (click) => {this.router.navigate(['admin/esp-api-log']);}
      }
    ]
  };

  constructor(private route: ActivatedRoute,private router:Router,private messageService: MessageService,public authService:AuthService,
    private filterService:FilterService){}

  ngOnInit() {
    this.authService.checkAuth();
    this.items = [this.bgfzItem,this.infoItem];
    this.cookieCheck();
    this.router.events.distinctUntilChanged((previous: any, current: any) => {
            // Subscribe to any `NavigationEnd` events where the url has changed
            if(current instanceof NavigationEnd) {
                return previous.url === current.url;
            }
            return true;
        }).subscribe((x: any) => {
            gtag('config', 'UA-112413205-1', {'page_path': x.url});
        });
    //gtag('config', 'UA-112413205-1');
  }

  ngOnChanges(){
  }

  ngAfterViewInit(){
    window.onload = () => { this.authService.setNewTimeOut(); };
    window.onmousemove = () => { this.authService.setNewTimeOut(); };
    window.onmousedown = () => { this.authService.setNewTimeOut(); };
    window.onclick = () => { this.authService.setNewTimeOut(); };
    window.onscroll = () => { this.authService.setNewTimeOut(); };
    window.onkeypress = () => { this.authService.setNewTimeOut(); };
  }

  ngAfterViewChecked(){
    if(this.authService.isLoggedIn){
      if(!_.contains(this.items,this.adminItem) && this.authService.isAdmin){
        this.items.splice(-1,0,this.adminItem);
      }
      if(!_.contains(this.items,this.internalDashItem) && this.authService.isAdmin){
        this.items.splice(-2,0,this.internalDashItem);
      }
      if(!_.contains(this.items,this.portfolioMonitoring)){
        this.items.splice(1,0,this.portfolioMonitoring);
      }
    }

    if(!this.authService.isLoggedIn){
      if(_.contains(this.items,this.portfolioMonitoring)){
        let index: number = this.items.indexOf(this.portfolioMonitoring);
        this.items.splice(index,1);
      }
      if(_.contains(this.items,this.adminItem)){
        let index: number = this.items.indexOf(this.adminItem);
        this.items.splice(index,1);
      }
      if(_.contains(this.items,this.internalDashItem)){
        let index: number = this.items.indexOf(this.internalDashItem);
        this.items.splice(index,1);
      }
    }

    if(!this.authService.isAdmin){
      if(_.contains(this.items,this.adminItem)){
        let index: number = this.items.indexOf(this.adminItem);
        this.items.splice(index,1);
      }
      if(_.contains(this.items,this.internalDashItem)){
        let index: number = this.items.indexOf(this.internalDashItem);
        this.items.splice(index,1);
      }
    }
  }

  showViaService() {
      this.messageService.add({severity:'success', summary:'Service Message', detail:'Via MessageService'});
  }
  login(){
    if(this.router.url!='/login'){
      this.authService.redirectUrl = this.router.url;
    }
    this.router.navigate(['/login']);
  }
  logout(){
    this.authService.logout();
    if(!this.authService.isLoggedIn){
      if(_.contains(this.items,this.portfolioMonitoring)){
        let index: number = this.items.indexOf(this.portfolioMonitoring);
        this.items.splice(index,1);
      }
      if(_.contains(this.items,this.adminItem)){
        let index: number = this.items.indexOf(this.adminItem);
        this.items.splice(index,1);
      }
      if(_.contains(this.items,this.internalDashItem)){
        let index: number = this.items.indexOf(this.internalDashItem);
        this.items.splice(index,1);
      }
    }
    this.router.navigate(['/bgfz-impact']);
  }

  cookieCheck(){
    if(localStorage.getItem('hide-cookie-law')){
      this.showCookieLaw = false;
    }
  }
  hideCookieLaw(){
    this.showCookieLaw = false;
    localStorage.setItem('hide-cookie-law','true');
  }
  readMore(){
    window.open('/privacy-statement');
  }
}
