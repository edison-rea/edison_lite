import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule,SystemJsNgModuleLoader } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import 'rxjs/add/operator/toPromise';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


import {MenubarModule} from 'primeng/components/menubar/menubar';
import {GrowlModule} from 'primeng/primeng';
import {MessageService} from 'primeng/components/common/messageservice';
import {ChartModule} from 'primeng/primeng';


import { HomeComponent } from './home/home.component';
//import { D3Component } from './d3/d3.component';
import {SharedModule} from './shared/shared.module';
import { AppService } from './app.service';
import { LoginComponent } from './login.component';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import {CaptchaModule} from 'primeng/primeng';
import { InfoComponent } from './appendix/info/info.component';
import { PrivacyStatementComponent } from './appendix/privacy-statement/privacy-statement.component';
import {_404Component} from './_404.component';
import {LoadingIndicator} from './loading-indicator';
import {LoadingService} from './shared/service/loading-services';
import {FilterService} from './shared/service/filter.service';
import { DisclosureComponent } from './appendix/disclosure/disclosure.component';

@NgModule({
  declarations: [
    AppComponent, HomeComponent, LoginComponent, InfoComponent, PrivacyStatementComponent,_404Component,
    LoadingIndicator, DisclosureComponent
  ],
  imports: [
    BrowserModule,HttpModule,BrowserAnimationsModule,AppRoutingModule,MenubarModule,GrowlModule,ChartModule,
    SharedModule.forRoot(),CaptchaModule
  ],
  providers: [SystemJsNgModuleLoader,MessageService,AppService,AuthGuard,AuthService,LoadingService,FilterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
