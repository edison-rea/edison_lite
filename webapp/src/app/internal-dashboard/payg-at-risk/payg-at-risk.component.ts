import { Component, OnInit, Input, Output, EventEmitter,
  OnChanges, SimpleChanges, SimpleChange,ViewChild } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import { CurrencyPipe,PercentPipe } from '@angular/common';
import {CustomHTMLElement} from '../../shared/class/custom.html.element.obj';
import {AppService} from '../../app.service';
import {Filter, FilterObj} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {InternalDashboardService} from '../internal-dashboard.service';
import {LoadingService} from '../../shared/service/loading-services';
import {DummyData} from '../../shared/class/dummy.names';

import * as _ from 'underscore';

declare var Plotly: any;

@Component({
  selector: 'app-payg-at-risk',
  templateUrl: './payg-at-risk.component.html',
  styleUrls: ['./payg-at-risk.component.css']
})
export class PaygAtRiskComponent implements OnInit {
  rawData:any[];
  subDashboardTitle:string = 'PAYG Performance: PAR30';
  showPrinterFriendly:boolean = false;
  parentDashboard:string = 'analytics';
  plotlyName:string='plotlyDiv';
  plotlyName2:string='plotlyDiv2';
  plotlyData:any[];
  plotlyLayout:any;
  plotlyData2:any[];
  plotlyLayout2:any;
  plotlyOptions:any = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'lasso2d',//Lasso Select
        'toggleSpikelines', //Toggle Spike
        'select2d', //Box Select
        'sendDataToCloud', //Save and Sent to Cloud
        'resetScale2d' //Reset axes
      ]
    };
  plotlyPrintOptions: any = {
    displayModeBar: false
  }
  _myPlot: CustomHTMLElement;
  tableData: any[];
  tabIndex:number = 0;

  espFilter:Filter;
  selectedEspFilters:string[];

  sellers:any[]=[];
  selectedSellers:string[];

  tierFilter:Filter;
  selectedTiers:string[];

  applicationFilter:Filter;
  selectedApplications:string[];

  paygDurationRange:number[];
  paygDurationMin:number;
  paygDurationMax:number;

  totalFinancedRange:number[];
  financedMin:number;
  financedMax:number;

  provinceFilter:Filter;
  selectedProvinces:string[];

  genderFilter:Filter;
  selectedGenders:string[];

  transactionTypeFilter:Filter;
  selectedTransTypes:string[];

  startDate:Date;
  endDate:Date;

  filtersReady:boolean = false;

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
    private internalDashboardService:InternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    this.internalDashboardService.getPar30Data().subscribe((data:any[])=>{
        this.rawData = data;
        console.log(data);
        this.tableData = this.rawData;
        this.setChartData(this.rawData);
        this.setSellerFilter(this.rawData);
        this.setSliderValues();
        this.filtersReady = true;
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )
    this.plotlyLayout = {
      title: '$ at Risk by Tier',
      showlegend: true,
      autosize: true,
      // xaxis: { title: 'Province' },
      yaxis: { title: '$ at Risk' },
      barmode: 'stack',
      hovermode:'closest'
    };
    this.plotlyLayout2 = {
      title: 'Total by Tier',
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: '$ at Risk',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: { l: 10, r: 10, b: 30, t: 50 },
      showlegend:false
    };

    this.espFilter = this.filterService.getESPFilter();
    this.selectedEspFilters = this.espFilter.filters;

    this.tierFilter = this.filterService.getTierFilter();
    this.selectedTiers = this.tierFilter.filters;

    this.applicationFilter = this.filterService.getCustomerTypeFilter();
    this.selectedApplications = this.applicationFilter.filters;

    this.provinceFilter = this.filterService.getProvinceFilter();
    this.selectedProvinces = this.provinceFilter.filters;

    this.genderFilter = this.filterService.getCustomerGenderFilter();
    this.selectedGenders = this.genderFilter.filters;

    this.transactionTypeFilter = this.filterService.getTransactionTypeFilter();
    this.selectedTransTypes = this.transactionTypeFilter.filters;
  }

  ngAfterViewInit(){
    if(this.plotlyData){
        Plotly.newPlot(this.plotlyName, this.plotlyData, this.plotlyLayout, this.plotlyOptions);
        Plotly.newPlot(this.plotlyName2, this.plotlyData2, this.plotlyLayout2, this.plotlyOptions);
    }

  }

  ngAfterViewChecked(){
    let _this = this;
    if(document.getElementById(this.plotlyName) && this.plotlyData){
      if(!this._myPlot){
        this._myPlot = <CustomHTMLElement>document.getElementById(this.plotlyName);
        let _this = this;
        this._myPlot.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.plotlyName)){
            Plotly.Plots.resize(_this._myPlot);
          }
        };
      }
    }
  }
  setChartData(data){
    if(data && data.length>0){
      let payg30Data = _.filter(data,function(row){return row['currDaysOutstanding'] >= 30})
      let _this = this;
      let tiers = [1,2,3,4,5,6];
      this.plotlyData = [];
      let pieTotals = [];
      let text = [];
      let totalAmtFunded = 0;

      _.each(data,function(row){
        totalAmtFunded += row['amtFinanced'];
      })

      for(let i in tiers){
          let tierData = _.where(payg30Data,{tierLevel:tiers[i]});
          let provinceTier = {
            Copperbelt:0,
            Southern:0,
            Eastern:0,
            Northern:0,
            Muchinga:0,
            Luapula:0,
            'North-Western':0,
            Western:0,
            Lusaka:0,
            Central:0,
            Total:0
          };

          _.each(tierData,function(row){
            provinceTier[row['province']] += row['amtFinanced'];
            provinceTier['Total'] += row['amtFinanced'];
          })

          let traceText = [
            provinceTier['Copperbelt'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              + '<br>' +  ((provinceTier['Copperbelt'] / totalAmtFunded) * 100).toFixed(2) + '%',
            provinceTier['Southern'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              + '<br>' +  ((provinceTier['Southern'] / totalAmtFunded) * 100).toFixed(2) + '%',
            provinceTier['Eastern'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              + '<br>' +  ((provinceTier['Eastern'] / totalAmtFunded) * 100).toFixed(2) + '%',
            provinceTier['Northern'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              + '<br>' +  ((provinceTier['Northern'] / totalAmtFunded) * 100).toFixed(2) + '%',
            provinceTier['Muchinga'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              + '<br>' +  ((provinceTier['Muchinga'] / totalAmtFunded) * 100).toFixed(2) + '%',
            provinceTier['Luapula'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              + '<br>' +  ((provinceTier['Luapula'] / totalAmtFunded) * 100).toFixed(2) + '%',
            provinceTier['North-Western'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              + '<br>' +  ((provinceTier['North-Western'] / totalAmtFunded) * 100).toFixed(2) + '%',
            provinceTier['Western'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              + '<br>' +  ((provinceTier['Western'] / totalAmtFunded) * 100).toFixed(2) + '%',
            provinceTier['Lusaka'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              + '<br>' +  ((provinceTier['Lusaka'] / totalAmtFunded) * 100).toFixed(2) + '%',
            provinceTier['Central'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              + '<br>' +  ((provinceTier['Central'] / totalAmtFunded) * 100).toFixed(2) + '%'
          ];

          let trace = {
            x:['Copperbelt','Southern','Eastern','Northern','Muchinga','Luapula','North-Western','Western','Lusaka','Central'],
            y:[
              provinceTier['Copperbelt'],
              provinceTier['Southern'],
              provinceTier['Eastern'],
              provinceTier['Northern'],
              provinceTier['Muchinga'],
              provinceTier['Luapula'],
              provinceTier['North-Western'],
              provinceTier['Western'],
              provinceTier['Lusaka'],
              provinceTier['Central']//,
              //provinceTier['Total']
            ],
            name: 'Tier ' + tiers[i].toString(),
            type: 'bar',
            text: traceText,
            hoverinfo:'text'
          }
          pieTotals.push(provinceTier['Total']);
          text.push(provinceTier['Total'].toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            + '<br>' +  ((provinceTier['Total'] / totalAmtFunded) * 100).toFixed(2) + '%');
          this.plotlyData.push(trace);
      }

      let tierLabels = [];
      _.each(tiers,function(tier){tierLabels.push('Tier ' + tier.toString())});

      this.plotlyData2 = [{
        values: pieTotals,
        labels: tierLabels,
        type: 'pie',
        hole: 0.3,
        text: text,
        textinfo: 'text',
        textposition: 'inside',
        hoverinfo: 'label',
        textfont: {
          color: 'white'
        },
        marker: {
          colors: this.filterService.getPlotlyColors()
        }
      }];
    }else{
      this.plotlyData = [];
      this.plotlyData2 = [{
        values: [],
        labels: [],
        type: 'pie',
        hole: 0.3
      }];
    }

    if(document.getElementById(this.plotlyName)){
      this.ngAfterViewInit();
    }
  }

  setSliderValues(){
    let minFinanced = 0;
    let maxFinanced = 1000;
    this.financedMin = minFinanced;
    this.financedMax = maxFinanced;
    this.totalFinancedRange=[minFinanced,maxFinanced];
    let minDuration = 0;
    let maxDuration = 48;
    if(minDuration % 6 > 0){
      let remainder = minDuration % 6;
      minDuration += (6 - remainder);
    }
    if(maxDuration % 6 > 0){
      let remainder = maxDuration % 6;
      maxDuration += (6 - remainder);
    }
    this.paygDurationMin = minDuration;
    this.paygDurationMax = maxDuration;
    this.paygDurationRange=[minDuration,maxDuration];
  }

  setSellerFilter(data:any){
    let _this = this;
    let sellerIds = _.uniq(_.pluck(data,'sellerId'));

    this.sellers = [];
    _.each(sellerIds,function(sellerId){
      _this.sellers.push({name:'Seller ' + sellerId, code:sellerId});
    })
  }

  filterData(){
    this.tableData = this.rawData;
    let _this = this;
    this.tableData = _.filter(this.tableData,function(row){
      return _.contains(_this.selectedEspFilters,row[_this.espFilter.physicalName])
          && _.contains(_this.selectedTiers,row[_this.tierFilter.physicalName].toString())
          && _.contains(_this.selectedApplications,row[_this.applicationFilter.physicalName])
          && ((row['planDuration']/30 >= _this.paygDurationRange[0] && row['planDuration']/30 <= _this.paygDurationRange[1]) || row['planDuration'] == null)
          && (row['amtFinanced'] >= _this.totalFinancedRange[0] && row['amtFinanced'] <= _this.totalFinancedRange[1])
          && _.contains(_this.selectedTransTypes,row[_this.transactionTypeFilter.physicalName])
          && _.contains(_this.selectedProvinces,row[_this.provinceFilter.physicalName])
          && _.contains(new FilterObj().getValuesOfSelectedFilters(_this.selectedGenders, _this.genderFilter),row[_this.genderFilter.physicalName]);
    })

    if(this.startDate){
      console.log(this.startDate);
      _this.tableData = _.filter(_this.tableData,function(row){
        let rowDate = new Date(row['dtAcquisition']);
        return rowDate >=_this.startDate;
      })
    }
    if(this.endDate){
      console.log(this.endDate);
      _this.tableData = _.filter(_this.tableData,function(row){
        let rowDate = new Date(row['dtAcquisition']);
        return rowDate<=_this.endDate;
      })
    }

    let filteredData = this.tableData;
    this.setSellerFilter(filteredData);

    if(this.selectedSellers){
      if(this.selectedSellers.length>0){
        let selectedSellerIds = _.pluck(_this.selectedSellers,'code');
        this.tableData = _.filter(this.tableData,function(row){
          return _.contains(selectedSellerIds,row['sellerId'].toString());
        })
      }
    }
    this.setChartData(this.tableData);
    this.ngAfterViewInit();
  }

  handleChange(e) {
    this.tabIndex = e.index;
    if(e.index == 0){
      Plotly.Plots.resize(this._myPlot);
    }
  }

  showPFModal(){
    this.showPrinterFriendly = true;
    Plotly.newPlot(this.plotlyName + 'Print', this.plotlyData, this.plotlyLayout, this.plotlyPrintOptions);
    Plotly.newPlot(this.plotlyName2 + 'Print', this.plotlyData2, this.plotlyLayout2, this.plotlyPrintOptions);
    Plotly.relayout(this.plotlyName + 'Print',{width:350})
    Plotly.relayout(this.plotlyName2 + 'Print',{width:350,height:350})
  }
  hidePFModal(){
    this.showPrinterFriendly = false;
  }

  getCSVFileName(){
    let today = new Date();
    let dateTimeStamp = today.getFullYear().toString()+(today.getMonth()+1).toString()+today.getDate().toString();
    return 'PAYG_Performance_PAR30_Data_' + dateTimeStamp;
  }

  processCheckBoxs(e){
    //console.log(e);
    this.filterData();
    if(document.getElementById(this.plotlyName)){
      Plotly.newPlot(this.plotlyName, this.plotlyData, this.plotlyLayout, this.plotlyOptions);
    }
  }

  getDummyName(index:any){
    let dummy = new DummyData()
    return dummy.getDummyName(index)
  }

}
