import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaygAtRiskComponent } from './payg-at-risk.component';

describe('PaygAtRiskComponent', () => {
  let component: PaygAtRiskComponent;
  let fixture: ComponentFixture<PaygAtRiskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaygAtRiskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaygAtRiskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
