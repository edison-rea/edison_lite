import { Component, OnInit, Input, Output, EventEmitter,
  OnChanges, SimpleChanges, SimpleChange,ViewChild } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import { CurrencyPipe,PercentPipe } from '@angular/common';
import {CustomHTMLElement} from '../../shared/class/custom.html.element.obj';
import {AppService} from '../../app.service';
import {Filter} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {InternalDashboardService} from '../internal-dashboard.service';
import {LoadingService} from '../../shared/service/loading-services';
//import {MultiSelectModule} from 'primeng/primeng';
import * as _ from 'underscore';

declare var Plotly: any;

@Component({
  selector: 'app-load-capacity',
  templateUrl: './load-capacity.component.html',
  styleUrls: ['./load-capacity.component.css']
})
export class LoadCapacityComponent implements OnInit {
  rawData:any[];
  subDashboardTitle:string = 'Microgrid Technical Performance: Percentage of total load capacity under contract';
  showPrinterFriendly:boolean = false;
  parentDashboard:string = 'analytics';
  plotlyName:string='plotlyDiv';
  plotlyData:any[];
  plotlyLayout:any;
  plotlyOptions:any = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'lasso2d',//Lasso Select
        'toggleSpikelines', //Toggle Spike
        'select2d', //Box Select
        'sendDataToCloud', //Save and Sent to Cloud
        'resetScale2d' //Reset axes
      ]
    };
  plotlyPrintOptions: any = {
    displayModeBar: false
  }
  _myPlot: CustomHTMLElement;
  tableData: any[];
  tabIndex:number = 0;

  provinceFilter:Filter;
  selectedProvinces:string[];

  sites:any[]=[];
  selectedSites:string[];

  startDate:Date;
  endDate:Date;

  plotlyReady:boolean = false;
  microgridData:any[];

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
    private internalDashboardService:InternalDashboardService, private loadingService:LoadingService) { }

    ngOnInit() {
      this.loadingService.show();
      this.internalDashboardService.getSiteData().subscribe((data:any[])=>{
          this.rawData = data;
          //this.tableData = data;
          this.setChartData(this.rawData);
          this.setSiteFilter(this.rawData);
          if(document.getElementById(this.plotlyName) && this.plotlyData){
            this.ngAfterViewInit();
          }
          this.loadingService.hide();
        },
        error=>{
          console.log(error);
          this.loadingService.hide();
        }
      )

      this.internalDashboardService.getMicrogridData().subscribe((data:any[])=>{
          this.microgridData = data;
          this.tableData = data;
        },
        error=>{
          console.log(error);
        }
      )

      this.plotlyLayout = {
        title: 'Percent of Total Load Capacity Under Contract',
        showlegend: true,
        autosize: true,
        xaxis:{title: "Time (months)"},
        yaxis:{hoverformat: '.2%', tickformat: '.0%', range: [0, 1], title: '% of Load Capacity Purchased'},
        type: "scatter",
        hovermode:'closest'
      };

      this.provinceFilter = this.filterService.getProvinceFilter();
      this.selectedProvinces = this.provinceFilter.filters;

      // this.siteFilter = this.filterService.getSiteFilter();
      // this.selectedSites = this.siteFilter.filters;
    }

    ngAfterViewInit(){
      if(this.plotlyData && this.plotlyReady){
        Plotly.newPlot(this.plotlyName, this.plotlyData, this.plotlyLayout, this.plotlyOptions);
      }
    }

    ngAfterViewChecked(){
      let _this = this;
      if(document.getElementById(this.plotlyName) && this.plotlyReady){
        if(!this._myPlot){
          this._myPlot = <CustomHTMLElement>document.getElementById(this.plotlyName);
          let _this = this;
          this._myPlot.on('plotly_click',function(data){
            console.log(data);
            return;
          });
          window.onresize = function() {
            if(_this.tabIndex == 0 && document.getElementById(_this.plotlyName)){
              Plotly.Plots.resize(_this._myPlot);
            }
          };
        }
      }
    }
    setChartData(data){
      let _this = this;
      let sites = _.uniq(_.pluck(data,'siteId'));
      this.plotlyData = [];

      sites = sites.slice(0,10);//Test Data has too many sites!! What if the real data has this many sites?!?! OMG FML

      _.each(sites,function(site){
        let siteData = _.where(data,{siteId:site});
        let dates:Date[] = _.uniq(_.pluck(siteData,'dtSiteWhAvg'));
        let xData = [];
        let capacity = siteData[0]['capacityKw'];
        let formattedDates = [];
        dates.sort();
        _.each(dates,function(date:Date){
          let siteDateData = _.where(siteData,{dtSiteWhAvg:date});
          let xVal = 0;
          let dt = new Date(date);
          formattedDates.push(dt);
          _.each(siteDateData,function(row){
            xVal += row['siteWhAvg']
          })
          xData.push(xVal / capacity);
        })
        let trace = {
          x: formattedDates,
          y: xData,
          type:'scatter',
          name: site.toString()
        }
        _this.plotlyData.push(trace);
      })
      this.plotlyReady = true;
      this.ngAfterViewInit();
      // aggregate all sites into an array representing % of load capacity purchased per month
    }

    setSiteFilter(data:any){
      let _this = this;
      let siteIds = _.uniq(_.pluck(_.filter(data,function(row){return row['siteId'] != null;}),'siteId'));
      this.sites = [];
      _.each(siteIds,function(siteId){
        _this.sites.push({name:'Site ' + siteId, code:siteId});
      });
    }

    filterData(){
      this.tableData = this.microgridData;
      let _this = this;
      this.tableData = _.filter(this.tableData,function(row){
        return _.contains(_this.selectedProvinces, row['nameProvince'])
            //&& _.contains(_this.selectedSites, row[_this.siteFilter.physicalName]);
      });

      let rawDataSite = this.rawData;

      if(this.selectedSites){
        if(this.selectedSites.length>0){
          let selectedSiteIds = _.pluck(_this.selectedSites,'code');
          this.tableData = _.filter(this.tableData,function(row){
            return _.contains(selectedSiteIds,row['extKey']);
          });
          rawDataSite = _.filter(rawDataSite,function(row){
            return _.contains(selectedSiteIds,row['siteId']);
          })
        }
      }

      if(this.startDate){
        rawDataSite = _.filter(rawDataSite,function(row){
          let rowDate = new Date(row['dtSiteWhAvg']);
          return rowDate >=_this.startDate;
        })
      }
      if(this.endDate){
        rawDataSite = _.filter(rawDataSite,function(row){
          let rowDate = new Date(row['dtSiteWhAvg']);
          return rowDate<=_this.endDate;
        })
      }

      this.setChartData(_.filter(rawDataSite,function(row){
        return _.contains(_this.selectedProvinces, row[_this.provinceFilter.physicalName])
            //&& _.contains(_this.selectedSites, row[_this.siteFilter.physicalName]);
      }));
    }

    handleChange(e) {
      this.tabIndex = e.index;
      if(e.index == 0){
        Plotly.Plots.resize(this._myPlot);
      }
    }

    showPFModal(){
      this.showPrinterFriendly = true;
      Plotly.newPlot(this.plotlyName + 'Print', this.plotlyData, this.plotlyLayout, this.plotlyPrintOptions);
    }
    hidePFModal(){
      this.showPrinterFriendly = false;
    }

    getCSVFileName(){
      let today = new Date();
      let dateTimeStamp = today.getFullYear().toString()+(today.getMonth()+1).toString()+today.getDate().toString();
      return 'Load_Capacity_Data_' + dateTimeStamp;
    }

    processCheckBoxs(e){
      //console.log(e);
      this.filterData();
      if(document.getElementById(this.plotlyName)){
        Plotly.newPlot(this.plotlyName, this.plotlyData, this.plotlyLayout, this.plotlyOptions);
      }
    }
}
