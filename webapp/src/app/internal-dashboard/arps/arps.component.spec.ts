import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArpsComponent } from './arps.component';

describe('ArpsComponent', () => {
  let component: ArpsComponent;
  let fixture: ComponentFixture<ArpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
