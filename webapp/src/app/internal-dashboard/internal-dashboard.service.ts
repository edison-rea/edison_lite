import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EnvConfig } from '../shared/service/env.config.service';

@Injectable()
export class InternalDashboardService {
  httpPrefix = this.envConfig.getEnvVariable('endPoint') + 'risk';
  headers = new Headers({'Content-Type':'application/json',
                        'Authorization':'Bearer ' + localStorage.getItem('access_token')})

  constructor(private envConfig:EnvConfig, private http:Http) { }

  public getRiskData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix,options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getRiskSellerData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/risk-seller',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getRiskMicrogridData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/risk-microgrid',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getMicrogridData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/microgrid',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getPar30Data = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/par30',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getDefaultData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/default',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getDefaultRateData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/default-rate',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getSalesPerSellerData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/sales-per-seller',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getHighRevenueCustomerData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/high-revenue-customer',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getSiteData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/site-smu-capacity',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getMicrogridHourlyData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/microgrid-hourly',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getARPUData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/arpu',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getCustomerClass = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'revenue-customer-class',options)
      .map((res:Response)=><any[]>res.json().payload);
  }
}
