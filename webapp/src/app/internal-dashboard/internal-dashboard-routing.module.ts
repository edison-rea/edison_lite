import {NgModule} from '@angular/core';
import {RouterModule,Routes} from '@angular/router';

import {InternalDashboardComponent} from './internal-dashboard.component';
import { PaygAtRiskComponent } from './payg-at-risk/payg-at-risk.component';
import { TotalDefaultsComponent } from './total-defaults/total-defaults.component';
import { DefautRateComponent } from './defaut-rate/defaut-rate.component';
import { SalesPerSellerComponent } from './sales-per-seller/sales-per-seller.component';
import { LoadCapacityComponent } from './load-capacity/load-capacity.component';
import { LoadFactorComponent } from './load-factor/load-factor.component';
import { ArpsComponent } from './arps/arps.component';
import { ArpuComponent } from './arpu/arpu.component';
import { HighRevenueCustomersComponent } from './high-revenue-customers/high-revenue-customers.component';

const appRoutes: Routes = [
  {
    path: '',
    component: InternalDashboardComponent
  },
  {
    path: 'analytics',
    component: InternalDashboardComponent
  },
  {
    path: 'payg-at-risk',
    component: PaygAtRiskComponent
  },
  {
    path: 'total-defaults',
    component: TotalDefaultsComponent
  },
  {
    path: 'default-rate',
    component: DefautRateComponent
  },
  {
    path: 'sales-per-seller',
    component: SalesPerSellerComponent
  },
  {
    path: 'load-capacity',
    component: LoadCapacityComponent
  },
  {
    path: 'load-factor',
    component: LoadFactorComponent
  },
  {
    path: 'arps',
    component: ArpsComponent
  },
  {
    path: 'arpu',
    component: ArpuComponent
  },
  {
    path: 'high-revenue-customers',
    component: HighRevenueCustomersComponent
  }
];

@NgModule({
  imports:[
    RouterModule.forChild(
      appRoutes
    )
  ],
  exports:[
    RouterModule
  ],
  providers:[

  ]
})

export class InternalDashboardRoutingModule{
  constructor(){

  }
}
