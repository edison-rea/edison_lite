import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighRevenueCustomersComponent } from './high-revenue-customers.component';

describe('HighRevenueCustomersComponent', () => {
  let component: HighRevenueCustomersComponent;
  let fixture: ComponentFixture<HighRevenueCustomersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighRevenueCustomersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighRevenueCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
