import { Component, OnInit, Input, Output, EventEmitter,
  OnChanges, SimpleChanges, SimpleChange,ViewChild } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import { CurrencyPipe,PercentPipe } from '@angular/common';
import {CustomHTMLElement} from '../../shared/class/custom.html.element.obj';
import {AppService} from '../../app.service';
import {Filter} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {InternalDashboardService} from '../internal-dashboard.service';
import {LoadingService} from '../../shared/service/loading-services';
import * as _ from 'underscore';
import {DummyData} from '../../shared/class/dummy.names';


declare var Plotly: any;

@Component({
  selector: 'app-high-revenue-customers',
  templateUrl: './high-revenue-customers.component.html',
  styleUrls: ['./high-revenue-customers.component.css']
})
export class HighRevenueCustomersComponent implements OnInit {
  rawData:any[];
  subDashboardTitle:string = 'High-Revenue Customers: Share Productive/Tier 3 ESS';
  showPrinterFriendly:boolean = false;
  parentDashboard:string = 'analytics';
  plotlyNameProductive:string='plotlyDivProductive';
  plotlyNameTier3:string='plotlyDivTier3';
  plotlyNameBoth:string='plotlyDivBoth';
  plotlyDataProductive:any[];
  plotlyDataTier3:any[];
  plotlyDataBoth:any[];
  plotlyLayoutProductive:any;
  plotlyLayoutTier3:any;
  plotlyLayoutBoth:any;
  plotlyOptions:any = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'lasso2d',//Lasso Select
        'toggleSpikelines', //Toggle Spike
        'select2d', //Box Select
        'sendDataToCloud', //Save and Sent to Cloud
        'resetScale2d' //Reset axes
      ]
    };
  plotlyPrintOptions: any = {
    displayModeBar: false
  }
  _myPlot: CustomHTMLElement;
  _myPlot2: CustomHTMLElement;
  _myPlot3: CustomHTMLElement;
  tableData: any[];
  tabIndex:number = 0;

  espFilter:Filter;
  selectedEspFilters:string[];

  provinceFilter:Filter;
  selectedProvinces:string[];

  sites:any[]=[];
  selectedSites:string[];

  startDate:Date;
  endDate:Date;

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
    private internalDashboardService:InternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    this.internalDashboardService.getHighRevenueCustomerData().subscribe((data:any[])=>{
        this.rawData = data;
        this.tableData = this.rawData;
        this.setChartData(_.where(this.rawData,{espCompanyName:'Standard Microgrid'}));
        this.setSiteFilter(this.rawData);
        if(document.getElementById(this.plotlyNameProductive)){
          this.ngAfterViewInit();
        }
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )
    this.plotlyLayoutProductive = {
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: 'Productive',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: { l: 50, r: 50, b: 10, t: 20 },
      showlegend:false
    };
    this.plotlyLayoutTier3 = {
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: 'Tier 3',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: { l: 50, r: 50, b: 10, t: 20 },
      showlegend:false
    };
    this.plotlyLayoutBoth = {
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: 'Combined',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: { l: 50, r: 50, b: 10, t: 20 },
      showlegend:false
    };

    this.espFilter = this.filterService.getESPFilter();
    this.selectedEspFilters = ['Standard Microgrid'];

    this.provinceFilter = this.filterService.getProvinceFilter();
    this.selectedProvinces = this.provinceFilter.filters;
  }

  ngAfterViewInit(){
    if(this.plotlyDataBoth){
      Plotly.newPlot(this.plotlyNameProductive, this.plotlyDataProductive, this.plotlyLayoutProductive, this.plotlyOptions);
      Plotly.newPlot(this.plotlyNameTier3, this.plotlyDataTier3, this.plotlyLayoutTier3, this.plotlyOptions);
      Plotly.newPlot(this.plotlyNameBoth, this.plotlyDataBoth, this.plotlyLayoutBoth, this.plotlyOptions);
    }
  }

  ngAfterViewChecked(){
    let _this = this;
    if(document.getElementById(this.plotlyNameProductive)
      && document.getElementById(this.plotlyNameTier3)
      && document.getElementById(this.plotlyNameBoth)
      && this.plotlyDataBoth){
      if(!this._myPlot && !this._myPlot2 && !this._myPlot3){
        this._myPlot = <CustomHTMLElement>document.getElementById(this.plotlyNameProductive);
        this._myPlot2 = <CustomHTMLElement>document.getElementById(this.plotlyNameTier3);
        this._myPlot3 = <CustomHTMLElement>document.getElementById(this.plotlyNameBoth);
        let _this = this;
        this._myPlot.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        this._myPlot2.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        this._myPlot3.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.plotlyNameProductive)){
            Plotly.Plots.resize(_this._myPlot);
            Plotly.Plots.resize(_this._myPlot2);
            Plotly.Plots.resize(_this._myPlot3);
          }
        };
      }
    }
  }
  setChartData(data){
    if(data.length>0){
      let productiveData = _.where(data,{productUse:'Productive'});
      this.plotlyDataProductive = this.setPlotlyDataFromFiltered(productiveData.length,data.length,'Productive');

      let tier3Data = _.filter(data,function(row){return row['tierLevel'] >= 3;});
      this.plotlyDataTier3 = this.setPlotlyDataFromFiltered(tier3Data.length,data.length,'Tier 3+');

      let bothData = _.filter(data,function(row){return (row['tierLevel'] >= 3 && row['productUse'] == 'Productive');});
      this.plotlyDataBoth = this.setPlotlyDataFromFiltered(bothData.length,data.length,'Both');
    }else{
      this.plotlyDataProductive = [{type:'pie'}];
      this.plotlyDataTier3 = [{type:'pie'}];
      this.plotlyDataBoth = [{type:'pie'}];
    }
  }

  setPlotlyDataFromFiltered(labelLength:number,allLength:number,label:string){
    let values = [labelLength,(allLength - labelLength)];
    let labels = [label,'Remainder'];

    let text = [label + '<br>' + labelLength.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' ESS',
        'Remainder<br>' + (allLength - labelLength).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' ESS'];

    return [{
      values: values,
      labels: labels,
      type: 'pie',
      hole: 0.4,
      textposition: 'inside',
      text:text,
      hoverinfo: 'label',
      textfont: {
        color: 'white'
      }
    }];
  }

  setSiteFilter(data:any){
    let _this = this;
    let siteIds = _.uniq(_.pluck(_.filter(data,function(row){return row['microgridId'] != null;}),'microgridId'));

    this.sites = [];
    _.each(siteIds,function(siteId){
      _this.sites.push({name:'Site ' + siteId, code:siteId});
    });
  }

  filterData(){
    this.tableData = this.rawData;
    let _this = this;
    this.tableData = _.filter(this.tableData,function(row){
      return _.contains(_this.selectedEspFilters,row[_this.espFilter.physicalName])
          && _.contains(_this.selectedProvinces,row[_this.provinceFilter.physicalName])
    });
    let filteredData = this.tableData;
    this.setSiteFilter(filteredData);

    if(this.selectedSites){
      if(this.selectedSites.length>0){
        let selectedSellerIds = _.pluck(_this.selectedSites,'code');
        this.tableData = _.filter(this.tableData,function(row){
          return _.contains(selectedSellerIds,row['microgridId']);
        })
      }
    }

    if(this.startDate){
      _this.tableData = _.filter(_this.tableData,function(row){
        let rowDate = new Date(row['dtConnection']);
        return rowDate >=_this.startDate;
      })
    }
    if(this.endDate){
      _this.tableData = _.filter(_this.tableData,function(row){
        let rowDate = new Date(row['dtConnection']);
        return rowDate<=_this.endDate;
      })
    }

    this.setChartData(this.tableData);
  }

  handleChange(e) {
    this.tabIndex = e.index;
    if(e.index == 0){
      Plotly.Plots.resize(this._myPlot);
      Plotly.Plots.resize(this._myPlot2);
      Plotly.Plots.resize(this._myPlot3);
    }
  }

  showPFModal(){
    this.showPrinterFriendly = true;
    let layoutProd = _.clone(this.plotlyLayoutProductive);
    let layoutTier3 = _.clone(this.plotlyLayoutTier3);
    let layoutBoth = _.clone(this.plotlyLayoutBoth);

    layoutProd['width'] = '215';
    layoutTier3['width'] = '215';
    layoutBoth['width'] = '215';

    layoutProd['height'] = '220';
    layoutTier3['height'] = '220';
    layoutBoth['height'] = '220';

    layoutProd['margin'] = { l: 5, r: 5, b: 5, t: 5 };
    layoutTier3['margin'] = { l: 5, r: 5, b: 5, t: 5 };
    layoutBoth['margin'] = { l: 5, r: 5, b: 5, t: 5 };
    //margin: { l: 50, r: 50, b: 10, t: 20 }

    Plotly.newPlot(this.plotlyNameProductive + 'Print', this.plotlyDataProductive, layoutProd, this.plotlyPrintOptions);
    Plotly.newPlot(this.plotlyNameTier3 + 'Print', this.plotlyDataTier3, layoutTier3, this.plotlyPrintOptions);
    Plotly.newPlot(this.plotlyNameBoth + 'Print', this.plotlyDataBoth, layoutBoth, this.plotlyPrintOptions);
  }
  hidePFModal(){
    this.showPrinterFriendly = false;
  }

  getCSVFileName(){
    let today = new Date();
    let dateTimeStamp = today.getFullYear().toString()+(today.getMonth()+1).toString()+today.getDate().toString();
    return 'High_Revenue_Costomer_Data_' + dateTimeStamp;
  }

  processCheckBoxs(e){
    this.filterData();
    if(document.getElementById(this.plotlyNameProductive)){
      Plotly.newPlot(this.plotlyNameProductive, this.plotlyDataProductive, this.plotlyLayoutProductive, this.plotlyOptions);
    }
    if(document.getElementById(this.plotlyNameTier3)){
      Plotly.newPlot(this.plotlyNameTier3, this.plotlyDataTier3, this.plotlyLayoutTier3, this.plotlyOptions);
    }
    if(document.getElementById(this.plotlyNameBoth)){
      Plotly.newPlot(this.plotlyNameBoth, this.plotlyDataBoth, this.plotlyLayoutBoth, this.plotlyOptions);
    }
  }

  getDummyName(index:any){
    let dummy = new DummyData()
    return dummy.getDummyName(index)
  }

}
