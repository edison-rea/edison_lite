import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPerSellerComponent } from './sales-per-seller.component';

describe('SalesPerSellerComponent', () => {
  let component: SalesPerSellerComponent;
  let fixture: ComponentFixture<SalesPerSellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesPerSellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesPerSellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
