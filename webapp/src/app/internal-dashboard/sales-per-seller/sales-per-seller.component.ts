import { Component, OnInit, Input, Output, EventEmitter,
  OnChanges, SimpleChanges, SimpleChange, ViewChild } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import { CurrencyPipe,PercentPipe } from '@angular/common';
import {CustomHTMLElement} from '../../shared/class/custom.html.element.obj';
import {AppService} from '../../app.service';
import {Filter, FilterObj} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {LoadingService} from  '../../shared/service/loading-services';
import {InternalDashboardService} from '../internal-dashboard.service';
//import {MultiSelectModule} from 'primeng/primeng';
import * as _ from 'underscore';
import {DummyData} from '../../shared/class/dummy.names';


declare var Plotly: any;

@Component({
  selector: 'app-sales-per-seller',
  templateUrl: './sales-per-seller.component.html',
  styleUrls: ['./sales-per-seller.component.css']
})

export class SalesPerSellerComponent implements OnInit {

  rawData:any[];
  subDashboardTitle:string = 'ESS sales per seller';
  showPrinterFriendly:boolean = false;
  parentDashboard:string = 'analytics';
  plotlyName:string='plotlyDiv';
  plotlyData:any[];
  plotlyLayout:any;
  plotlyOptions:any = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'lasso2d',//Lasso Select
        'toggleSpikelines', //Toggle Spike
        'select2d', //Box Select
        'sendDataToCloud', //Save and Sent to Cloud
        'resetScale2d' //Reset axes
      ]
    };
  plotlyPrintOptions: any = {
    displayModeBar: false
  }
  _myPlot: CustomHTMLElement;
  tableData: any[];
  tabIndex:number = 0;

  espFilter:Filter;
  selectedEspFilters:string[];

  sellers:any[]=[{
    label:['Seller 1', 'Seller 2'],
    value:['Seller 1', 'Seller 2']}];
  selectedSellers:string[];

  tierFilter:Filter;
  selectedTiers:string[];

  applicationFilter:Filter;
  selectedApplications:string[];

  paygDurationRange:number[];
  paygDurationMin:number;
  paygDurationMax:number;

  totalFinancedRange:number[];
  financedMin:number;
  financedMax:number;

  provinceFilter:Filter;
  selectedProvinces:string[];

  genderFilter:Filter;
  selectedGenders:string[];

  transactionTypeFilter:Filter;
  selectedTransTypes:string[];

  startDate:Date;
  endDate:Date;

  filtersReady:boolean = false;

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
    private internalDashboardService:InternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    this.internalDashboardService.getSalesPerSellerData().subscribe((data:any[])=>{
        this.rawData = data;
        this.tableData = this.rawData;
        this.setChartData(this.rawData);
        this.setSellerFilter(this.rawData);
        this.setSliderValues();
        this.filtersReady = true;
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    );

    this.plotlyLayout = {
      title: 'Sales per Seller',
      showlegend: false,
      autosize: true,
      xaxis: { title: 'ESP' },
      yaxis: { title: 'Sales ($)' },
      type: 'box'
    };

    this.espFilter = this.filterService.getESPFilter();
    this.selectedEspFilters = this.espFilter.filters;

    this.tierFilter = this.filterService.getTierFilter();
    this.selectedTiers = this.tierFilter.filters;

    this.applicationFilter = this.filterService.getCustomerTypeFilter();
    this.selectedApplications = this.applicationFilter.filters;

    this.provinceFilter = this.filterService.getProvinceFilter();
    this.selectedProvinces = this.provinceFilter.filters;

    this.genderFilter = this.filterService.getCustomerGenderFilter();
    this.selectedGenders = this.genderFilter.filters;

    this.transactionTypeFilter = this.filterService.getTransactionTypeFilter();
    this.selectedTransTypes = this.transactionTypeFilter.filters;
  }

  ngAfterViewInit(){
    if(this.plotlyData){
        Plotly.newPlot(this.plotlyName, this.plotlyData, this.plotlyLayout, this.plotlyOptions);
    }
  }

  ngAfterViewChecked(){
    let _this = this;
    if(document.getElementById(this.plotlyName) && this.plotlyData){
      if(!this._myPlot){
        this._myPlot = <CustomHTMLElement>document.getElementById(this.plotlyName);
        let _this = this;
        this._myPlot.on('plotly_click', function(data){
          console.log(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.plotlyName)){
            Plotly.Plots.resize(_this._myPlot);
          }
        };
      }
    }
  }
  setChartData(data){
    let _this = this;
    let esps = this.espFilter.filters;
    this.plotlyData = []

    for(let i in esps){
        let sellerData = _.where(data, {espCompanyName: esps[i].toString()});
        let sellers = _.countBy(sellerData,function(row){return row.sellerId});
        let sellerKeys = Object.keys(sellers);

        _.each(sellerKeys,function(key){
          sellers[key] = 0;
        })
        _.each(sellerData,function(row){
          sellers[row.sellerId] += row['amtFinanced'];
        })

        let sellerArray = [];
        _.each(sellerKeys,function(key){
          sellerArray.push(sellers[key]);
        })

        let trace = {
          y:sellerArray,
          name: esps[i].toString(),
          type: 'box',
          boxpoints: false
        }
        this.plotlyData.push(trace);
    }
    if(document.getElementById(this.plotlyName)){
      this.ngAfterViewInit();
    }
  }

  setSliderValues(){
    //hard coding min and max slider values vs having them calculated based on current data per user request
    // let financed = _.pluck(this.rawData,'amtFinanced');
    // let minFinanced = _.min(financed);
    // let maxFinanced = _.max(financed);
    let minFinanced = 0;
    let maxFinanced = 1000;
    this.financedMin = minFinanced;
    this.financedMax = maxFinanced;
    this.totalFinancedRange=[minFinanced,maxFinanced];
    // let duration = _.pluck(this.rawData,'planDuration');
    // let minDuration = _.min(duration);
    // let maxDuration = _.max(duration);
    let minDuration = 0;
    let maxDuration = 48;
    if(minDuration % 6 > 0){
      let remainder = minDuration % 6;
      minDuration += (6 - remainder);
    }
    if(maxDuration % 6 > 0){
      let remainder = maxDuration % 6;
      maxDuration += (6 - remainder);
    }
    this.paygDurationMin = minDuration;
    this.paygDurationMax = maxDuration;
    this.paygDurationRange=[minDuration,maxDuration];
  }

  setSellerFilter(data:any){
    let _this = this;
    let sellerIds = _.uniq(_.pluck(data,'sellerId'));

    this.sellers = [];
    _.each(sellerIds,function(sellerId){
      _this.sellers.push({name:'Seller ' + sellerId, code:sellerId});
    })
  }

  filterData(){
    this.tableData = this.rawData;
    let _this = this;
    this.tableData = _.filter(this.tableData,function(row){
      return _.contains(_this.selectedEspFilters,row[_this.espFilter.physicalName])
          && _.contains(_this.selectedTiers,row[_this.tierFilter.physicalName].toString())
          && _.contains(_this.selectedApplications,row[_this.applicationFilter.physicalName])
          && ((row['planDuration']/30 >= _this.paygDurationRange[0] && row['planDuration']/30 <= _this.paygDurationRange[1]) || row['planDuration'] == null)
          && (row['amtFinanced'] >= _this.totalFinancedRange[0] && row['amtFinanced'] <= _this.totalFinancedRange[1])
          && _.contains(_this.selectedTransTypes,row[_this.transactionTypeFilter.physicalName])
          && _.contains(_this.selectedProvinces,row[_this.provinceFilter.physicalName])
          && _.contains(new FilterObj().getValuesOfSelectedFilters(_this.selectedGenders, _this.genderFilter),row[_this.genderFilter.physicalName]);
    });

    if(this.startDate){
      _this.tableData = _.filter(_this.tableData,function(row){
        let rowDate = new Date(row['dtAcquisition']);
        return rowDate >=_this.startDate;
      })
    }
    if(this.endDate){
      _this.tableData = _.filter(_this.tableData,function(row){
        let rowDate = new Date(row['dtAcquisition']);
        return rowDate<=_this.endDate;
      })
    }

    let filteredData = this.tableData;
    this.setSellerFilter(filteredData);

    if(this.selectedSellers){
      if(this.selectedSellers.length>0){
        let selectedSellerIds = _.pluck(_this.selectedSellers,'code');
        this.tableData = _.filter(this.tableData,function(row){
          return _.contains(selectedSellerIds,row['sellerId'].toString());
        })
      }
    }
    this.setChartData(this.tableData);
    this.ngAfterViewInit();
  }

  handleChange(e) {
    this.tabIndex = e.index;
    if(e.index == 0){
      Plotly.Plots.resize(this._myPlot);
    }
  }

  showPFModal(){
    this.showPrinterFriendly = true;
    Plotly.newPlot(this.plotlyName + 'Print', this.plotlyData, this.plotlyLayout, this.plotlyPrintOptions);
  }
  hidePFModal(){
    this.showPrinterFriendly = false;
  }

  getCSVFileName(){
    let today = new Date();
    let dateTimeStamp = today.getFullYear().toString() + (today.getMonth() + 1).toString() + today.getDate().toString();
    return 'ESS_sales_per_seller_Data_' + dateTimeStamp;
  }

  processCheckBoxs(e){
    //console.log(e);
    this.filterData();
    if(document.getElementById(this.plotlyName)){
      Plotly.newPlot(this.plotlyName, this.plotlyData, this.plotlyLayout, this.plotlyOptions);
    }
  }

  getDummyName(index:any){
    let dummy = new DummyData()
    return dummy.getDummyName(index)
  }

}
