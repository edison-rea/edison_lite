import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalDefaultsComponent } from './total-defaults.component';

describe('TotalDefaultsComponent', () => {
  let component: TotalDefaultsComponent;
  let fixture: ComponentFixture<TotalDefaultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalDefaultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalDefaultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
