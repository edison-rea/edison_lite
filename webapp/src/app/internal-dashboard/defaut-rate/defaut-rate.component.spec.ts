import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefautRateComponent } from './defaut-rate.component';

describe('DefautRateComponent', () => {
  let component: DefautRateComponent;
  let fixture: ComponentFixture<DefautRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefautRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefautRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
