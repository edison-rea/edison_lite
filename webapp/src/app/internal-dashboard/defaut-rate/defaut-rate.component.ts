import { Component, OnInit, Input, Output, EventEmitter,
  OnChanges, SimpleChanges, SimpleChange,ViewChild } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import { CurrencyPipe,PercentPipe } from '@angular/common';
import {CustomHTMLElement} from '../../shared/class/custom.html.element.obj';
import {AppService} from '../../app.service';
import {Filter, FilterObj} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {InternalDashboardService} from '../internal-dashboard.service';
import {LoadingService} from '../../shared/service/loading-services';
//import {MultiSelectModule} from 'primeng/primeng';
import * as _ from 'underscore';
import {DummyData} from '../../shared/class/dummy.names';


declare var Plotly: any;

@Component({
  selector: 'app-defaut-rate',
  templateUrl: './defaut-rate.component.html',
  styleUrls: ['./defaut-rate.component.css']
})
export class DefautRateComponent implements OnInit {
  rawData:any[];
  subDashboardTitle:string = 'PAYG Performance: ESS default rates';
  showPrinterFriendly:boolean = false;
  parentDashboard:string = 'analytics';
  plotlyName:string='plotlyDiv';
  plotlyData:any[];
  plotlyLayout:any;
  plotlyName2:string='plotlyDiv2';
  plotlyData2:any[];
  plotlyLayout2:any;
  plotlyOptions:any = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'lasso2d',//Lasso Select
        'toggleSpikelines', //Toggle Spike
        'select2d', //Box Select
        'sendDataToCloud', //Save and Sent to Cloud
        'resetScale2d' //Reset axes
      ]
    };
  plotlyPrintOptions: any = {
    displayModeBar: false
  }
  _myPlot: CustomHTMLElement;
  tableData: any[];
  tabIndex:number = 0;

  espFilter:Filter;
  selectedEspFilters:string[];

  sellers:any[]=[{label:'Seller 1', value:'Seller 1'}];
  selectedSellers:string[];

  tierFilter:Filter;
  selectedTiers:string[];

  applicationFilter:Filter;
  selectedApplications:string[];

  paygDurationRange:number[];
  paygDurationMin:number;
  paygDurationMax:number;

  totalFinancedRange:number[];
  financedMin:number;
  financedMax:number;

  provinceFilter:Filter;
  selectedProvinces:string[];

  genderFilter:Filter;
  selectedGenders:string[];

  transactionTypeFilter:Filter;
  selectedTransTypes:string[];

  startDate:Date;
  endDate:Date;

  filtersReady:boolean = false;

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
    private internalDashboardService:InternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    this.internalDashboardService.getDefaultRateData().subscribe((data:any[])=>{
        this.rawData = data;
        this.tableData = data;
        this.setChartData(this.rawData);
        this.setSellerFilter(this.rawData);
        this.setSliderValues();
        this.filtersReady = true;
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )
    this.plotlyLayout = {
      title: 'Default Rate by Tier',
      showlegend: true,
      autosize: true,
      xaxis: { 
        // ticks:'',
        showticklabels: false
      },
      yaxis:{hoverformat: '.2%', tickformat: '.0%', title: 'Defaults'},
      barmode: 'stack',
      hovermode:'closest'
    };
    this.plotlyLayout2 = {
      title: 'Total by Tier',
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: 'Default Rate',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: { l: 10, r: 10, b: 30, t: 50 },
      showlegend:false
    };

    this.espFilter = this.filterService.getESPFilter();
    if(this.espFilter.filters.indexOf('Standard Microgrid')>=0) {
      this.espFilter.filters.splice(this.espFilter.filters.indexOf('Standard Microgrid'), 1);
    }
    this.selectedEspFilters = this.espFilter.filters;

    this.tierFilter = this.filterService.getTierFilter();
    this.selectedTiers = this.tierFilter.filters;

    this.applicationFilter = this.filterService.getCustomerTypeFilter();
    this.selectedApplications = this.applicationFilter.filters;

    this.provinceFilter = this.filterService.getProvinceFilter();
    this.selectedProvinces = this.provinceFilter.filters;

    this.genderFilter = this.filterService.getCustomerGenderFilter();
    this.selectedGenders = this.genderFilter.filters;

    this.transactionTypeFilter = this.filterService.getTransactionTypeFilter();
    this.selectedTransTypes = this.transactionTypeFilter.filters;
  }

  ngAfterViewInit(){
    if(this.plotlyData){
        Plotly.newPlot(this.plotlyName, this.plotlyData, this.plotlyLayout, this.plotlyOptions);
        Plotly.newPlot(this.plotlyName2, this.plotlyData2, this.plotlyLayout2, this.plotlyOptions);
    }

  }

  ngAfterViewChecked(){
    let _this = this;
    if(document.getElementById(this.plotlyName) && this.plotlyData){
      if(!this._myPlot){
        this._myPlot = <CustomHTMLElement>document.getElementById(this.plotlyName);
        let _this = this;
        this._myPlot.on('plotly_click',function(data){
          // console.log(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.plotlyName)){
            Plotly.Plots.resize(_this._myPlot);
          }
        };
      }
    }
  }
  setChartData(data){
    // console.log(data);
    let _this = this;
    let tiers
    if(this.selectedTiers){
      tiers = this.selectedTiers;
    }else{
      tiers = [1,2,3,4,5,6];
    }
    let pieTotals = [];
    let text = [];

    this.plotlyData = [];

    for(let i in tiers){
        let tierData = _.where(data,{tierLevel:parseInt(tiers[i])});
        let tierDefaultData = _.filter(tierData, function(row){
          return (row.paymentStatus==="Default" || row.currDaysOutstanding>=90 || row.repossession==true);
        });

        let espTier = _.countBy(_.pluck(tierData,'espCompanyName'),function(row){return row;});
        // console.log("esp = ");
        // console.log(espTier);
        let espDefaultTier = _.countBy(_.pluck(tierDefaultData,'espCompanyName'),function(row){return row;});
        // console.log("espDefaultTier = ");
        // console.log(espDefaultTier);
        let x: string[] = [];
        let y: any[] = [];

        let espKeys: string[] = Object.keys(espTier);
        espKeys.sort();
        // console.log("espKeys");
        // console.log(espKeys);

        _.each(espKeys,function(key){
          if(espDefaultTier[key] / data.length>0){
          x.push(key);
          y.push(espDefaultTier[key] / data.length);
          // console.log("x: " + key + " y: " + espDefaultTier[key] + "/" + data.length);
            }
        });

        let trace = {
          x:x,
          y:y,
          name: 'Tier ' + tiers[i].toString(),
          type: 'bar',
          hoverinfo: "y"
        };
        this.plotlyData.push(trace);

        pieTotals.push(tierDefaultData.length / data.length);
    }

    let tierLabels = [];
    _.each(tiers,function(tier){tierLabels.push('Tier ' + tier.toString())});

    this.plotlyData2 = [{
      values: pieTotals,
      labels: tierLabels,
      type: 'pie',
      hole: 0.4,
      textposition: 'inside',
      hoverinfo: 'label',
      textfont: {
        color: 'white'
      },
      marker: {
        colors: this.filterService.getPlotlyColors()
      }
    }];
    if(document.getElementById(this.plotlyName)){
      this.ngAfterViewInit();
    }
  }

  setSliderValues(){
    let minFinanced = 0;
    let maxFinanced = 1000;
    this.financedMin = minFinanced;
    this.financedMax = maxFinanced;
    this.totalFinancedRange=[minFinanced,maxFinanced];
    let minDuration = 0;
    let maxDuration = 48;
    if(minDuration % 6 > 0){
      let remainder = minDuration % 6;
      minDuration += (6 - remainder);
    }
    if(maxDuration % 6 > 0){
      let remainder = maxDuration % 6;
      maxDuration += (6 - remainder);
    }
    this.paygDurationMin = minDuration;
    this.paygDurationMax = maxDuration;
    this.paygDurationRange=[minDuration,maxDuration];
  }

  setSellerFilter(data:any){
    let _this = this;
    let sellerIds = _.uniq(_.pluck(_.where(data,{paymentStatus:'Default'}),'sellerId'));

    this.sellers = [];
    _.each(sellerIds,function(sellerId){
      _this.sellers.push({name:'Seller ' + sellerId, code:sellerId});
    })
  }

  filterData(){
    this.tableData = this.rawData;
    let _this = this;
    this.tableData = _.filter(this.tableData,function(row){
      return _.contains(_this.selectedEspFilters,row[_this.espFilter.physicalName])
          && _.contains(_this.selectedTiers,row[_this.tierFilter.physicalName].toString())
          && _.contains(_this.selectedApplications,row[_this.applicationFilter.physicalName])
          && ((row['planDuration']/30 >= _this.paygDurationRange[0] && row['planDuration']/30 <= _this.paygDurationRange[1]) || row['planDuration'] == null)
          && (row['amtFinanced'] >= _this.totalFinancedRange[0] && row['amtFinanced'] <= _this.totalFinancedRange[1])
          && _.contains(_this.selectedTransTypes,row[_this.transactionTypeFilter.physicalName])
          && _.contains(_this.selectedProvinces,row[_this.provinceFilter.physicalName])
          && _.contains(new FilterObj().getValuesOfSelectedFilters(_this.selectedGenders, _this.genderFilter),row[_this.genderFilter.physicalName]);
    })

    if(this.startDate){
      _this.tableData = _.filter(_this.tableData,function(row){
        let rowDate = new Date(row['dtAcquisition']);
        return rowDate >=_this.startDate;
      })
    }
    if(this.endDate){
      _this.tableData = _.filter(_this.tableData,function(row){
        let rowDate = new Date(row['dtAcquisition']);
        return rowDate<=_this.endDate;
      })
    }

    let filteredData = this.tableData;
    this.setSellerFilter(filteredData);

    if(this.selectedSellers){
      if(this.selectedSellers.length>0){
        let selectedSellerIds = _.pluck(_this.selectedSellers,'code');
        this.tableData = _.filter(this.tableData,function(row){
          return _.contains(selectedSellerIds,row['sellerId'].toString());
        })
      }
    }

    this.setChartData(this.tableData);
    this.ngAfterViewInit();
  }

  handleChange(e) {
    this.tabIndex = e.index;
    if(e.index == 0){
      Plotly.Plots.resize(this._myPlot);
    }
  }

  showPFModal(){
    this.showPrinterFriendly = true;
    Plotly.newPlot(this.plotlyName + 'Print', this.plotlyData, this.plotlyLayout, this.plotlyPrintOptions);
    Plotly.newPlot(this.plotlyName2 + 'Print', this.plotlyData2, this.plotlyLayout2, this.plotlyPrintOptions);
    Plotly.relayout(this.plotlyName + 'Print',{width:350})
    Plotly.relayout(this.plotlyName2 + 'Print',{width:350,height:350})
  }
  hidePFModal(){
    this.showPrinterFriendly = false;
  }

  getCSVFileName(){
    let today = new Date();
    let dateTimeStamp = today.getFullYear().toString()+(today.getMonth()+1).toString()+today.getDate().toString();
    return 'PAYG_Performance_Default_Rate_Data_' + dateTimeStamp;
  }

  processCheckBoxs(e){
    //console.log(e);
    this.filterData();
    if(document.getElementById(this.plotlyName)){
      Plotly.newPlot(this.plotlyName, this.plotlyData, this.plotlyLayout, this.plotlyOptions);
    }
  }

  getDummyName(index:any){
    let dummy = new DummyData()
    return dummy.getDummyName(index)
  }

}
