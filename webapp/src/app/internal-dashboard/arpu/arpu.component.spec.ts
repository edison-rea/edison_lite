import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArpuComponent } from './arpu.component';

describe('ArpuComponent', () => {
  let component: ArpuComponent;
  let fixture: ComponentFixture<ArpuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArpuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArpuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
