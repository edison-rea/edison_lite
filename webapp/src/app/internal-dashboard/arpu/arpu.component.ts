import { Component, OnInit, Input, Output, EventEmitter,
  OnChanges, SimpleChanges, SimpleChange,ViewChild } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import {CustomHTMLElement} from '../../shared/class/custom.html.element.obj';
import {AppService} from '../../app.service';
import {Filter} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {InternalDashboardService} from '../internal-dashboard.service';
import {LoadingService} from '../../shared/service/loading-services';
import {D3Component} from '../../shared/d3/d3.component';
import * as _ from 'underscore';
import {ProvinceEnum} from "../../shared/enum/enums.model";

declare var Plotly: any;
@Component({
  selector: 'app-arpu',
  templateUrl: './arpu.component.html',
  styleUrls: ['./arpu.component.css']
})
export class ArpuComponent implements OnInit {
  @ViewChild('zambiaChart') private zambiaChart: D3Component;
  @ViewChild('zambiaChartPrint') private zambiaChartPrint:D3Component;
  chartData: Array<any>;
  pointCoords:any[];
  rawData:any[];
  customerClass:any[];
  subDashboardTitle:string = 'Microgrid Technical Performance: Load Factor';
  showPrinterFriendly:boolean = false;
  parentDashboard:string = 'analytics';
  plotlyBoxName1:string='plotlyBoxDiv1';
  plotlyBoxData1:any[];
  plotlyBoxLayout1:any;
  plotlyOptions:any = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'lasso2d',//Lasso Select
        'toggleSpikelines', //Toggle Spike
        'select2d', //Box Select
        'sendDataToCloud', //Save and Sent to Cloud
        'resetScale2d' //Reset axes
      ]
  };
  plotlyBoxName2:string='plotlyBoxDiv2';
  plotlyBoxData2:any[];
  plotlyBoxLayout2:any;
  plotlyPieName:string='plotlyPieDiv';
  plotlyPieData:any[];
  plotlyPieLayout:any;
  plotlyPrintOptions: any = {
    displayModeBar: false
  };
  _boxPlot1: CustomHTMLElement;
  _boxPlot2: CustomHTMLElement;
  _piePlot: CustomHTMLElement;
  tableData: any[];
  tabIndex:number = 0;

  sites:any[]=[];
  selectedSites:string[];
  provinceFilter:Filter;
  selectedProvinces:string[];
  tierFilter:Filter;
  selectedTiers:string[];
  customerTypeFilter:Filter;
  selectedCustomerTypes:string[];
  startDate:Date;
  endDate:Date;

  plotlyBox1Ready:boolean = false;
  plotlyBox2Ready:boolean = false;
  microgridData:any[];

  lowDataCell:number;
  lowCaption:string;
  midDataCell:number;
  midCaption:string;
  highDataCell:number;
  highCaption:string;

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
    private internalDashboardService:InternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    this.internalDashboardService.getARPUData().subscribe((data:any[])=>{ // We need to update this with the load-factor data service
        this.rawData = data;
        this.setChartData(this.rawData);
        this.generateData(this.rawData);
        this.setSiteFilter(data);
        if(document.getElementById(this.plotlyBoxName1) && this.plotlyBoxData1 &&
          document.getElementById(this.plotlyBoxName2) && this.plotlyBoxData2){
          this.ngAfterViewInit();
        }
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    );

    this.internalDashboardService.getMicrogridData().subscribe((data:any[])=>{
        this.microgridData = data;
        this.tableData = data;
        this.setZambiaCoordinates(data);
      },
      error=>{
        console.log(error);
      }
    );

    this.internalDashboardService.getCustomerClass().subscribe((data:any[])=>{
        this.customerClass = data;
      },
      error=>{
        console.log(error);
      }
    );

    this.plotlyBoxLayout1 = {
      title: 'ARPU',
      showlegend: false,
      autosize: true,
      // xaxis:{title: "Microgrid"},
      yaxis:{title: 'Average Revenue per User/Site in USD'},
      type: "box"
    };

    this.plotlyBoxLayout2 = {
      title: 'ARPS',
      showlegend: false,
      autosize: true,
      // xaxis:{title: "Microgrid"},
      yaxis:{title: 'Average Revenue per User/Site in USD'},
      type: "box"
    };

    this.plotlyPieLayout = {
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: 'Customers',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: { l: 30, r: 30, b: 30, t: 30 },
      showlegend:false
    };

    this.provinceFilter = this.filterService.getProvinceFilter();
    this.selectedProvinces = this.provinceFilter.filters;
    this.tierFilter = this.filterService.getTierFilter();
    this.selectedTiers = this.tierFilter.filters;
    this.customerTypeFilter = this.filterService.getCustomerTypeFilter();
    this.selectedCustomerTypes = this.customerTypeFilter.filters;
  }

  ngAfterViewInit(){
    if(this.plotlyBoxData1 && this.plotlyBox1Ready) {
      Plotly.newPlot(this.plotlyBoxName1, this.plotlyBoxData1,
        this.plotlyBoxLayout1, this.plotlyOptions);
      Plotly.newPlot(this.plotlyBoxName2, this.plotlyBoxData2,
        this.plotlyBoxLayout2, this.plotlyOptions);
      Plotly.newPlot(this.plotlyPieName,this.plotlyPieData,
        this.plotlyPieLayout, this.plotlyOptions);
    }
  }

  ngAfterViewChecked(){
    // let _this = this;
    if(document.getElementById(this.plotlyBoxName1) && this.plotlyBox1Ready) {
      if(!this._boxPlot1) {
        this._boxPlot1 = <CustomHTMLElement>document.getElementById(this.plotlyBoxName1);
        let _this = this;
        this._boxPlot1.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.plotlyBoxName1)){
            Plotly.Plots.resize(_this._boxPlot1);
          }
        };
      }
    }
    if(document.getElementById(this.plotlyBoxName2) && this.plotlyBox2Ready) {
      if(!this._boxPlot2) {
        this._boxPlot2 = <CustomHTMLElement> document.getElementById(this.plotlyBoxName2);
        let _this = this;
        this._boxPlot2.on('plotly_click', function(data) {
          console.log(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.plotlyBoxName2)) {
            Plotly.Plots.resize(_this._boxPlot2);
          }
        };
      }
    }
    if(document.getElementById(this.plotlyPieName) && this.plotlyPieData){
      if(!this._piePlot){
        this._piePlot = <CustomHTMLElement> document.getElementById(this.plotlyPieName);
        let _this = this;
        this._piePlot.on('plotly_click', function(data) {
          console.log(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.plotlyPieName)) {
            Plotly.Plots.resize(_this._piePlot);
          }
        };
      }
    }
  }

  generateData(data:any[]) {
    // let _this = this;
    console.log(data);
    let provinceData = _.countBy(_.pluck(data,'province'),function(prvnc){
      return prvnc;
    });

    this.chartData = [
      {name: "Copperbelt", value: 0},
      {name: "Southern", value: 0},
      {name: "Eastern", value: 0},
      {name: "Northern", value: 0},
      {name: "Muchinga", value: 0},
      {name: "Luapula", value: 0},
      {name: "North-Western", value: 0},
      {name: "Western", value: 0},
      {name: "Lusaka", value: 0},
      {name: "Central", value: 0}
    ];

    _.each(this.chartData,function(val){
      if(provinceData[val.name] != null && provinceData[val.name] != ""){
        val.value = provinceData[val.name];
      }else{
        val.value = null;
      }
    })
  }

  setChartData(data) {
    let _this = this;
    let provinces = _.uniq(_.pluck(data,'province'));

    this.plotlyBoxData1 = [];
    this.plotlyBoxData2 = [];

    _.each(provinces,function(p){
      let provinceData = _.where(data,{province:p});
      let traceAPRU = {
        y: _.pluck(provinceData,'revenueAvg30'),
        name: p,
        type:'box'
      };
      _this.plotlyBoxData1.push(traceAPRU);
      let siteY = [];
      let sites = _.pluck(provinceData,'microgridId');
      _.each(sites,function(s){
        let siteRev = 0;
        _.each(_.where(provinceData,{microgridId:s}),function(row){
          siteRev += row['revenueAvg30'];
        });
        siteY.push(siteRev);
      });
      let traceARPS = {
        y: siteY,
        name: p,
        type:'box'
      };
      _this.plotlyBoxData2.push(traceARPS);
    });

    let custTypeData = _.countBy(data,function(row){return row['customerClass']});
    let keys = Object.keys(custTypeData);
    let labels = [];
    let text = [];
    let values = [];
    _.each(keys,function(key){
      labels.push(key);
      text.push(key + '<br>' + custTypeData[key].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
      values.push(custTypeData[key]);
    });

    this.plotlyPieData = [{
      values: values,
      labels: labels,
      type: 'pie',
      hole: 0.4,
      textposition: 'inside',
      text:text,
      hoverinfo: 'label',
      textfont: {
        color: 'white'
      }
    }];

    if(custTypeData['Low']){
      let lowTarget = (_.findWhere(this.customerClass,{customerClass:'Low'}))['nbrPlannedCoefficient'];
      let lowActual = custTypeData['Low'] / data.length;
      if(lowActual > lowTarget){
        this.lowCaption = 'Above';
        this.lowDataCell = lowActual - lowTarget;
      }else if (lowTarget > lowActual){
        this.lowCaption = 'Below';
        this.lowDataCell = lowTarget - lowActual;
      }
    }
    if(custTypeData['Medium']){
      let midTarget = (_.findWhere(this.customerClass,{customerClass:'Medium'}))['nbrPlannedCoefficient'];
      let midActual = custTypeData['Medium'] / data.length;
      if(midActual > midTarget){
        this.midCaption = 'Above';
        this.midDataCell = midActual - midTarget;
      }else if (midTarget > midActual){
        this.midCaption = 'Below';
        this.midDataCell = midTarget - midActual;
      }
    }
    if(custTypeData['High']){
      let highTarget = (_.findWhere(this.customerClass,{customerClass:'High'}))['nbrPlannedCoefficient'];
      let highActual = custTypeData['High'] / data.length;
      if(highActual > highTarget){
        this.highCaption = 'Above';
        this.highDataCell = highActual - highTarget;
      }else if (highTarget > highActual){
        this.highCaption = 'Below';
        this.highDataCell = highTarget - highActual;
      }
    }

    this.plotlyBox1Ready = true;
    this.plotlyBox2Ready = true;

    this.ngAfterViewInit();
  }

  setSiteFilter(data:any){
    let _this = this;
    let siteIds = _.uniq(_.pluck(_.filter(data,function(row){return row['microgridId'] != null;}),'microgridId'));

    this.sites = [];
    _.each(siteIds,function(siteId){
      _this.sites.push({name:'Site ' + siteId, code:siteId});
    });
  }

  setZambiaCoordinates(data:any[]){
    let _this = this;

    this.pointCoords = [];
    _.each(data,function(site){
      let coords = site['textLngLtd'].split('/');
      let village:string;
      if (site['nameVillage']) {
        village = site['nameVillage'];
      } else {
        village = 'N/A';
      }
      let pointCoord = {
        nameText:'Site ' + site['extKey'] + ' - Village: ',
        name:village,
        coordinates:[parseFloat(coords[1]),parseFloat(coords[0])]
      };
      _this.pointCoords.push(pointCoord);
    });
  }

  filterData() {
    this.tableData = this.microgridData;
    let _this = this;
    this.tableData = _.filter(this.tableData,function(row){
      return _.contains(_this.selectedProvinces, row['nameProvince'])
    });

    let rawDataSite = this.rawData;

    if(this.selectedSites){
      if(this.selectedSites.length>0){
        let selectedSiteIds = _.pluck(_this.selectedSites,'code');
        this.tableData = _.filter(this.tableData,function(row){
          return _.contains(selectedSiteIds,row['extKey']);
        });
        rawDataSite = _.filter(rawDataSite,function(row){
          return _.contains(selectedSiteIds,row['microgridId']);
        })
      }
    }

    if(this.startDate){
      rawDataSite = _.filter(rawDataSite,function(row){
        let rowDate = new Date(row['dtConnection']);
        return rowDate >=_this.startDate;
      })
    }
    if(this.endDate){
      rawDataSite = _.filter(rawDataSite,function(row){
        let rowDate = new Date(row['dtConnection']);
        return rowDate<=_this.endDate;
      })
    }

    this.setZambiaCoordinates(this.tableData);
    let filteredData = _.filter(rawDataSite,function(row){
      return _.contains(_this.selectedTiers, row[_this.tierFilter.physicalName].toString())
        && _.contains(_this.selectedCustomerTypes,row[_this.customerTypeFilter.physicalName]);
    });

    this.generateData(filteredData);

    filteredData = _.filter(filteredData,function(row){
      return _.contains(_this.selectedProvinces, row[_this.provinceFilter.physicalName]);
    });

    this.setChartData(filteredData);
  }

  handleChange(e) {
    this.tabIndex = e.index;
    if(e.index == 0){
      Plotly.Plots.resize(this._boxPlot1);
      Plotly.Plots.resize(this._boxPlot2);
    }
  }

  showPFModal() {
    this.showPrinterFriendly = true;
    let layout1 = _.clone(this.plotlyBoxLayout1);
    let layout2 = _.clone(this.plotlyBoxLayout2);
    let layout3 = _.clone(this.plotlyPieLayout);

    layout1['width'] = '350';
    layout2['width'] = '350';
    // layout3['width'] = '200';
    //
    // layout1['hight'] = '200';
    // layout2['hight'] = '200';
    // layout3['hight'] = '200';

    Plotly.newPlot(this.plotlyBoxName1 + 'Print', this.plotlyBoxData1, layout1, this.plotlyPrintOptions);
    Plotly.newPlot(this.plotlyBoxName2 + 'Print', this.plotlyBoxData2, layout2, this.plotlyPrintOptions);
    Plotly.newPlot(this.plotlyPieName + 'Print', this.plotlyPieData, layout3, this.plotlyPrintOptions);
    this.zambiaChartPrint.updateChart();
  }
  hidePFModal() {
    this.showPrinterFriendly = false;
  }

  getCSVFileName() {
    let today = new Date();
    let dateTimeStamp = today.getFullYear().toString()+(today.getMonth()+1).toString()+today.getDate().toString();
    return 'Load_Factor_Data_' + dateTimeStamp;
  }

  processCheckBoxs(e) {
    //console.log(e);
    this.filterData();
    if(document.getElementById(this.plotlyBoxName1)) {
      Plotly.newPlot(this.plotlyBoxName1, this.plotlyBoxData1, this.plotlyBoxLayout1, this.plotlyOptions);
    }
    if(document.getElementById(this.plotlyBoxName2)) {
      Plotly.newPlot(this.plotlyBoxName2, this.plotlyBoxData2, this.plotlyBoxLayout2, this.plotlyOptions);
    }
  }
}
