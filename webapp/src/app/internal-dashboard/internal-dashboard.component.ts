import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { CurrencyPipe,PercentPipe,DecimalPipe } from '@angular/common';
import {AppService} from '../app.service';
import {FilterService} from '../shared/service/filter.service';
import {D3Component} from '../shared/d3/d3.component';
import * as _ from 'underscore';
import {MenuItem} from 'primeng/primeng';
import {Filter} from '../shared/class/filter.obj';
import {DummyData} from '../shared/class/dummy.names';
import {InternalDashboardService} from './internal-dashboard.service';
import {LoadingService} from '../shared/service/loading-services';
import  *  as  riskData  from  '../shared/service/risk.json';

@Component({
  selector: 'app-internal-dashboard',
  templateUrl: './internal-dashboard.component.html',
  styleUrls: ['./internal-dashboard.component.css']
})
export class InternalDashboardComponent implements OnInit {
  pointCoords:[number[],number[]];
  selectedProvince:string = "All";
  portAtRisk:number;
  totalDefaults:number;
  defaultRate:number;
  avgSalesPerSeller:number;
  loadCapacityContract:number;
  loadFactor:number;
  arps:number;
  arpu:number;
  tier3Plus:number;

  @ViewChild('zambiaChart') private zambiaChart: D3Component;
  chartData: Array<any>;
  rawData:any[];
  sellerData:any[];
  microgridData:any[];
  tableData:any[];

  espFilter:Filter;
  selectedESPs:string[];
  selectedTypes = ['SHS','Microgrid'];
  provinceFilter:Filter;
  selectedProvinces:string[];
  startDate:Date;
  endDate:Date;
  showPrinterFriendly:boolean = false;

  constructor(private appService:AppService, public router:Router, private filterService:FilterService,
      private internalDashboardService:InternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    this.espFilter = this.filterService.getESPFilter();
    this.selectedESPs = this.espFilter.filters;
    this.provinceFilter = this.filterService.getProvinceFilter();
    this.selectedProvinces = this.provinceFilter.filters;

    this.internalDashboardService.getRiskSellerData().subscribe((data:any[])=>{
        this.sellerData = data;
        this.setSellerCell(data);

        // this.subRiskDataPull()

      },
      error=>{
        console.log(error);
      }
    );
    this.internalDashboardService.getRiskMicrogridData().subscribe((data:any[])=>{


      console.log(data)

        this.microgridData = data;
        this.setMicrogridCells(data);
      },
      error=>{
        console.log(error);
      }
    );

    this.internalDashboardService.getRiskData().subscribe((data:any[])=>{
      
        this.rawData = data;
        this.tableData = data;
        this.setDashboardCells(data);
        this.generateData(data);
        this.loadingService.hide();

      },error=>{
        console.log(error);
        this.loadingService.hide();
      }
    );

  }


  generateData(data:any[]) {
    let _this = this
    let provinceData = _.countBy(_.pluck(data,'province'),function(prvnc){
      return prvnc;
    });

    this.chartData = [
      {name: "Copperbelt", value: 0},
      {name: "Southern", value: 0},
      {name: "Eastern", value: 0},
      {name: "Northern", value: 0},
      {name: "Muchinga", value: 0},
      {name: "Luapula", value: 0},
      {name: "North-Western", value: 0},
      {name: "Western", value: 0},
      {name: "Lusaka", value: 0},
      {name: "Central", value: 0}
    ];

    _.each(this.chartData,function(val){
      if(provinceData[val.name] != null && provinceData[val.name] != ""){
        val.value = provinceData[val.name];
      }else{
        val.value = null;
      }
    })
  }

  setDashboardCells(data:any[]){
    if(data.length>0){
      let paygData = _.filter(data,function(row){return (row['espCompanyName'] != 'Standard Microgrid');});
      let defaults = _.where(paygData,{indDefault:true})
      this.totalDefaults = defaults.length;
      this.defaultRate = defaults.length / paygData.length;

      let tier3Prod = _.where(data,{indProd3Plus:true});
      this.tier3Plus = tier3Prod.length / data.length;

      let port30Plus = _.where(paygData,{ind30Overdue:true});
      this.portAtRisk = port30Plus.length / paygData.length;

      let microgridData = _.where(data,{connectionType:'Microgrid'});
      if(microgridData.length>0){
        let totalSiteRevenue = 0;
        _.each(microgridData,function(row){
          totalSiteRevenue += row['revenueAvg30'];
        });
        let custCount = _.uniq(microgridData,'customerId');
        let siteCount = _.uniq(microgridData,'microgridId');

        this.arpu = totalSiteRevenue / custCount.length;
        this.arps = totalSiteRevenue / siteCount.length;
      }else{
        this.arpu = 4; // TODO: fix, only valid for demo
        this.arps = 895;
      }
    }else{
      this.totalDefaults = 0;
      this.defaultRate = 0;
      this.tier3Plus = 0;
      this.portAtRisk = 0;
      this.arpu = 0;
      this.arps = 0;
    }
  }

  setSellerCell(data:any[]){
    //avgSalesPerSeller
    let totalSales = 0.00;
    _.each(data,function(row){
      totalSales += row['amtTotalSales'];
    })
    let distinctSellers = _.uniq(_.pluck(data,'sellerId'));
    if(distinctSellers.length>0){
        this.avgSalesPerSeller = totalSales / distinctSellers.length;
    }else{
      this.avgSalesPerSeller = 0;
    }
  }

  setMicrogridCells(data:any[]){

    let totalInUse = 0;
    let totalOutput = 0;
    let totalAvgLoad = 2;
    let totalPeak = 10;
    _.each(data,function(row){
      // totalInUse += row['totalKwHrsPerDay']; TODO: just for the demo
      totalInUse += row['capacityKw'];
      // totalOutput += row['avgDailyOutputKw']; TODO: used just for the demo
      totalOutput += row['capacityUnderContract'];
      // totalAvgLoad += row['avgLoadKw'];
      // totalPeak += row['peakLoadKw'];
    })

    // console.log(totalOutput)
    // console.log(totalInUse)

    if(totalOutput > 0){
      this.loadCapacityContract = totalInUse / totalOutput;
    }else{
      // this.loadCapacityContract = 0; fall back value
      this.loadCapacityContract = 0.4;
    }
    if(totalPeak>0){
      this.loadFactor = totalAvgLoad / totalPeak;
    }else{
        this.loadFactor = 0;
    }
  }

  processCheckBox(e){
    // this.generateData(this.rawData);
    //console.log(e);
    this.filterData();
    this.zambiaChart.updateChart();
  }

  filterData(){
    let _this = this;
    this.tableData = this.rawData;

    this.tableData = _.filter(this.tableData,function(row){
      return (_.contains(_this.selectedESPs,row['espCompanyName'])
          && _.contains(_this.selectedProvinces,row['province'])
          //&& _.contains(_this.selectedTypes,row['connectionType'])
        );
    });

    if(this.startDate){
      console.log(this.startDate);
      _this.tableData = _.filter(_this.tableData,function(row){
        let rowDate = new Date(row['dtAcquisition']);
        return rowDate >=_this.startDate;
      })
    }
    if(this.endDate){
      console.log(this.endDate);
      _this.tableData = _.filter(_this.tableData,function(row){
        let rowDate = new Date(row['dtAcquisition']);
        return rowDate<=_this.endDate;
      })
    }

    this.generateData(this.tableData);
    if(this.selectedProvince != 'All'){
      this.tableData = _.where(this.tableData,{province:this.selectedProvince});
    }
    this.setDashboardCells(this.tableData);
    this.setSellerCell(_.filter(this.sellerData,function(row){
      return (_.contains(_this.selectedESPs,row['espCompanyName'])
        && _.contains(_this.selectedProvinces,row['province']));
    }));
    this.setMicrogridCells(_.filter(this.microgridData,function(row){
      return (_.contains(_this.selectedProvinces,row['province']));
    }));
  }

  provinceClicked(e){
    if(e!='All'){
      this.selectedProvince = e.province.properties.CAPTION;
      this.filterData();
    }else{
      this.selectedProvince = 'All';
      this.filterData();
    }
    this.zambiaChart.updateChart();
  }

  showPFModal(){
    this.showPrinterFriendly = true;
  }
  
  hidePFModal(){
    this.showPrinterFriendly = false;
  }

  getDummyName(index:any){
    let dummy = new DummyData()
    return dummy.getDummyName(index)
  }

  subRiskDataPull(){
    const data = (<any>riskData).payload;

    this.rawData = data;
    this.tableData = data;
    this.setDashboardCells(data);
    this.generateData(data);
    this.loadingService.hide();

  }

}
