import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InternalDashboardComponent } from './internal-dashboard.component';
import {SharedModule} from '../shared/shared.module';

import { InternalDashboardRoutingModule } from './internal-dashboard-routing.module';
import { InternalDashboardService } from './internal-dashboard.service';
import { PaygAtRiskComponent } from './payg-at-risk/payg-at-risk.component';
import { TotalDefaultsComponent } from './total-defaults/total-defaults.component';
import { DefautRateComponent } from './defaut-rate/defaut-rate.component';
import { SalesPerSellerComponent } from './sales-per-seller/sales-per-seller.component';
import { LoadCapacityComponent } from './load-capacity/load-capacity.component';
import { LoadFactorComponent } from './load-factor/load-factor.component';
import { ArpsComponent } from './arps/arps.component';
import { ArpuComponent } from './arpu/arpu.component';
import { HighRevenueCustomersComponent } from './high-revenue-customers/high-revenue-customers.component';
import {ListboxModule} from 'primeng/components/listbox/listbox';

@NgModule({
  imports: [
    CommonModule,InternalDashboardRoutingModule,SharedModule.forRoot(),ListboxModule
  ],
  declarations: [InternalDashboardComponent, PaygAtRiskComponent, TotalDefaultsComponent, DefautRateComponent, SalesPerSellerComponent, LoadCapacityComponent, LoadFactorComponent, ArpsComponent, ArpuComponent, HighRevenueCustomersComponent],
  providers: [InternalDashboardService]
})
export class InternalDashboardModule { }
