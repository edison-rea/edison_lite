import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadFactorComponent } from './load-factor.component';

describe('LoadFactorComponent', () => {
  let component: LoadFactorComponent;
  let fixture: ComponentFixture<LoadFactorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadFactorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadFactorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
