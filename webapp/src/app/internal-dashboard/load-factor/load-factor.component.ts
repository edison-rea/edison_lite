import { Component, OnInit, Input, Output, EventEmitter,
  OnChanges, SimpleChanges, SimpleChange,ViewChild } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import { CurrencyPipe,PercentPipe } from '@angular/common';
import {CustomHTMLElement} from '../../shared/class/custom.html.element.obj';
import {AppService} from '../../app.service';
import {Filter} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {InternalDashboardService} from '../internal-dashboard.service';
import {LoadingService} from '../../shared/service/loading-services';
//import {MultiSelectModule} from 'primeng/primeng';
import * as _ from 'underscore';

declare var Plotly: any;

@Component({
  selector: 'app-load-factor',
  templateUrl: './load-factor.component.html',
  styleUrls: ['./load-factor.component.css']
})

export class LoadFactorComponent implements OnInit {
  rawData:any[];
  subDashboardTitle:string = 'Microgrid Technical Performance: Load Factor';
  showPrinterFriendly:boolean = false;
  parentDashboard:string = 'analytics';
  plotlyLoadDistName:string='plotlyLoadDistDiv';
  plotlyLoadDistData:any[];
  plotlyLoadDistLayout:any;
  plotlyLoadDistOptions:any = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'lasso2d',//Lasso Select
        'toggleSpikelines', //Toggle Spike
        'select2d', //Box Select
        'sendDataToCloud', //Save and Sent to Cloud
        'resetScale2d' //Reset axes
      ]
  };
  plotlyBoxName:string='plotlyBoxDiv';
  plotlyBoxData:any[];
  plotlyBoxLayout:any;
  plotlyBoxOptions:any = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'lasso2d',//Lasso Select
        'toggleSpikelines', //Toggle Spike
        'select2d', //Box Select
        'sendDataToCloud', //Save and Sent to Cloud
        'resetScale2d' //Reset axes
      ]
  };
  plotlyPrintOptions: any = {
    displayModeBar: false
  }
  _loadDistPlot: CustomHTMLElement;
  _boxPlot: CustomHTMLElement;
  tableData: any[];
  tabIndex:number = 0;

  provinceFilter:Filter;
  selectedProvinces:string[];

  sites:any[];
  selectedSites:string[];
  plotlyLoadDistReady:boolean = false;
  plotlyBoxReady:boolean = false;
  microgridData:any[];

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
    private internalDashboardService:InternalDashboardService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    this.internalDashboardService.getMicrogridHourlyData().subscribe((data:any[])=>{
        this.rawData = data;
        this.setChartData(this.rawData);
        this.setSiteFilter(this.rawData);
        if(document.getElementById(this.plotlyLoadDistName) && this.plotlyLoadDistData &&
          document.getElementById(this.plotlyBoxName) && this.plotlyBoxData){
          this.ngAfterViewInit();
        }
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )

    this.internalDashboardService.getMicrogridData().subscribe((data:any[])=>{
        this.microgridData = data;
        this.tableData = data;

        this.setBoxChart(data);
        this.ngAfterViewInit();
      },
      error=>{
        console.log(error);
      }
    )

    this.plotlyLoadDistLayout = {
      title: '24-Hr Monthly Avg Load Distribution',
      showlegend: true,
      autosize: true,
      xaxis:{range: [1, 24], title: "Time (hour)"},
      yaxis:{hoverformat: '.2r', tickformat: '.0r', title: 'Load in W'},
      type: "scatter",
      hovermode:'closest'
    };

    this.plotlyBoxLayout = {
      title: '30-Day Load Range',
      showlegend: false,
      autosize: true,
      //xaxis:{title: "Microgrid"},
      yaxis:{title: 'Load in W'},
      type: "box"
    };

    this.provinceFilter = this.filterService.getProvinceFilter();
    this.selectedProvinces = this.provinceFilter.filters;
  }

  ngAfterViewInit(){
    if(this.plotlyLoadDistData && this.plotlyLoadDistReady) {
      Plotly.newPlot(this.plotlyLoadDistName, this.plotlyLoadDistData,
        this.plotlyLoadDistLayout, this.plotlyLoadDistOptions);
      Plotly.newPlot(this.plotlyBoxName, this.plotlyBoxData,
        this.plotlyBoxLayout, this.plotlyBoxOptions);
    }
  }

  ngAfterViewChecked(){
    let _this = this;
    if(document.getElementById(this.plotlyLoadDistName) && this.plotlyLoadDistReady) {
      if(!this._loadDistPlot) {
        this._loadDistPlot = <CustomHTMLElement>document.getElementById(this.plotlyLoadDistName);
        let _this = this;
        this._loadDistPlot.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.plotlyLoadDistName)){
            Plotly.Plots.resize(_this._loadDistPlot);
          }
        };
      }
    }
    if(document.getElementById(this.plotlyBoxName) && this.plotlyBoxReady) {
      if(!this._boxPlot) {
        this._boxPlot = <CustomHTMLElement> document.getElementById(this.plotlyBoxName);
        let _this = this;
        this._boxPlot.on('plotly_click', function(data) {
          console.log(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.plotlyBoxName)) {
            Plotly.Plots.resize(_this._boxPlot);
          }
        };
      }
    }
  }
  setChartData(data) {
    let _this = this;
    let sites = _.uniq(_.pluck(data,'microgridId'));
    this.plotlyLoadDistData = [];

    sites = sites.slice(0,10);//Test Data has too many sites!! Building charts without data... my dream come true

    _.each(sites, function(site) {
      let siteData = _.where(data,{microgridId:site});
      let hours = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
      let xData = [];
      _.each(hours,function(hr){
          let hourlyData = _.findWhere(siteData,{hour:hr});
          if(hourlyData['peakWh'] && hourlyData['peakWh'] > 0){
            xData.push(hourlyData['avgWh']/hourlyData['peakWh']);
          }else{
            xData.push(null);
          }

      });

      let trace = {
        x: hours,
        y: xData,
        type:'scatter',
        name: site.toString()
      }
      _this.plotlyLoadDistData.push(trace);
    });
    this.plotlyLoadDistReady = true;
    this.ngAfterViewInit();
    // aggregate all sites into an array representing % of load capacity purchased per month
  }

  setSiteFilter(data:any){
    let _this = this;
    let siteIds = _.uniq(_.pluck(_.filter(data,function(row){return row['microgridId'] != null;}),'microgridId'));
    this.sites = [];
    _.each(siteIds,function(siteId){
      _this.sites.push({name:'Site ' + siteId, code:siteId});
    });
  }

  setBoxChart(data:any[]){
    if(data.length>0){
      let _this = this;
      this.plotlyBoxData = [];
      _.each(data.slice(0,10),function(site){
        let y = [site['nbrBaseLoadKw'],site['nbrAvgLoadKw'],site['nbrPeakLoadKw']];
        let trace = {
          y: y,
          type: 'box',
          name: 'Site ' + site['extKey']
        };
        _this.plotlyBoxData.push(trace);
      })
    }else{
      let trace = {
        y: [],
        type: 'box',
        name: ''
      };
      this.plotlyBoxData.push(trace);
    }
    this.plotlyBoxReady = true;
  }

  filterData() {
    this.tableData = this.microgridData;
    let _this = this;
    this.tableData = _.filter(this.tableData,function(row){
      return _.contains(_this.selectedProvinces, row['nameProvince'])
          //&& _.contains(_this.selectedSites, row[_this.siteFilter.physicalName]);
    })

    let rawDataSite = this.rawData;

    if(this.selectedSites){
      if(this.selectedSites.length>0){
        let selectedSiteIds = _.pluck(_this.selectedSites,'code');
        this.tableData = _.filter(this.tableData,function(row){
          return _.contains(selectedSiteIds,row['extKey']);
        });
        rawDataSite = _.filter(rawDataSite,function(row){
          return _.contains(selectedSiteIds,row['microgridId']);
        })
      }
    }

    this.setChartData(_.filter(rawDataSite,function(row){
      return _.contains(_this.selectedProvinces, row[_this.provinceFilter.physicalName])
          //&& _.contains(_this.selectedSites, row[_this.siteFilter.physicalName]);
    }));
    this.setBoxChart(this.tableData);
  }

  handleChange(e) {
    this.tabIndex = e.index;
    if(e.index == 0){
      Plotly.Plots.resize(this._loadDistPlot);
      Plotly.Plots.resize(this._boxPlot);
    }
  }

  showPFModal() {
    this.showPrinterFriendly = true;
    Plotly.newPlot(this.plotlyLoadDistName + 'Print', this.plotlyLoadDistData, this.plotlyLoadDistLayout, this.plotlyPrintOptions);
    Plotly.newPlot(this.plotlyBoxName + 'Print', this.plotlyBoxData, this.plotlyBoxLayout, this.plotlyPrintOptions);
    Plotly.relayout(this.plotlyLoadDistName + 'Print',{width:340})
    Plotly.relayout(this.plotlyBoxName + 'Print',{width:340})
  }
  hidePFModal() {
    this.showPrinterFriendly = false;
  }

  getCSVFileName() {
    let today = new Date();
    let dateTimeStamp = today.getFullYear().toString()+(today.getMonth()+1).toString()+today.getDate().toString();
    return 'Load_Factor_Data_' + dateTimeStamp;
  }

  processCheckBoxs(e) {
    //console.log(e);
    this.filterData();
    if(document.getElementById(this.plotlyLoadDistName)) {
      Plotly.newPlot(this.plotlyLoadDistName, this.plotlyLoadDistData, this.plotlyLoadDistLayout, this.plotlyLoadDistOptions);
    }
    if(document.getElementById(this.plotlyBoxName)) {
      Plotly.newPlot(this.plotlyBoxName, this.plotlyBoxData, this.plotlyBoxLayout, this.plotlyBoxOptions);
    }
  }
}
