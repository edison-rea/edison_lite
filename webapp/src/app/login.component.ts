import { Component, ApplicationRef, ViewChild }   from '@angular/core';
import { Router }      from '@angular/router';
import { AuthService } from './auth.service';
import { Captcha } from 'primeng/components/captcha/captcha';
import * as _ from 'underscore';

@Component({
  template: `
    <h2>LOGIN</h2>
    <div *ngIf="attempts<4">
      <div  *ngIf="error" style="color:red;">Error! Email or Password is incorrect!</div>
      <div style="display:inline-block;margin-right:10px;">
        <h3>Email:</h3>
        <input type="text" [(ngModel)]="email"/>
      </div>
      <div style="display:inline-block;">
        <h3>Password:</h3>
        <input type="password" [(ngModel)]="pwd"/>
      </div>
      <p>
        <button (click)="login()"  *ngIf="!authService.isLoggedIn">Login</button>
        <button (click)="logout()" *ngIf="authService.isLoggedIn">Logout</button>
      </p>
    </div>
    <p-captcha [ngStyle]="{'display': attempts>=4 ? 'block' : 'none' }" siteKey="6LdeFakZAAAAAAzQp2mG9QyZHzXchg6s2bcHZzGr" (onResponse)="showResponse($event)"></p-captcha>

    `
})
export class LoginComponent {
  @ViewChild(Captcha) captcha: Captcha;
  //message: string;
  email: string;
  pwd: any;
  error: boolean = false;
  attempts: number = 0;

  constructor(public authService: AuthService, public router: Router, private applicationRef:ApplicationRef) {
    //this.setMessage();
  }

  // setMessage() {
  //   this.message = 'Logged ' + (this.authService.isLoggedIn ? 'in' : 'out');
  // }

  login() {
    //this.message = 'Trying to log in ...';

    this.authService.login(this.email,this.pwd).subscribe((res:any) => {
        this.authService.isLoggedIn = true;
        localStorage.setItem('access_token',res['access_token']);
        localStorage.setItem('token_type',res['token_type']);
        localStorage.setItem('expires_in',res['expires_in']);
        localStorage.setItem('scope',res['scope']);
        localStorage.setItem('jti',res['jti']);
        localStorage.setItem('usr_email',this.email);

        this.authService.getRoles().subscribe((res:any[])=>{
            if(_.contains(_.pluck(res,'authority'),'ROLE_ADMIN')){
              localStorage.setItem('authority','ROLE_ADMIN');
              this.authService.isAdmin = true;
            }else if(_.contains(_.pluck(res,'authority'),'ROLE_USER')){
              localStorage.setItem('authority','ROLE_USER');
              this.authService.isAdmin = false;
            }else{
              localStorage.setItem('authority','ROLE_PUBLIC');
              this.authService.isAdmin = false;
            }
          },
          error=>{
            console.log(error);
          }
        )
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
        let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/bgfz-impact';

        // Redirect the user
        this.router.navigate([redirect]);
        this.applicationRef.tick();

      },
      error=>{
        this.error = true;
        this.attempts +=1;
      }
    );
  }

  logout() {
    this.authService.logout();
    //this.setMessage();
    this.router.navigate(['/bfgz-impact']);
  }

  showResponse(e){
    this.attempts = 0;
    this.error = false;
    this.captcha.reset();
  }
}
