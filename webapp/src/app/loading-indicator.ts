import {Component, ElementRef, OnInit, OnDestroy} from '@angular/core';
import {LoadingService} from "./shared/service/loading-services";
import { Subscription } from 'rxjs/Subscription';
import { LoaderState } from './shared/class/loader';

@Component({
    selector: 'loading-indicator',
     template: `
       <span [style.visibility]="show ? 'visible': 'hidden'"  class="loading">

       </span>
      `,
})
export class LoadingIndicator implements OnInit, OnDestroy {
  show = false;

  private subscription: Subscription;

  constructor(
      private loaderService: LoadingService
  ) { }

  ngOnInit() {
      this.subscription = this.loaderService.loaderState
          .subscribe((state: LoaderState) => {
              this.show = state.show;
          });
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }

    // private isLoading = false;
    // private subscription: any;
    //
    // //we probably want a reference to ElementRef here to do some DOM manipulations
    // constructor(public el: ElementRef, public loadingService: LoadingService) { }
    //
    // showOrHideLoadingIndicator(loading) {
    //     this.isLoading = loading;
    //     if (this.isLoading) this.playLoadingAnimation();
    //     //else cancel the animation?
    // }
    //
    // playLoadingAnimation() {
    //     //this will be your implementation to start the loading animation
    // }
    //
    // ngOnInit() {
    //     this.subscription = this.loadingService.loadingObserver.subscribe((loading:boolean) => console.log(loading));//loading => this.showOrHideLoadingIndicator(loading));
    // }
    //
    // ngOnDestroy() {
    //     this.subscription.unsubscribe();
    // }
}
