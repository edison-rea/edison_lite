import { Component, OnInit } from '@angular/core';

import{EspApiLogService} from './esp-api-log.service';
import {MessageService} from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-esp-api-log',
  templateUrl: './esp-api-log.component.html',
  styleUrls: ['./esp-api-log.component.css']
})
export class EspApiLogComponent implements OnInit {
  espApiLog:any[];

  constructor(private espApiLogService:EspApiLogService,private messageService:MessageService) { }

  ngOnInit() {
    this.espApiLogService.getEspApiLog().subscribe((data:any[])=>{
      this.espApiLog = data;
    },
    error=>{
      this.messageService.add({severity:'error', summary:'Failed to fetch ESP API Log', detail:error});
    })
  }

}
