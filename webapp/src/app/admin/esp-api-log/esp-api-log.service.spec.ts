import { TestBed, inject } from '@angular/core/testing';

import { EspApiLogService } from './esp-api-log.service';

describe('EspApiLogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EspApiLogService]
    });
  });

  it('should be created', inject([EspApiLogService], (service: EspApiLogService) => {
    expect(service).toBeTruthy();
  }));
});
