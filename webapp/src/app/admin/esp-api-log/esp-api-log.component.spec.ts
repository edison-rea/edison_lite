import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspApiLogComponent } from './esp-api-log.component';

describe('EspApiLogComponent', () => {
  let component: EspApiLogComponent;
  let fixture: ComponentFixture<EspApiLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspApiLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspApiLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
