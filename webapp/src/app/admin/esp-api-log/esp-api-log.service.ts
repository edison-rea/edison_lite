import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EnvConfig } from '../../shared/service/env.config.service';

@Injectable()
export class EspApiLogService {

  constructor(private envConfig:EnvConfig, private http:Http) { }

  public getEspApiLog = (): Observable<any[]> => {
    let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({headers:headers});

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'esp-api-log',options)
      .map((res:Response)=><any[]>res.json().payload);
  }
}
