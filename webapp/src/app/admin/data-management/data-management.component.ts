import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

import {SelectItem} from 'primeng/primeng';
import {FileUpload} from 'primeng/components/fileupload/fileupload';
import {EnvConfig} from '../../shared/service/env.config.service';
import {LoadingService} from '../../shared/service/loading-services';
import {DataManagementService} from './data-management.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {AuthService} from '../../auth.service';
import {DomSanitizer,SafeUrl} from '@angular/platform-browser'
import * as _ from 'underscore';
import * as convert from 'xml-js';

@Component({
  selector: 'app-data-management',
  templateUrl: './data-management.component.html',
  styleUrls: ['./data-management.component.css']
})
export class DataManagementComponent implements OnInit {
  @ViewChild(FileUpload) fileUploader: FileUpload;
  uploadTypes:SelectItem[];
  selectedType:string;
  downloadTypes:SelectItem[];
  downloadType:string;
  downloadData:any[];
  cols:any[];
  colsByType:any;
  active:boolean=false;
  startDate:Date;
  endDate:Date;
  safeUriExport:any;

  constructor(private envConfig:EnvConfig, private dataManagementService:DataManagementService,private messageService: MessageService,
    private loadingService:LoadingService, private authService:AuthService,private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.endDate = new Date();
    this.startDate = new Date();
    this.startDate.setMonth(this.startDate.getMonth() - 3);

    this.colsByType =
      { tier: [
          {field: "id", header: "id"},
          {field: "nbrTierLevel", header: "nbrTierLevel"},
          {field: "textApplicationTier", header: "textApplicationTier"},
          {field: "nbrWhCoefficient", header: "nbrWhCoefficient"},
          {field: "nbrBeneficiaryCoefficient", header: "nbrBeneficiaryCoefficient"},
          {field: "nbrCo2Coeffiecent", header: "nbrCo2Coeffiecent"},
          {field: "nbrLampsDisplaceCoefficient", header: "nbrLampsDisplaceCoefficient"},
          {field: "nbrLightCoefficient", header: "nbrLightCoefficient"},
          {field: "nbrWCoefficient", header: "nbrWCoefficient"}
        ],
        product:[
          {field: "uiId", header: "id"},
          {field: "extKey", header: "extKey"},
          {field: "espCompanyName", header: "espCompanyName"},
          {field: "nbrTierLevel", header: "nbrTierLevel"},
          {field: "textApplicationTier", header: "textApplicationTier"},
          {field: "productType", header: "productType"},
          {field: "nameProduct", header: "nameProduct"},
          {field: "nbrWhMin", header: "nbrWhMin"},
          {field: "nbrWhMax", header: "nbrWhMax"},
          {field: "nbrLights", header: "nbrLights"},
          {field: "nbrWtPeak", header: "nbrWtPeak"}
        ],
        "esp-target":[
          {field: "uiId", header: "id"},
          {field: "espCompanyName", header:"espCompanyName"},
          {field: "nbrTierLevel", header:"nbrTierLevel"},
          {field: "textApplicationTier", header:"textApplicationTier"},
          {field: "textYrQtr", header: "textYrQtr"},
          {field: "nbrEssTarget", header: "nbrEssTarget"},
          {field: "nbrQtrDistribution", header:"nbrQtrDistribution"}
        ],
        "financial-leverage":[
          {field: "uiId", header:"id"},
          {field: "espCompanyName", header:"espCompanyName"},
          {field: "financeType", header:"financeType"},
          {field: "amtFunding", header:"amtFunding"},
          {field: "dtFunding", header:"dtFunding"}
        ],
        jobs:[
          {field: "id", hdeader:"id"},
          {field: "textJobType", hdeader:"textJobType"},
          {field: "nbrJobsCreated", hdeader:"nbrJobsCreated"}
        ],
        "revenue-customer-class":[
          {field: "id", header:"id"},
          {field: "customerClass", header:"customerClass"},
          {field: "nbrRevenueMin", header:"nbrRevenueMin"},
          {field: "nbrRevenueMax", header:"nbrRevenueMax"},
          {field: "nbrPlannedCoefficient", header:"nbrPlannedCoefficient"}
        ]
      }
    this.uploadTypes = [
      {label:'ESS Schedule',value:'esp-target'},
      {label:'Financial Leverage',value:'financial-leverage'},
      {label:'Jobs',value:'jobs'},
      {label:'Product Types',value:'product'},
      {label:'Revenue Customer Class',value:'revenue-customer-class'},
      {label:'Tiers',value:'tier'}
    ]
    this.downloadTypes = [
      {label:'Event Data',value:'event'},
      {label:'Microgrid Data',value:'microgrid'},
      {label:'Microgrid ESS Data',value:'microgrid-ess'},
      {label:'Party Data',value:'party'},
      {label:'PAYG Data',value:'payg'},
      {label:'SHS ESS Data',value:'shs-ess'},
      {label:'Wh Transaction Data',value:'wh-transaction'}
    ]
  }

  fileSelect(){
    console.log('File Select');
  }
  drowDownChange(){
    this.active = true;
    this.fileUploader.clear();
    this.getSelectedTableData();
  }

  getSelectedTableData(){
    this.dataManagementService.getTableData(this.selectedType).subscribe((data:any[])=>{
        this.cols = this.colsByType[this.selectedType];
        this.downloadData = data;
      },
      error=>{
        console.log(error);
        this.authService.logout();
      }
    )
  }

  onBeforeUpload(e){
    //Callback to invoke before file upload begins to customize the request such as post parameters before the files.
    this.loadingService.show();
  }

  onBeforeSend(e){
    e.xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem('access_token'));
    //Callback to invoke before file send begins to customize the request such as adding headers.
  }

  onBasicUpload(e){
    console.log('Basic Upload:',e);
  }

  onUpload(e){
    let res = e['xhr']['response']
    console.log(e);
    console.log(res.payload);
    this.getSelectedTableData();
    this.loadingService.hide();
    this.messageService.add({severity:'success', summary:'Upload Successful', detail:'Success'});
  }

  onError(e){
    console.log(JSON.parse(e['xhr']['response'])['message']);
    this.loadingService.hide();
    this.messageService.add({severity:'error',summary:'Upload Failed', detail: JSON.parse(e['xhr']['response'])['message']});
  }

  getUrl(){
    return this.envConfig.getEnvVariable('endPoint') + this.selectedType + '/upload';
  }

  exportXML(){
    this.loadingService.show();
    this.dataManagementService.getDataForXML(this.downloadType,this.startDate,this.endDate).subscribe((data:any[])=>{
      let blob = new Blob([JSON.stringify(data)], { type: 'text/JSON' });
      let url= window.URL.createObjectURL(blob);
      let uri:SafeUrl = this.sanitizer.bypassSecurityTrustUrl(url);
      this.safeUriExport = uri;
      let downloader = document.getElementById('downloader');
      this.loadingService.hide();
      setTimeout(function(){
          downloader.click();
      }, 100);
    });
  }

  clearDates(){
    this.startDate = undefined;
    this.endDate = undefined;
  }
}
