import { TestBed, inject } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { DataManagementService } from './data-management.service';

describe('DataManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataManagementService],
      imports:[FormsModule]
    });
  });

  it('should be created', inject([DataManagementService], (service: DataManagementService) => {
    expect(service).toBeTruthy();
  }));
});
