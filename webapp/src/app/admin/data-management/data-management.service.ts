import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import {EnvConfig} from '../../shared/service/env.config.service';

@Injectable()
export class DataManagementService {
  constructor(private envConfig:EnvConfig, private http:Http) { }

  public getTableData = (uploadType:string): Observable<any[]> =>{
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + uploadType,options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getDataForXML = (downloadType:string,startDate:Date,endDate:Date): Observable<any[]> =>{
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});

    let strStartDate = startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate();
    let strEndDate = endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate();

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'xml-export/' + downloadType + "?start-date=" + strStartDate + "&end-date=" + strEndDate,options)
      .map((res:Response)=><any[]>res.json().payload);
  }
}
