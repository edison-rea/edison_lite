import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EnvConfig } from '../../shared/service/env.config.service';
import { User } from '../../shared/class/user.obj';

@Injectable()
export class UserManagementService {

  constructor(private envConfig:EnvConfig,private http:Http) { }

  public getAllUsers = (): Observable<User[]> =>{
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});
    console.log(this.envConfig.getEnvVariable('endPoint') + 'user');
    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'user',options)
      .map((res:Response)=><User[]>res.json().payload);
  }

  public saveUser = (user:User): Observable<User> =>{
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});

    return this.http.put(this.envConfig.getEnvVariable('endPoint') + 'user',user,options)
      .map((res:Response)=><User>res.json().payload);
  }

  public createUser = (user:User): Observable<User> => {
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});

    console.log(user);

    return this.http.post(this.envConfig.getEnvVariable('endPoint') + 'user',user,options)
      .map((res:Response)=><User>res.json().payload);
  }

  public deleteUser = (user:User): Observable<string> => {
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});

    return this.http.delete(this.envConfig.getEnvVariable('endPoint') + 'user/' + user.id,options)
      .map((res:Response)=><string>res.json().payload);
  }
}
