import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {User} from '../../shared/class/user.obj';
import * as _ from 'underscore';

import {ConfirmationService} from 'primeng/primeng';
import {UserManagementService} from './user-management.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {PasswordValidator} from '../../shared/validator/password.validator';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css'],
  providers: [ConfirmationService]
})
export class UserManagementComponent implements OnInit {
  users:User[];
  displayAdd:boolean = false;
  displayEdit:boolean = false;
  selectedUser:User;
  selectedUserIndex:number;
  newUser:User={isActive:true};
  visible:boolean = true;
  roles:any[] = [{label:'Select Role...',value:null},{label:'Admin',value:'ROLE_ADMIN'},{label:'Internal',value:'ROLE_USER'}];

  constructor(private confirmationService:ConfirmationService, private userManagementService:UserManagementService,private messageService:MessageService) {
    // this.roles = [{label:'Admin',value:'Admin'},{label:'Internal',value:'Internal'}];
   }

  ngOnInit() {
    this.userManagementService.getAllUsers().subscribe((data:User[])=>{
      this.users = data;
    },
    error=>{
      console.log(error);
      this.messageService.add({severity:'error', summary:'Error Fetching Users', detail:error});
    });
  }

  //***Add User Funcationality***
  displayAddUser(){
    this.displayAdd = true;
  }
  onSubmitAdd(){
    console.log('Add');
    if(this.newUser.textPhoneNumber === ""){
      this.newUser.textPhoneNumber = null;
    }
    this.displayAdd = false;
    this.userManagementService.createUser(this.newUser).subscribe((user:User)=>{
      this.users.push(user);
      this.newUser = {isActive:true};
      this.updateVisibility();
      this.messageService.add({severity:'success', summary:'Add Was Successful', detail:'Successfully added ' + user.nameFirst + ' ' + user.nameLast});
    },
    error=>{
      console.log(error);
      this.messageService.add({severity:'error', summary:'Add User Failed', detail:error});
    })
  }
  cancelUser(){
    this.newUser = {isActive:true};
    this.displayAdd = false;
  }

  //***Edit User Funcationality***
  editUser(user:User){
    this.selectedUserIndex = this.users.indexOf(user);
    this.selectedUser = Object.assign({},user);
    this.displayEdit = true;
  }
  deactivateUser(){
    this.selectedUser.isActive = !this.selectedUser.isActive;
  }
  onSubmitEdits(){
    if(this.selectedUser.textPhoneNumber === ""){
      this.selectedUser.textPhoneNumber = null;
    }
    this.userManagementService.saveUser(this.selectedUser).subscribe((user:User)=>{
      this.users[this.selectedUserIndex] = user;
      this.updateVisibility();
      this.displayEdit = false;
      this.messageService.add({severity:'success', summary:'Update Was Successful', detail:'Successfully updated ' + user.nameFirst + ' ' + user.nameLast});
    },
    error=>{
      console.log(error);
      this.messageService.add({severity:'error', summary:'Update Failed', detail:error});
    });
  }

  //***Delete User Funcationality***
  deleteUser(user:User){
    this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
              this.userManagementService.deleteUser(user).subscribe((res:string)=>{
                  this.messageService.add({severity:'success', summary:'Delete Was Successful', detail:'Successfully deleted ' + user.nameFirst + ' ' + user.nameLast});
                },
                error => {
                  console.log(error);
                  this.messageService.add({severity:'error', summary:'Upload Failed', detail:error});
                }
              )

              if(_.contains(this.users,user)){
                let index: number = this.users.indexOf(user);
                this.users.splice(index,1);
                this.updateVisibility();
              }
            },
            reject: () => {

            }
        });

    console.log(this.users);

  }

  //PrimeNG was having issue with Updating table content. Use this to refresh table
  updateVisibility(): void {
    this.visible = false;
    setTimeout(() => this.visible = true, 0);
  }

  pwdTest:string;
  passwordPattern(pwd:string){
    let valid: boolean = true;
    let ucase: boolean = false;
    let lcase: boolean = false;
    let special: boolean = false;
    if((/[a-z]/.test(pwd))){
      console.log('lower');
      valid = false;
    }
    if((/[A-Z]/.test(pwd))){
      console.log('upper');
      ucase = true;
    }
    if((/[0-9]/.test(pwd))){
      console.log('number');
      lcase = true;
    }
    if((/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(pwd))){
      console.log('Special Char');
      special = true;
    }
    if(pwd.indexOf(' ') >= 0){
      console.log('Space');
      valid = false;
    }

    if(valid && ucase && lcase && special){
      return null;
    }else{
      return {validatePassword:{valid:false}};
    }
  }

}

interface Validator<T extends FormControl>{
  (c:T): {[error: string]:any};
}
