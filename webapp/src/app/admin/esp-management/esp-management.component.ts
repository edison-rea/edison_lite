import { Component, OnInit } from '@angular/core';
import { Esp } from '../../shared/class/esp.obj';
import * as _ from 'underscore';

import {ConfirmationService} from 'primeng/primeng';
import {EspManagementService} from './esp-management.service';
import {EspApiLogService} from '../esp-api-log/esp-api-log.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {FilterService} from '../../shared/service/filter.service';

@Component({
  selector: 'app-esp-management',
  templateUrl: './esp-management.component.html',
  styleUrls: ['./esp-management.component.css'],
  providers: [ConfirmationService]
})
export class EspManagementComponent implements OnInit {
  esps:Esp[];
  espApiLog:any;
  displayAdd:boolean = false;
  displayEdit:boolean = false;
  selectedEsp:Esp;
  selectedEspIndex:number;
  newEsp:Esp={id:null,textApiToken:"1",isActive:true};
  visible:boolean = true;

  constructor(private confirmationService:ConfirmationService, private espManagmentService:EspManagementService,
    private messageService:MessageService, private espApiLogService: EspApiLogService, private filterService:FilterService) { }

  ngOnInit() {
    this.espManagmentService.getAllEsp().subscribe((data:any[]) => {
      this.esps = data;
    },
    error=>{
      console.log(error);
    });
    this.getEspApiLogData();
  }

  getEspApiLogData(){
    this.espApiLogService.getEspApiLog().subscribe((data:any[])=>{
      this.espApiLog = data;
    },
    error=>{
      console.log(error);
    })
  }

  getMostRecentPass(espName:string){
    let espApiRecords = _.where(this.espApiLog,{espCompanyName:espName,cdResult:'Pass'});

    if(espApiRecords.length>0){
      return _.max(_.pluck(espApiRecords,'loggedDate'));
    }else{
      return null;
    }
  }
  getMostRecentFail(espName:string){
    let espApiRecords = _.where(this.espApiLog,{espCompanyName:espName,cdResult:'Fail'});

    if(espApiRecords.length>0){
      return _.max(_.pluck(espApiRecords,'loggedDate'));
    }else{
      return null;
    }
  }

  //***Create Esp Functionality***
  displayAddEsp(){
    this.displayAdd = true;
  }
  onSubmitNew(){
    this.displayAdd = false;
    this.espManagmentService.createEsp(this.newEsp).subscribe((esp:Esp)=>{
      this.esps.push(esp);
      this.newEsp = {id:null,textApiToken:"1",isActive:true};
      this.updateVisibility();
      this.messageService.add({severity:'success', summary:'Add Was Successful', detail:'Successfully added ' + esp.nameCompany});
      this.filterService.setEsps();
    },
    error=>{
      this.messageService.add({severity:'error', summary:'Add Esp Failed', detail:error});
    })
  }
  cancelEsp(){
    this.newEsp = {id:null,textApiToken:"1",isActive:true};
    this.displayAdd = false;
  }

  //***Edit Esp Functionality***
  editEsp(esp:Esp){
    this.selectedEspIndex = this.esps.indexOf(esp);
    this.selectedEsp = Object.assign({},esp);
    this.displayEdit = true;
  }
  deactivateEsp(){
    this.selectedEsp.isActive = !this.selectedEsp.isActive;
  }
  regenerateApiToken(){
    this.espManagmentService.generateApiToken().subscribe((token:string)=>{
      this.selectedEsp.textApiToken = token;
    },
    error=>{
      console.log(error);
      this.messageService.add({severity:'error', summary:'Token Generation Failed', detail:error});
    })
  }
  onSubmitEdits(){
    this.espManagmentService.saveEsp(this.selectedEsp).subscribe((esp:Esp)=>{
      this.filterService.setEsps();
      this.esps[this.selectedEspIndex] = esp;
      this.updateVisibility();
      this.displayEdit = false;
      this.messageService.add({severity:'success', summary:'Update Was Successful', detail:'Successfully updated ' + esp.nameCompany});
    },
    error=>{
      this.messageService.add({severity:'error', summary:'Upload Failed', detail:error});
    });

  }

  //***Delete Esp Functionality***
  deleteEsp(esp:Esp){
    this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
              this.espManagmentService.deleteEsp(esp).subscribe((res:string)=>{
                  this.filterService.setEsps();
                  this.messageService.add({severity:'success', summary:'Delete Was Successful', detail:'Successfully deleted ' + esp.nameCompany});
                },
                error => {
                  this.messageService.add({severity:'error', summary:'Upload Failed', detail:error});
                }
              )

              if(_.contains(this.esps,esp)){
                let index: number = this.esps.indexOf(esp);
                this.esps.splice(index,1);
                this.updateVisibility();
              }
            },
            reject: () => {

            }
        });

    console.log(this.esps);
  }

  //PrimeNG was having issue with Updating table content. Use this to refresh table
  updateVisibility(): void {
    this.visible = false;
    setTimeout(() => this.visible = true, 0);
  }


}
