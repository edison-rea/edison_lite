import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EnvConfig } from '../../shared/service/env.config.service';
import { Esp } from '../../shared/class/esp.obj';

@Injectable()
export class EspManagementService {

  constructor(private envConfig:EnvConfig,private http:Http) { }

  public getAllEsp = (): Observable<any[]> =>{
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'esp',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public saveEsp = (esp:Esp): Observable<Esp> =>{
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});

    return this.http.put(this.envConfig.getEnvVariable('endPoint') + 'esp',esp,options)
      .map((res:Response)=><Esp>res.json().payload);
  }

  public createEsp = (esp:Esp): Observable<Esp> => {
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});

    return this.http.post(this.envConfig.getEnvVariable('endPoint') + 'esp',esp,options)
      .map((res:Response)=><Esp>res.json().payload);
  }

  public generateApiToken = (): Observable<string> => {
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'esp/api-token',options)
      .map((res:Response)=><string>res.json().payload);
  }

  public deleteEsp = (esp:Esp): Observable<string> => {
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});

    return this.http.delete(this.envConfig.getEnvVariable('endPoint') + 'esp/' + esp.id,options)
      .map((res:Response)=><string>res.json().payload);
  }
}
