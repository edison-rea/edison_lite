import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import {SharedModule} from '../shared/shared.module';

import {AdminRoutingModule} from './admin-routing.module';
import { DataManagementComponent } from './data-management/data-management.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { EspManagementComponent } from './esp-management/esp-management.component';
import { EspApiLogComponent } from './esp-api-log/esp-api-log.component';

import {DataManagementService} from './data-management/data-management.service';
import {EspManagementService} from './esp-management/esp-management.service';
import {UserManagementService} from './user-management/user-management.service';
import {EspApiLogService} from './esp-api-log/esp-api-log.service';

import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';
import {PasswordValidator} from '../shared/validator/password.validator';

@NgModule({
  imports: [
    CommonModule,AdminRoutingModule,SharedModule.forRoot(),ConfirmDialogModule
  ],
  declarations: [AdminComponent, DataManagementComponent, UserManagementComponent, EspManagementComponent, EspApiLogComponent,PasswordValidator],
  providers: [ConfirmationService,DataManagementService,EspManagementService,UserManagementService,EspApiLogService,PasswordValidator]
})
export class AdminModule { }
