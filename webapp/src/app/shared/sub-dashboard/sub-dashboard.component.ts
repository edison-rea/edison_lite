import { Component, OnInit, Input, Output, EventEmitter,
  OnChanges, SimpleChanges, SimpleChange,ViewChild } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import { CurrencyPipe,PercentPipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import {AppService} from '../../app.service';
import {Filter} from '../class/filter.obj';
import {CustomHTMLElement} from '../class/custom.html.element.obj';
import {TableColumn} from '../class/table.column.obj';
import {D3Component} from '../d3/d3.component';
import {AuthService} from '../../auth.service';
import {FilterService} from '../service/filter.service';
import {DummyData} from '../class/dummy.names';

import * as _ from 'underscore';

declare var Plotly: any;

@Component({
  selector: 'edison-subDashboard',
  templateUrl: './sub-dashboard.component.html',
  styleUrls: ['./sub-dashboard.component.css']
})
export class SubDashboardComponent implements OnInit {
  @ViewChild('zambiaChart') private zambiaChart:D3Component;
  @ViewChild('zambiaChartPrint') private zambiaChartPrint:D3Component;
  @Input() subDashboardTitle:string;
  @Input() rawData: any[];
  @Input() dataFilters: Filter[];
  @Input() zambiaData: any[];
  @Input() zambiaCoordinates: [number[],number[]];
  @Input() plotlyData1: any[];
  @Input() plotlyLayout1: any;
  @Input() plotlyData2: any[];
  @Input() plotlyLayout2: any;
  @Input() plotlyOptions: any = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'lasso2d',//Lasso Select
        'toggleSpikelines', //Toggle Spike
        'select2d', //Box Select
        'sendDataToCloud', //Save and Sent to Cloud
        'resetScale2d' //Reset axes
      ]
    };
  @Input() parentDashboard: string = 'bgfz-impact';
  @Input() tableColumns: TableColumn[];
  @Input() showDataTab: boolean = false;
  @Input() dataDownLoad: boolean = false;
  @Input() chartInfo: string;
  @Input() chartInfoLink: any;
  @Input() chartInfoLinkExt: any;
  @Input() chartInfoAfter: string;
  @Input() chartWidth: string = '40vw';
  @Input() zambiaLegendTitle: string = "Number of Subscriptions";
  @Output() onDataFilter = new EventEmitter<any>();
  @Output() onPlotlyClick = new EventEmitter<any>();

  plotlyName1 = 'plotlyDiv1';
  plotlyName2 = 'plotlyDiv2';
  _myPlot: CustomHTMLElement;
  tableData: any[];
  tabIndex:number = 0;
  selectedFilters: any[] = [];
  showPrinterFriendly: boolean = false;
  plotlyOptionsPrint: any = {
      displayModeBar: false
      // displaylogo: false,
      // modeBarButtonsToRemove: [
      //   'lasso2d',//Lasso Select
      //   'toggleSpikelines', //Toggle Spike
      //   'select2d', //Box Select
      //   'sendDataToCloud', //Save and Sent to Cloud
      //   'resetScale2d' //Reset axes
      // ]
    };
  isAdminLocal:boolean = false;
  startDate:Date;
  endDate:Date;

  constructor(public router: Router, private appService:AppService,private _sanitizer: DomSanitizer,
    private authService:AuthService, private filterService:FilterService) { }
  ngOnInit() {
    this.isAdminLocal = this.authService.isAdmin;
    if(this.isAdminLocal && !_.contains(_.pluck(this.dataFilters,'header'),'ESP') && this.dataFilters){
      this.dataFilters.push(this.filterService.getESPFilter());
    }
    let _this = this;
    _.each(this.dataFilters,function(filter){
      _this.selectedFilters.push(filter.filters);
    });
    this.tableData = this.rawData;
  }

  ngOnChanges(changes: SimpleChanges) {
    if(document.getElementById(this.plotlyName1)){
      Plotly.newPlot(this.plotlyName1, this.plotlyData1, this.plotlyLayout1, this.plotlyOptions);
      if(this.plotlyData2){
         Plotly.newPlot(this.plotlyName2, this.plotlyData2, this.plotlyLayout2, this.plotlyOptions);
      }
    }
    if(this.zambiaData && this.zambiaChart){
      this.zambiaChart.updateChart();
    }
    if(this.zambiaData && this.zambiaChartPrint){
      this.zambiaChartPrint.updateChart();
    }
  }

  ngAfterViewInit(){
    Plotly.newPlot(this.plotlyName1, this.plotlyData1, this.plotlyLayout1, this.plotlyOptions);
    if(this.plotlyData2){
      Plotly.newPlot(this.plotlyName2, this.plotlyData2, this.plotlyLayout2, this.plotlyOptions);
    }
  }

  ngAfterViewChecked(){
    let _this = this;
    if(document.getElementById(this.plotlyName1)){
      if(!this._myPlot){
        this._myPlot = <CustomHTMLElement>document.getElementById(this.plotlyName1);
        let _this = this;
        this._myPlot.on('plotly_click',function(data){
          _this.onPlotlyClick.emit(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.plotlyName1)){
            Plotly.Plots.resize(_this._myPlot);
            if(_this.plotlyData2){
              Plotly.Plots.resize(document.getElementById(_this.plotlyName2));
            }
          }
        };
      }
    }
  }

  filterData(){
    let _this = this;
    this.tableData = this.rawData;
    _.each(this.dataFilters,function(dataFilter){
      let filterIndex = _this.dataFilters.indexOf(dataFilter);
     for(let i = 0; i<_this.tableData.length; i++){
       if(_this.tableData[i]['customerGender']==null){
         _this.tableData[i]['customerGender']='Not Provided';
       }
     }
      _this.tableData = _.filter(_this.tableData,function(row){
        return _.contains(_this.selectedFilters[filterIndex],(row[dataFilter.physicalName] || '').toString());
      })
    });
    if(this.isAdminLocal || this.parentDashboard == 'portfolio-monitoring'){
      if(this.tableData.length>0){
        let dtString:string = '';
        if(this.tableData[0]['dtAcquisition']){
          dtString = 'dtAcquisition';
        }else if(this.tableData[0]['dtConnection']){
          dtString = 'dtConnection';
        }else if(this.tableData[0]['dtFunding']){
          dtString = 'dtFunding';
        }

        if(this.startDate){
          this.tableData = _.filter(this.tableData,function(row){
            let rowDate = new Date(row[dtString]);
            return rowDate >=_this.startDate;
          })
        }
        if(this.endDate){
          this.tableData = _.filter(this.tableData,function(row){
            let rowDate = new Date(row[dtString]);
            return rowDate<=_this.endDate;
          })
        }
      }
    }

  }

  processCheckBoxs(e){
    this.filterData();
    this.onDataFilter.emit(this.tableData);
  }

  handleChange(e) {
    this.tabIndex = e.index;
    if(e.index == 0){
      Plotly.Plots.resize(this._myPlot);
    }
  }

  getCSVFileName(){
    let today = new Date();
    let dateTimeStamp = today.getFullYear().toString()+(today.getMonth()+1).toString()+today.getDate().toString();
    return this.subDashboardTitle + '_Data_' + dateTimeStamp;
  }

  showPFModal(){
    this.showPrinterFriendly = true;
    Plotly.newPlot(this.plotlyName1 + 'Print', this.plotlyData1, this.plotlyLayout1, this.plotlyOptionsPrint);
    if(this.plotlyData2 || this.zambiaData){
        Plotly.relayout(this.plotlyName1 + 'Print',{width:350})
    }

    if(this.plotlyData2){
      Plotly.newPlot(this.plotlyName2 + 'Print', this.plotlyData2, this.plotlyLayout2, this.plotlyOptionsPrint);
      Plotly.relayout(this.plotlyName2 + 'Print',{width:350,height:350})
    }
  }
  hidePFModal(){
    this.showPrinterFriendly = false;
  }
  getChartDivClass(){
    if(this.plotlyData2 && this.zambiaData){
      return 'chartDiv-3'
    }
    else if(this.zambiaData){
      return 'chartDiv-2'
    }
    else if(this.plotlyData2){
      return 'chartDiv-2'
    }else{
      return 'chartDiv-1'
    }

  }

  provinceClicked(e){
    console.log(e);
  }
  zambiaDblClick(){
    console.log('Zambia Double Clicked!');
  }
  getSafeChartInfo(){
    return this._sanitizer.bypassSecurityTrustHtml(this.chartInfo);
  }
  navigateRoute(route){
    console.log(route);
    //this.router.navigate([route]);
  }

  getDummyName(index:any){
    let dummy = new DummyData()
    return dummy.getDummyName(index)
  }

}
