import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubDashboardComponent } from './sub-dashboard.component';
import {Router} from "@angular/router";
import {AppService} from "../../app.service";
import {DomSanitizer} from "@angular/platform-browser";
import {AuthService} from "../../auth.service";
import {FilterService} from "../service/filter.service";
import {NO_ERRORS_SCHEMA} from "@angular/core";
import {cold} from "jasmine-marbles";
import {ProvinceEnum} from "../enum/enums.model";
import * as _ from 'underscore';

describe('SubDashboardComponent', () => {
  let component: SubDashboardComponent;
  let fixture: ComponentFixture<SubDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubDashboardComponent ],
      providers: [
        { provide: Router, useClass: MockRouter },
        { provide: AppService, useClass: MockAppService },
        { provide: FilterService, useClass: MockFilterService },
        { provide: AuthService, useClass: MockAuthService },
        { provide: DomSanitizer, useClass: MockDomSanitizer } ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubDashboardComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should update displayed data when gender filters are toggled', () => {
    let data = [
      { customerGender : "M" },
      { customerGender : "F" },
      { customerGender : null }
    ];
    let filters = [
      {
        header: 'Customer Gender',
        physicalName: 'customerGender',
        filters: ['M', 'F', 'Not Provided'],
        values: [{label: 'M', value: 'M'}, {label: 'F', value: 'F'}, {label: 'Not Provided', value: null}],
        type: 'checkbox'
      }
    ];
    component.rawData = data;
    component.dataFilters = filters;
    fixture.detectChanges();

    component.filterData();
    fixture.detectChanges();
    expect(component.tableData.length).toEqual(3);

    component.selectedFilters[0].pop();
    component.filterData();
    fixture.detectChanges();
    expect(component.tableData.length).toEqual(2);

    component.selectedFilters[0].pop();
    component.filterData();
    fixture.detectChanges();
    expect(component.tableData.length).toEqual(1);

    component.selectedFilters[0].pop();
    component.filterData();
    fixture.detectChanges();
    expect(component.tableData.length).toEqual(0);
  });

  it('should update displayed data when province filters are toggled', () => {
    let provinces = Object.values(ProvinceEnum);
    let data = [];
    _.each(provinces,function(province){
      data.push( { province: province } );
    });
    let filters = [
      {
        header:'Province',
        physicalName:'province',
        filters: Object.values(ProvinceEnum),
        type:'checkbox'
      }
    ];
    component.rawData = data;
    component.dataFilters = filters;
    fixture.detectChanges();

    component.filterData();
    fixture.detectChanges();
    expect(component.tableData.length).toEqual(provinces.length);

    for (let index = 0; index < provinces.length; index++) {
      component.selectedFilters[0].pop();
      component.filterData();
      fixture.detectChanges();
      expect(component.tableData.length).toEqual(provinces.length - index - 1);
    }
  });

  it('should filter out data with null provinces', () => {
    let data = [ { provinces : null }];
    let filters = [
      {
        header:'Province',
        physicalName:'province',
        filters: Object.values(ProvinceEnum),
        type:'checkbox'
      }
    ];
    component.rawData = data;
    component.dataFilters = filters;
    fixture.detectChanges();

    component.filterData();
    fixture.detectChanges();
    expect(component.tableData.length).toEqual(0);
  });

});

class MockRouter {
  getEnvVariable(s: String) {
    return s;
  }
}

class MockAppService {
  getTableData(s: String) {
    var expected = cold('--u|', { u: [{id: "1"}] });
    return expected;
  }

  getDataForXML(downloadType: String, startDate: Date, endDate: Date) {
    var expected = cold('--u|', { u: {"response" : "hello"} });
    return expected;
  }
}

class MockFilterService {
  getESPFilter() {
    return {"physicalName" : "espCompanyName"};
  }
}

class MockAuthService {
  isAdmin : true;
}

class MockDomSanitizer {
  sanitize() {
    return "Trusted";
  }

  bypassSecurityTrustUrl(s: String) {
    return "Trusted";
  }
}
