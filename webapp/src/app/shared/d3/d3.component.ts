import { Component, OnInit, OnChanges, ViewChild, ElementRef, Input, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import * as d3 from 'd3';
import {AppService} from '../../app.service';
import {GeoData} from './geodata.obj';
import {Feature} from './feature.obj';
import * as _ from 'underscore';
import  *  as  geoJson  from  '../../../assets/geojson/zambia-province.json';

@Component({
  selector: 'app-d3',
  templateUrl: './d3.component.html',
  styleUrls: ['./d3.component.css']//,
  //encapsulation: ViewEncapsulation.None
})
export class D3Component implements OnInit, OnChanges {
  //@ViewChild('chart') private chartContainer: ElementRef;
  @Input() private data: Array<any>;
  @Input() private name: string = "d3Chart";
  @Input() private pointCoords: [number[],number[]];
  @Output() provinceClicked = new EventEmitter<any>();
  @Input() width: number = 600;
  @Input() height: number = 500;
  @Input() legendTitle: string = "Number of Subscriptions";
  geoData:GeoData;
  total:number = 0;

  constructor(private appService:AppService) { }

  ngOnInit() {
    let __this = this;
    _.each(this.data,function(province){
      if(province['value']){
          __this.total += parseInt(province.value);
      }
    })

  //   this.appService.getZambiaProvinceJson().subscribe((data:any)=>{
  //     this.geoData = data
  //     this.createChart();
  //   },
  //   error=>console.log(error)
  // );

  // replace gejson load with flatfile load
    const data = (<any>geoJson)
    
    this.geoData = data
    this.createChart();


  }

  ngOnChanges(){
    if(this.geoData){
      if (this.data) {
        this.updateChart();
      }
    }

  }

  createChart(){
    let _this = this;

    //Map projection
    let projection = d3.geoMercator()
        .scale(this.width * 4)
        .center([27.852535,-13.801583843404675]) //projection center
        .translate([this.width/2,this.height/2]) //translate to center the map in view

    //Generate paths based on projection
    let path = d3.geoPath()
        .projection(projection);

        // user color brewer 2
    let color = d3.scaleQuantize<string>()
      .range(['#edf8e9', '#c7e9c0', '#a1d99b', '#74c476', '#41ab5d', '#238b45', '#005a32']);

    //Adds the tooltip box
    let div = d3.select("#" + this.name)
      .select('div')
      .append("div")
      .attr("class", "tooltip")
      .style("opacity", 0)
      .style("left", 0)
      .style("top", 0);

    //Select the SVG
    let svg = d3.select("#" + this.name)
      .select('div')
      .append('svg')//d3.select("#" + this.svgId)
      .attr("width", this.width)
      .attr("height", this.height)
      .attr("class", "chart-content")
      .attr('viewBox', '0 0 ' + this.width + ' ' + this.height)
      .attr('preserveAspectRatio', 'xMinYMin meet');

    //Group for the map features
    let features = svg.append("g")
        .attr("class","features");

    let geodata:any = this.geoData;

    //Set input domain for color scale
    color.domain([
        d3.min(this.data, function (d) { return +d.value || Infinity; }),
        d3.max(this.data, function (d) { return +d.value; })
      ]);

    for (var i = 0; i < this.data.length; i++) {
          var dataState = this.data[i].name;
          var dataValue = +this.data[i].value;
          //Find the corresponding state inside the GeoJSON
          for (var j = 0; j < geodata.features.length; j++) {
              var jsonState = geodata.features[j].properties.CAPTION;
              if (dataState == jsonState) {
                  geodata.features[j].properties['amount'] = dataValue;
                  break;
              }
          }
      }

      var paths = svg.selectAll("path")
                   .data(geodata.features);

      paths.enter().append("path")
        .attr("d", path)
        .style("stroke", "black")
        .style("fill", function (d:Feature) {
             var value = d.properties['amount'];
             if (value) { return color(value); }
             else { return "#ccc"; }
         })
        .on('mouseover', showToolTip)
        .on('mouseout', hideToolTip)
        .on("click",clicked);

      let points;
      if(this.pointCoords){
        points = this.pointCoords;
      }else
      {
        points = [];
      }

      svg.selectAll("circle")
    		.data(points).enter()
    		.append("circle")
    		.attr("cx", function (d) { return projection(d['coordinates'])[0]; })
    		.attr("cy", function (d) { return projection(d['coordinates'])[1]; })
    		.attr("r", "5px")
    		.attr("fill", "#8C2A2F")
        .attr("stroke", "#8C2A2F")
        .attr("stroke-width", "1px")
        .on('mouseover', function(d){
          div.transition()
              .duration(200)
              .style("opacity", .9);
          div.html(d.nameText + d.name);
        })
        .on('mouseout', function(d){
          div.transition()
              .duration(200)
              .style("opacity", .0);
        })


      svg.selectAll("text")
        .data(geodata.features)
        .enter()
        .append("svg:text")
        .text(function(d:Feature){
          if(_this.width>=550){
            return d.properties.CAPTION;
          }else{
            return d.properties.CaptionShort;
          }

        })
        .attr("x",function(d:Feature){
          var b = <d3.GeoPermissibleObjects>d;
          if(d.properties['middle-x']){ return path.centroid(b)[0] + (d.properties['middle-x'] * (_this.width / 559)) }
          else{ return path.centroid(b)[0]; }

        })
        .attr("y",function(d:Feature){
          var b = <d3.GeoPermissibleObjects>d;
          if(d.properties['middle-y']){ return path.centroid(b)[1] + (d.properties['middle-y'] * (_this.height / 600)) }
          else{ return path.centroid(b)[1]; }
        })
        .attr("text-anchor","middle")
        .attr("font-size","8pt")
        .attr("fill",function(d:Feature){
          let max = d3.max(color.domain())
          let min = d3.min(color.domain())
          let checkVal = ((max - min) / 2) + min;
          return (parseInt(d.properties['amount']) > checkVal) ? 'white' : 'black';
        })
        .attr("cursor","pointer")
        .on('mouseover', showToolTip)
        .on('mouseout', hideToolTip)
        .on("click",clicked);


    var numStops = 10;
    var countScale = d3.scaleLinear()
	   .domain([d3.min(this.data, function (d) { return +d.value; }), d3.max(this.data, function (d) { return +d.value; })])
	   .range([0, _this.width])
    var countPoint = [];
    var countRange = countScale.domain();
    countRange[2] = countRange[1] - countRange[0];
    for(var i = 0; i < numStops; i++) {
    	countPoint.push(i * countRange[2]/(numStops-1) + countRange[0]);
    }
    //Create the gradient
    svg.append("defs")
    	.append("linearGradient")
    	.attr("id", "legend-subscriptions" + _this.name)
    	.attr("x1", "0%").attr("y1", "0%")
    	.attr("x2", "100%").attr("y2", "0%")
    	.selectAll("stop")
    	.data(d3.range(numStops))
    	.enter().append("stop")
    	.attr("offset", function(d,i) {
    		return countScale( countPoint[i] )/_this.width;
    	})
    	.attr("stop-color", function(d,i) {
    		return color( countPoint[i] );
    	});

      ///////////////////////////////////////////////////////////////////////////
      ////////////////////////// Draw the legend ////////////////////////////////
      ///////////////////////////////////////////////////////////////////////////

      var legendWidth = Math.min(_this.width*0.8, 400);
      //Color Legend container
      var legendsvg = svg.append("g")
      	.attr("class", "legendWrapper")
      	.attr("transform", "translate(" + (_this.width/2) + "," + (_this.height) + ")");

      //Draw the Rectangle
      legendsvg.append("rect")
      	.attr("class", "legendRect")
      	.attr("x", -legendWidth/2)
      	.attr("y", -35)
      	//.attr("rx", hexRadius*1.25/2)
      	.attr("width", legendWidth)
      	.attr("height", 10)
      	.style("fill", "url(#legend-subscriptions" + _this.name + ")")
        .style("stroke", "black");

      //Append title
      legendsvg.append("text")
      	.attr("class", "legendTitle")
      	.attr("x", 0)
      	.attr("y", -45)
      	.style("text-anchor", "middle")
      	.text(_this.legendTitle);

        //Set scale for x-axis
      var xScale = d3.scaleLinear()
      	 .range([-legendWidth/2, legendWidth/2])
      	 .domain([ d3.min(this.data, function (d) { return +d.value; }), d3.max(this.data, function (d) { return +d.value; })] );

      //Define x-axis
      var xAxis = d3.axisBottom(xScale)
        .ticks(5);

      //Set up X axis
      legendsvg.append("g")
      	.attr("class", "axis")
      	.attr("transform", "translate(0," + (-30) + ")")
      	.call(xAxis);

    function showToolTip(d:Feature){
      let amt = parseInt(d.properties['amount']);
      let amtStr = amt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      let total = _this.total;

      div.transition()
          .duration(200)
          .style("opacity", .9);
      div.html(d.properties.CAPTION + " : " + amtStr + " : " + ((amt / total) * 100).toFixed(2) + "%");
    }

    function hideToolTip(d:Feature){
      div.transition()
          .duration(200)
          .style("opacity", .0);
    }

    function clicked(d,i) {
      _this.provinceClicked.emit({province: d});
    }
  }

  updateChart() {
    d3.select("#" + this.name).selectAll('div > *').remove();
    this.createChart();
  }
}
