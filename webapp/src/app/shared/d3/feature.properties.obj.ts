export interface FeatureProperties{
  AREA?:string,
  PERIMETER?: number,
  ID?:string,
  CAPTION?:string,
  CaptionShort?:string,
  amount?:any
}
