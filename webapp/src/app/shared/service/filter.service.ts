import { Injectable } from '@angular/core';
import {Filter} from '../class/filter.obj';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {ProvinceEnum} from "../../shared/enum/enums.model";

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EnvConfig } from '../../shared/service/env.config.service';

import * as _ from 'underscore';

@Injectable()
export class FilterService {
  esps:string[];

  constructor(private envConfig:EnvConfig,private http:Http) {
    this.setEsps();
  }

  setEsps(){
    let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({headers:headers});
    this.http.get(this.envConfig.getEnvVariable('endPoint') + 'filters/esp',options)
      .map((res:Response)=><any[]>res.json().payload).subscribe((data:string[])=>{
        this.esps = data;
      },
      error=>{
        console.log(error);
      }
    );
  }

  getProvinceFilter(){
    let provinceFilter:Filter = {
        header:'Province',
        physicalName:'province',
        // filters:['Luapula','Northern','Muchinga','Eastern','Central','Lusaka','Copperbelt','Southern','Western', 'North-Western'],
        filters: Object.values(ProvinceEnum),
        type:'checkbox'
      };
    return provinceFilter;
  }

  getTierFilter(){
    let tierFilter:Filter = {
      header:'Tiers',
      physicalName:'tierLevel',
      // filters:['1','2','3','4','5','6'],
      filters:['1','2','3'],
      type:'checkbox'
    };
    return tierFilter;
  }

  getESSTechnologyFilter(){
    let eSSTechnologyFilert:Filter = {
      header:'ESS Technology',
      physicalName: 'connectionType',
      filters:['SHS','Microgrid'],
      type:'checkbox'
    };
    return eSSTechnologyFilert;
  }

  getCustomerTypeFilter(){
    let customerTypeFilter:Filter = {
      header:'Customer Type',
      physicalName: 'productUse',
      filters:['Household','Productive','Institutional'],
      type:'checkbox'
    };
    return customerTypeFilter;
  }
  getESPFilter(){
    
    let espArr = _.clone(this.esps);
    let espFilter = {
      header:'ESP',
      physicalName: 'espCompanyName',
      filters: espArr,
      type:'checkbox',
      dummy: ['Space Force','GMB','Amda','Locomotive Energy','Dangote']
    }

    // console.log(espFilter)


    return espFilter;
  }

  getCustomerGenderFilter(){
    let customerGenderFilter:Filter = {
      header:'Customer Gender',
      physicalName: 'customerGender',
      filters:['M', 'F', 'Not Provided'],
      values:[{label:'M',value:'M'},{label:'F',value:'F'},{label:'Not Provided',value:null}],
      type:'checkbox',
    };
    return customerGenderFilter;
  }

  getTransactionTypeFilter(){
    let filter:Filter = {
      header:'Transaction Type',
      physicalName: 'transactionType',
      filters:['Cash','Mobile Money','Token','Other'],
      type:'checkbox'
    }
    return filter;
  }

  getSiteFilter(){
    let filter:Filter = {
      header:'Microgrid Site',
      physicalName: 'microgridSite',
      filters:['Site 1','Site 2','Other'],
      type:'checkbox'
    }
    return filter;
  }

  getPlotlyColors(){
    return [    '#1f77b4',  // muted blue
    '#ff7f0e',  // safety orange
    '#2ca02c',  // cooked asparagus green
    '#d62728',  // brick red
    '#9467bd',  // muted purple
    '#8c564b',  // chestnut brown
    '#e377c2',  // raspberry yogurt pink
    '#7f7f7f',  // middle gray
    '#bcbd22',  // curry yellow-green
    '#17becf'   // blue-teal
    ]
  }

}
