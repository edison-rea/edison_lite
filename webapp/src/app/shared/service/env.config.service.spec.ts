import { TestBed, inject } from '@angular/core/testing';

import { EnvConfig } from './env.config.service';

describe('EnvConfig', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnvConfig]
    });
  });

  it('should be created', inject([EnvConfig], (service: EnvConfig) => {
    expect(service).toBeTruthy();
  }));
});
