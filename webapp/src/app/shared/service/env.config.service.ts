import { Injectable } from '@angular/core';

@Injectable()
export class EnvConfig {


  constructor() { }

  public getEnvVariable(variable:string){
    let retString:string;
    switch(variable){
      case "endPoint":
        //Prod https
        //retString = "https://edisonapi.bgfz.org:8443/edison-prod/";
        //Dev https
        retString = "https://devedisonapi.bgfz.org:8443/edison-dev/";
        //Local
        // retString = "http://localhost:8080/";
        //Dev http

        // retString = "http://ec2-18-194-220-86.eu-central-1.compute.amazonaws.com:8080/edison-dev/";
        //Prod http
        // retString = "http://ec2-18-196-141-170.eu-central-1.compute.amazonaws.com:8080/edison-prod/";
        break;
      default:
        retString = "Invalid Variable";
        break;
    }
    return retString;
  }

  // for testing out a quick demo version of the application
  public getDemoVariable(variable:string){
    let retString:string;
    switch(variable){
      case "endPoint":
        //Local
        retString = "http://127.0.0.1:5000/";
        // retString = "/";

        break;
      default:
        retString = "Invalid Variable";
        break;
    }
    return retString;
  }

}
