import { LoaderState } from '../class/loader';

import {Injectable} from '@angular/core';
// import {Observable} from "rxjs/Observable";
// import {Observer} from "rxjs/Observer";
import {Subject} from 'rxjs/Subject';
//import 'rxjs/add/operator/share';

/**
 * Singleton service, injected at app level
 */
@Injectable()
export class LoadingService {
  private loaderSubject = new Subject<LoaderState>();

  loaderState = this.loaderSubject.asObservable();

  constructor() { }

  show() {
    this.loaderSubject.next(<LoaderState>{show: true});
  }

  hide() {
    this.loaderSubject.next(<LoaderState>{show: false});
  }

    // loading$: Observable<string>;
    // private _observer: Observer<string>;
    //
    // constructor() {
    //     this.loading$ = new Observable<string>(
    //         observer => this._observer = observer).share();
    // }
    //
    // public toggleLoadingIndicator(name) {
    //     if (this._observer) {
    //         this._observer.next(name);
    //     }
    // }
}
