export enum ProvinceEnum {
  LUAPULA="Luapula",
  NORTHERN="Northern",
  MUCHINGA="Muchinga",
  EASTERN="Eastern",
  CENTRAL="Central",
  LUSAKA="Lusaka",
  COPPERBELT="Copperbelt",
  SOUTHERN="Southern",
  WESTERN="Western",
  NORTH_WESTERN="North-Western"
}
