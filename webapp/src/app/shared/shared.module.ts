import {NgModule,ModuleWithProviders} from '@angular/core';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import 'rxjs/add/operator/toPromise';
import {D3Component} from './d3/d3.component';
import {CheckboxModule} from 'primeng/components/checkbox/checkbox';
import {DataTableModule} from 'primeng/components/datatable/datatable';
import {ButtonModule} from 'primeng/components/button/button';
import {DialogModule} from 'primeng/components/dialog/dialog';
import {MenuModule} from 'primeng/components/menu/menu';
import {FileUploadModule} from 'primeng/components/fileupload/fileupload';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import {TabViewModule} from 'primeng/components/tabview/tabview';
import {MultiSelectModule} from 'primeng/components/multiselect/multiselect';
import {CalendarModule} from 'primeng/components/calendar/calendar';
import {SliderModule} from 'primeng/components/slider/slider';
import { SubDashboardComponent } from './sub-dashboard/sub-dashboard.component';
//import {FilterService} from './service/filter.service';
import {EnvConfig} from './service/env.config.service';

@NgModule({
  imports: [CommonModule,CheckboxModule,FormsModule,DataTableModule,ButtonModule,DialogModule,
    MenuModule,FileUploadModule,DropdownModule,
            TabViewModule,MultiSelectModule,CalendarModule,SliderModule],
  declarations: [D3Component, SubDashboardComponent],
  exports: [CommonModule,D3Component,CheckboxModule,FormsModule,DataTableModule,ButtonModule,DialogModule,
    MenuModule,FileUploadModule,DropdownModule,
            TabViewModule,MultiSelectModule,CalendarModule,SliderModule,SubDashboardComponent]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
      return{
        ngModule: SharedModule,
        providers: [EnvConfig]
      }
    }
  }
