export interface Filter{
  header:string,
  physicalName:string,
  filters:string[],
  type:string,
  values?:{label:string,value:string}[]
}

export class FilterObj {

  getValuesOfSelectedFilters(selected: string[], filter: Filter): string[] {
    let value: any = filter['values'];
    let result: string[] = [];
    for (let i: number = 0; i < selected.length; i++) {
      for (let j: number = 0; j < value.length; j++) {
        if (value[j]['label'] === selected[i]) {
          result.push(value[j]['value']);
        }
      }
    }
    return result;
  }
}
