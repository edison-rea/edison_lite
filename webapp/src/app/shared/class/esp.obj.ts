export interface Esp{
  id?: string;
  nameCompany?: string;
  textApiToken?: string;
  textNotificationEmail?: string;
  isActive?:boolean;
}
