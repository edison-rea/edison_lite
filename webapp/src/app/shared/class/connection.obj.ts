import {Esp} from './esp.obj';
import {Tier} from './tier.obj';

export interface Connection{
  connectionType?:string;
  esp?:Esp;
  customer?:any;
  beneficiary?:any;
  province?:string;
  dtAcquisition?:Date;
  textProductUse?:string;
  textProductSubUse?:string;
  product?:any;
  tier?:Tier;
}
