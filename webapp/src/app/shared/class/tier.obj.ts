export interface Tier{
  id?:string;
  nbrTierLevel?:number;
  textApplicationTier?:string;
  nbrWhCoefficient?:number;
  nbrBeneficiaryCoefficient?:number;
  nbrCo2Coeffiecent?:number;
  nbrLampsDisplaceCoefficient?:number;
  nbrLightCoefficient?:number;
  nbrWCoefficient?:number;
}
