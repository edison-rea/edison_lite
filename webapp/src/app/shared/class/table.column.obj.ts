export interface TableColumn{
  header:string,
  field:string,
  sortable?:boolean,
  filter?:boolean,
  filterMatchMode?:string,
  format?:string
}
