export interface User{
  id?:string;
  nameFirst?:string;
  nameLast?:string;
  textEmail?:string;
  hashPassword?:string;
  textPhoneNumber?:string;
  cdRole?:boolean;
  isActive?:boolean;
}
