import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, FormControl } from '@angular/forms';

@Directive({
  selector: '[validatePassword][ngModel],[validatePassword][formControl]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => PasswordValidator), multi: true }
  ]
})
export class PasswordValidator {

  constructor() {}

  validate(c: FormControl) {
    return this.validator(c.value,c.pristine);
  }

  validator(pwd:string,pristine:boolean){
    let valid: boolean = true;
    let ucase: boolean = false;
    let lcase: boolean = false;
    let hasnumber: boolean = false;
    let special: boolean = false;
    if(pwd){
      if((/[a-z]/.test(pwd))){
        lcase = true;
      }
      if((/[A-Z]/.test(pwd))){
        ucase = true;
      }
      if((/[0-9]/.test(pwd))){
        hasnumber = true;
      }
      if((/[^A-Za-z0-9 ]/g.test(pwd))){
        special = true;
      }
      if(pwd.indexOf(' ') >= 0){
        valid = false;
      }
    }

    //console.log('Upper: ',ucase,' Lower: ',lcase,' Number: ',hasnumber,' Special: ',special,' Valid: ', valid);
    if((valid && ucase && lcase && special && hasnumber) || pristine){
      return null;
    }else{
      return {validatePassword:{valid:false}};
    }
  }
}
