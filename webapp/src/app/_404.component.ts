import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-404',
  template: `
  <h1>Error 404 - Page Not Found</h1>
  `
})
export class _404Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
