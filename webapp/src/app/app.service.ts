import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

//import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AppService {

  constructor(private http: Http) { }
  
  public getZambiaProvinceJson(){
   return this.http.get("./assets/geojson/zambia-province.geojson")
    .map(res => res.json());
  }

  public getZambiaData(){
    let provinces = ['Luapula','Northern','Eastern','Central','Lusaka','Copperbelt','Southern','Western','North-Western'];
    let genders = ['Male','Female'];
    let eSSTechnologyTypes = ['SHS','Microgrid'];
    let tiers = ['1','2','3','4','5','6'];
    let customerTypes = ['Household','Productive','Institutional'];
    let customerSubTypes = ['School','Health','Other']
    let esps = ['Standard Microgrid','Fenix','D.Light Atlas','Vitalite','ECS'];
    let paymentStatuses = ['Active','Default']

    let results: any[] = [];

    for(let i = 0; i < 10000; i++){
      let customerType = customerTypes[Math.floor(Math.random() * 3)];
      let customerSubType:string;
      if(customerType=='Institutional'){
        customerSubType = customerSubTypes[Math.floor(Math.random() * 3)];
      }
      results.push({
        province: provinces[Math.floor(Math.random() * 9)],
        customerGender: genders[(Math.random()) > 0.2 ? 0 : 1],
        eSSTechnologyType: eSSTechnologyTypes[Math.floor(Math.random() * 2)],
        tier: tiers[Math.floor(Math.random() * 6)],
        customerType: customerType,
        customerSubType: customerSubType,
        esp: esps[Math.floor(Math.random() * 5)],
        paymentStatus: paymentStatuses[(Math.random()) > 0.05 ? 0 : 1],
      })

    }

    return results;
  }

  public isIE(){
    return typeof window.navigator.msSaveBlob !== 'undefined';
  };

}
