import {NgModule} from '@angular/core';
import {RouterModule,Routes} from '@angular/router';

import {PortfolioMonitoringComponent} from './portfolio-monitoring.component';
import {RepeatCustomerComponent} from './repeat-customer/repeat-customer.component';
import { UpgradesComponent } from './upgrades/upgrades.component';

const routes: Routes = [
  {
    path: '',
    component: PortfolioMonitoringComponent
  },
  {
    path: 'portfolio-monitoring',
    component: PortfolioMonitoringComponent
  },
  {
    path: 'repeat-customer',
    component: RepeatCustomerComponent
  },
  {
    path: 'upgrades',
    component: UpgradesComponent
  }
];

@NgModule({
  imports:[
    RouterModule.forChild(
      routes
    )
  ],
  exports:[
    RouterModule
  ],
  providers:[

  ]
})

export class PortfolioMonitoringRoutingModule{
  constructor(){

  }
}
