import { Component, OnInit } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import {AppService} from '../../app.service';
import {Filter} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {TableColumn} from '../../shared/class/table.column.obj';
import {LoadingService} from '../../shared/service/loading-services';
import {PortfolioMonitoringService} from '../portfolio-monitoring.service';
import * as _ from 'underscore';

@Component({
  selector: 'app-repeat-customer',
  templateUrl: './repeat-customer.component.html',
  styleUrls: ['./repeat-customer.component.css']
})
export class RepeatCustomerComponent implements OnInit {
  dataFilters: Filter[]=[];
  rawData:any[];
  plotlyData:any[];
  plotlyLayout:any;
  tableColumns: TableColumn[];

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
   private loadingService:LoadingService, private portfolioMonitoringService:PortfolioMonitoringService) { }

  ngOnInit() {
    this.loadingService.show();
    this.dataFilters.push(this.filterService.getProvinceFilter());
    this.dataFilters.push(this.filterService.getTierFilter());
    this.dataFilters.push(this.filterService.getCustomerTypeFilter());


    this.tableColumns = [
      {header:'Customer ID',field:'customerId',sortable:true,filter:true,filterMatchMode:'contains',format:'None'},
      {header:'Customer Firstname',field:'customerFirstName',sortable:true,filter:true,filterMatchMode:'startsWith',format:'None'},
      {header:'Customer Lastname',field:'customerLastName',sortable:true,filter:true,filterMatchMode:'contains',format:'None'},
      {header:'Customer Province',field:'province',sortable:true,filter:true,filterMatchMode:'startsWith',format:'None'},
      {header:'Customer Address',field:'address',sortable:true,filter:true,filterMatchMode:'startsWith',format:'None'},
      {header:'Customer Telephone',field:'phoneNumber',sortable:true,filter:true,filterMatchMode:'startsWith',format:'None'},
      {header:'Customer gender',field:'customerGender',sortable:true,filter:true,filterMatchMode:'startsWith',format:'None'},
      {header:'ESP',field:'espCompanyName',sortable:true,filter:true,filterMatchMode:'startsWith',format:'None'},
      {header:'Product ID current',field:'productId',sortable:true,filter:true,filterMatchMode:'startsWith',format:'None'},
      {header:'Microgrid ID',field:'microgridId',sortable:true,filter:true,filterMatchMode:'startsWith',format:'None'},
      {header:'Seller ID',field:'sellerId',sortable:true,filter:true,filterMatchMode:'startsWith',format:'None'},
      {header:'PAYG ID',field:'paygId',sortable:true,filter:true,filterMatchMode:'startsWith',format:'None'},
      {header:'Date of Purchase / Date of connection',field:'dtConnection',sortable:true,filter:true,filterMatchMode:'startsWith',format:'None'}
    ]

    this.plotlyLayout = {
      title:'Duplicate Customers',
      margin: { l: 50, r: 50, b: 50, t: 50 }
    }


    this.portfolioMonitoringService.getDuplicatCustomerData().subscribe((data:any[])=>{
        this.rawData = data;
        this.setPlotlyData(data);
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    );
    //this.rawData = this.appService.getZambiaData();
    //this.setPlotlyData(this.rawData);
  }

  onPlotlyClick(e){
    console.log(e);
  }

  setPlotlyData(data){
    let espData = _.countBy(_.pluck(data,'espCompanyName'),function(esp){return esp;});
    let keys = Object.keys(espData);
    keys = _.sortBy(keys,function(esp){return esp;});

    let text = [];
    let values = [];
    let labels = [];

    _.each(keys,function(key){
      labels.push(key);
      values.push(espData[key]);
      text.push(espData[key].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    })

    this.plotlyData = [{
      x:labels,
      y:values,
      type:'bar',
      text:text,
      textposition: 'auto',
      hoverinfo: 'none',
      marker:{
        color: 'rgb(158,202,225)',
        opacity: 0.6,
        line: {
          color: 'rbg(8,48,107)',
          width: 1.5
        }
      }
    }]
  }
}
