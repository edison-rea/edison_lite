import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepeatCustomerComponent } from './repeat-customer.component';

describe('RepeatCustomerComponent', () => {
  let component: RepeatCustomerComponent;
  let fixture: ComponentFixture<RepeatCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepeatCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepeatCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
