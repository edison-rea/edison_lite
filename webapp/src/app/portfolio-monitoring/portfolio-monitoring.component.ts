import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AppService} from '../app.service';
import {PortfolioMonitoringService} from './portfolio-monitoring.service';
import {LoadingService} from '../shared/service/loading-services';
import {Filter} from '../shared/class/filter.obj';
import {DummyData} from '../shared/class/dummy.names';
import {FilterService} from '../shared/service/filter.service';
import  *  as  targetData  from  '../shared/service/target.json';
import  *  as  portfolioMonitoringData  from  '../shared/service/portfolio.json';




import * as _ from 'underscore';

declare var Plotly: any;

@Component({
  selector: 'app-portfolio-monitoring',
  templateUrl: './portfolio-monitoring.component.html',
  styleUrls: ['./portfolio-monitoring.component.css']
})
export class PortfolioMonitoringComponent implements OnInit {
  lineDataEss:any;
  lineDataVfm:any;
  tableData:any[];
  histogram:Date[];
  financeData:any[];
  targetData:any[];
  rawData:any[];
  linePlotName: string = 'linePlotDiv';
  linePlotName2: string = 'linePlotDivVFM';
  _myPlot: CustomHTMLElement;
  _myPlot2: CustomHTMLElement;
  startDate:Date;
  endDate:Date;
  tabIndex:number = 0;
  duplicateCustomers:number;
  upgrades:number;
  essDeviation:number;
  essPctDvtn:number;
  vfmDeviation:number;
  vfmPctDvtn:number;
  showPrinterFriendly:boolean = false;

  lineLayout:any = {autosize: true};
  plotlyOptions: any = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'lasso2d',//Lasso Select
        'toggleSpikelines', //Toggle Spike
        'select2d', //Box Select
        'sendDataToCloud', //Save and Sent to Cloud
        'resetScale2d' //Reset axes
      ]
    };
  plotlyOptionsPrint: any = {
    displayModeBar: false
  };
  espFilter:Filter;
  selectedEsps:string[];
  provinceFilter:Filter;
  selectedProvince:string[];
  tierFilter:Filter;
  selectedTiers:string[];
  customerTypeFilter:Filter;
  selectedCustomerTypes:string[];

  constructor(private appService: AppService, public router: Router, private portfolioMonitoringService:PortfolioMonitoringService,
    private loadingService:LoadingService, private filterService:FilterService) { }

  ngOnInit() {
    this.loadingService.show();
    this.endDate = new Date();
    this.startDate = new Date();

    this.portfolioMonitoringService.getFinanceData().subscribe((data:any[])=>{
        this.financeData = data;
      },
      error=>{
        console.log(error);
      }
    );
    this.portfolioMonitoringService.getTargetData().subscribe((data:any[])=>{

      const target = (<any>targetData).payload;
      // if data is not loaded, use local  version of the file

        _.each(target,function(row){
          // console.log(row)
          // let qtrYr = row['dtYrQtr']; // TODO: replaced the date conversion regex with a simple pull request 
          // let qtrYr = row['postedDate']; 
          // let year = qtrYr.substring(0,4);
          // let qtr = qtrYr.substring(5,6);
          // let mnth = 1;
          // if(parseInt(qtr)==1){mnth = 3;}else if(parseInt(qtr)==2){mnth = 6;}
          // else if(parseInt(qtr)==3){mnth = 9;}else if(parseInt(qtr)==4){mnth = 12;};
          // row['dtYrQtr'] = new Date(year.toString() + '-' + mnth.toString() + '-' + '01');

          row['dtYrQtr'] = new Date(row['postedDate']);
          
        });


        this.targetData = target;

        // FIXME: run flat pull datatsets
        
        // sub portfolio data
        this.subPortfolioPull()

      },
      error=>{
        console.log(error);

      }
      
    );
    
    // largest pull data
    // this.portfolioMonitoringService.getPortfolioMonitoringData()
    // .subscribe((data:any[])=>{
    //     this.rawData = data;
    //     this.tableData = data;
    //     this.setCellData(data);

    //     let dtArray:Date[] = [];
    //     _.filter(data, function (row){
    //       let dtVal = new Date(row['dtConnection']);
    //       dtArray.push(dtVal);
    //     });
    //     this.startDate=_.min(dtArray);

    //     this.getHistogram();

    //     this.updateDateRange();

    //     this.loadingService.hide();
    //   },
    //   error=>{
    //     console.log(error);
    //     this.loadingService.hide();
    //   }
    // );

    // sub portfolio data
    // this.subPortfolioPull()

    this.lineLayout = {
      title: 'Number of Subscriptions',
      xaxis: {
        title: 'Months'
      },
      yaxis: {
        autorange: true
      },
      margin: {l: 50,r: 50,b: 35,t: 68}
    };
    this.espFilter = this.filterService.getESPFilter();

    this.selectedEsps = this.espFilter.filters;
    this.provinceFilter = this.filterService.getProvinceFilter();
    this.selectedProvince = this.provinceFilter.filters;
    this.tierFilter = this.filterService.getTierFilter();
    this.selectedTiers = this.tierFilter.filters;
    this.customerTypeFilter = this.filterService.getCustomerTypeFilter();
    this.selectedCustomerTypes = this.customerTypeFilter.filters;
  }

  ngAfterViewInit(){
    if(this.lineDataEss){
      
      Plotly.newPlot(this.linePlotName, this.lineDataEss, this.lineLayout, this.plotlyOptions);
      Plotly.newPlot(this.linePlotName2,this.lineDataVfm,{
        title: 'Value For Money (SEK)',
        xaxis: {
          title: 'Months'
        },
        yaxis: {
          autorange: true
        },
        margin: {l: 50,r: 50,b: 35,t: 68}
      },this.plotlyOptions);
    }
  }
  ngAfterViewChecked(){
    let _this = this;
    if(document.getElementById(this.linePlotName) && this.lineDataEss
      && document.getElementById(this.linePlotName2) && this.lineDataVfm){
      if(!this._myPlot && !this._myPlot2){
        this._myPlot = <CustomHTMLElement>document.getElementById(this.linePlotName);
        this._myPlot2 = <CustomHTMLElement>document.getElementById(this.linePlotName2);
        let _this = this;
        this._myPlot.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        this._myPlot2.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.linePlotName) && document.getElementById(_this.linePlotName2)){
            Plotly.Plots.resize(_this._myPlot);
            Plotly.Plots.resize(_this._myPlot2);
          }
        };
      }
    }
  }

  getHistogram(){
    this.portfolioMonitoringService.getHistogram(this.startDate,this.endDate).subscribe((data:any[])=>{
        this.histogram = data;
      },error=>{
        console.log(error);
      }
    );
  }

  setPlotlyData(data:any[],finData:any[]){

    let _this = this;
    let endDates = _.pluck(this.histogram,'endDate');
    endDates.sort();
    let xDates = [];
    let yEss = [];
    let yVfm = [];
    _.each(endDates,function(i:Date){
      let dt = new Date(i);
      xDates.push(dt);
      let filtered = _.filter(data,function(row){
        let dtVal = new Date(row['dtConnection']);
        return dtVal<=dt;
      });

      let filteredFinance = _.filter(finData,function(row){
        let dtVal = new Date(row['dtFunding']);
        return dtVal<=dt;
      });

      if(filtered){
        yEss.push(filtered.length);
        let filteredTypes = _.countBy(filtered,function(row){return row['tierLevel']});
        if(filteredTypes){
          let yVfmVal = 0;
          let tierData = Object.keys(filteredTypes);
          let tierCo = {'1':0.5,'2':1,'3':2,'4':3,'5':4,'6':5};
          for(let t in tierData){
            yVfmVal += filteredTypes[tierData[t]] * tierCo[tierData[t]];
          }
          let vfmNumerator = 0;
          _.each(filteredFinance,function(row){vfmNumerator += row['amtFunding']});
          yVfm.push(vfmNumerator / yVfmVal);
        }
      }else{
        yEss.push(0);
        yVfm.push(0);
      }
    });

    let filteredTargets = _.filter(this.targetData,function(row){
      let dt = new Date(row['dtYrQtr']);
      return dt >= _this.startDate && dt <= _this.endDate && _.contains(_this.selectedEsps,row[_this.espFilter.physicalName]);
    });
    let targetX = _.uniq(_.pluck(filteredTargets,'dtYrQtr'),false,function(date){return date.getTime()});

    var date_sort_asc = function (date1, date2) {
      if (date1 > date2) return 1;
      if (date1 < date2) return -1;
      return 0;
    };

    targetX.sort(date_sort_asc);

    let targetYEss = [];
    let targetYVfm = [];
    _.each(targetX,function(dtYrQtr){
      let dtData = _.filter(filteredTargets,function(row){
        let dt = new Date(row['dtYrQtr']);
        return dt <= dtYrQtr;
      });
      let ess = 0;
      let vfm = 0;
      let tierCo = {'1':0.5,'2':1,'3':2,'4':3,'5':4,'6':5};
      _.each(dtData,function(row){
        ess += row['nbrEssTarget'];
        vfm += (row['nbrEssTarget'] * tierCo[row['nbrTierLevel'].toString()]);
      });

      targetYEss.push(ess);
      let filteredFinance = _.filter(finData,function(row){
        let dtVal = new Date(row['dtFunding']);
        return dtVal<=dtYrQtr;
      });
      let vfmNumerator = 0;
      _.each(filteredFinance,function(row){vfmNumerator += row['amtFunding']});
      if(vfm>0){targetYVfm.push(vfmNumerator / vfm);}else{targetYVfm.push(0)};

    });

    this.lineDataEss = [
      {
        name:'Connections',
        x:xDates,
        y:yEss,
        line: {
          dash:'solid',
          width: 2,
          color: 'blue'
        },
        showLegend: false
      },
      {
        name:'Target',
        x:targetX,
        y:targetYEss,
        line: {
          dash:'solid',
          width: 2,
          color: 'oragne'
        },
        marker:{
          color:'orange'
        },
        mode: 'lines+markers',
        showLegend: false
      }
    ];
    this.lineDataVfm = [
      {
        name:'VFM',
        x:xDates,
        y:yVfm,
        line: {
          dash:'solid',
          width: 2,
          color: 'blue'
        },
        showLegend: false
      },
      {
        name:'Target',
        x:targetX,
        y:targetYVfm,
        line: {
          dash:'solid',
          width: 2,
          color: 'oragne'
        },
        marker:{
          color:'orange'
        },
        mode: 'lines+markers',
        showLegend: false
      }
    ];

    let totalEss = data.length;
    let totalEssTarget = 0;

    if(targetYEss.length>0){totalEssTarget = targetYEss[(targetYEss.length - 1)];}
    let totalVfm = yVfm[(yVfm.length - 1)];
    let totalVfmTarget = 0;
    if(targetYVfm.length>0){totalVfmTarget = targetYVfm[(targetYVfm.length - 1)];}

    this.essDeviation = totalEssTarget - totalEss;//If > 0 then under
    if(totalEssTarget > 0){
      this.essPctDvtn = Math.abs(this.essDeviation) / totalEssTarget;
    }else{
      this.essPctDvtn = null;
    }
    this.vfmDeviation = totalVfmTarget - totalVfm;
    if(totalVfmTarget > 0){
      this.vfmPctDvtn = Math.abs(this.vfmDeviation) / totalVfmTarget;
    }else{
      this.vfmPctDvtn = null;
    }
  }

  setCellData(data:any[]){
    let _this = this;
    let sortData = _.sortBy(data,'dtConnection');
    sortData.reverse();
    let customerData = _.groupBy(sortData,'customerId');

    this.duplicateCustomers = 0;
    this.upgrades = 0;
    let customerKeys = Object.keys(customerData);
    _.each(customerKeys,function(key){
      let customer = customerData[key];
      if(customer.length>1){
        _this.duplicateCustomers += 1;
        if(parseInt(customer[0]['tierLevel']) > parseInt(customer[1]['tierLevel'])){
          _this.upgrades += 1;
        }
      }
    })
  }

  updateDateRange(){
    this.portfolioMonitoringService.getHistogram(this.startDate,this.endDate).subscribe((data:any[])=>{
        this.histogram = data;
        this.filterData();
        this.ngAfterViewInit();
      },error=>{
        console.log(error);
      }
    );
  }

  setToDefault(){
    this.endDate = new Date();
    this.startDate = new Date();
    let dtArray:Date[] = [];
    _.filter(this.rawData, function (row) {
      let dtVal = new Date(row['dtConnection']);
      dtArray.push(dtVal);
    });
    this.startDate = _.min(dtArray);
    // this.startDate.setMonth(this.startDate.getMonth() - 3);
    this.updateDateRange();
  }

  filterData(){
    let _this = this;
    let data = _.filter(this.rawData,function(row){
      return _.contains(_this.selectedEsps,row[_this.espFilter.physicalName])
          && _.contains(_this.selectedProvince,row[_this.provinceFilter.physicalName])
          && _.contains(_this.selectedTiers,row[_this.tierFilter.physicalName].toString())
          && _.contains(_this.selectedCustomerTypes,row[_this.customerTypeFilter.physicalName])
    });
    let finData = _.filter(this.financeData,function(row){return _.contains(_this.selectedEsps,row[_this.espFilter.physicalName])});
    this.tableData = data;
    this.setPlotlyData(data,finData);
    this.setCellData(data);
  }

  processCheckBoxs(e){
    this.filterData();
    this.ngAfterViewInit();
  }

  onClick(e){
    console.log(e);
  }

  download(){
    console.log(this.tabIndex);
  }

  handleChange(e) {
    this.tabIndex = e.index;
    if(e.index == 0){
      Plotly.Plots.resize(this._myPlot);
      Plotly.Plots.resize(this._myPlot2);
    }
  }

  getCSVFileName(){
    let today = new Date();
    let dateTimeStamp = today.getFullYear().toString()+(today.getMonth()+1).toString()+today.getDate().toString();
    return 'Portfolio_Monitoring_Data_' + dateTimeStamp;
  }

  showPFModal(){
    this.showPrinterFriendly = true;

    if(this.lineDataEss){
      let layout = Object.assign({},this.lineLayout);
      layout['width'] = 600;
      layout['height'] = 400;

      Plotly.newPlot(this.linePlotName + 'Print', this.lineDataEss, layout, this.plotlyOptionsPrint);
      Plotly.newPlot(this.linePlotName2 + 'Print',this.lineDataVfm,{
        title: 'Value For Money (Targets Dashed)',
        xaxis: {
          title: 'Months'
        },
        yaxis: {
          autorange: true
        },
        margin: {l: 50,r: 50,b: 35,t: 68},
        width:600,
        height: 400
      },this.plotlyOptionsPrint);
    }
  }
  hidePFModal(){
    Plotly.Plots.resize(document.getElementById(this.linePlotName));
    Plotly.Plots.resize(document.getElementById(this.linePlotName2));
    this.showPrinterFriendly = false;
  }

  getDummyName(index:any){
    let dummy = new DummyData()
    return dummy.getDummyName(index)
  }

  // subtitute portfolio data pull
  subPortfolioPull(){
    
    const data = (<any>portfolioMonitoringData).payload

    this.rawData = data;
    this.tableData = data;
    this.setCellData(data);

    let dtArray:Date[] = [];
    _.filter(data, function (row){
      let dtVal = new Date(row['dtConnection']);
      dtArray.push(dtVal);
    });
    this.startDate=_.min(dtArray);

    this.getHistogram();

    this.updateDateRange();

    this.loadingService.hide();
  }

}

export class CustomHTMLElement extends HTMLElement {
  constructor() {
    super();
  }
  on(event_type, cb) {
  }
}
