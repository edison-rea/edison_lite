import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortfolioMonitoringComponent } from './portfolio-monitoring.component';
import {SharedModule} from '../shared/shared.module';

import {PortfolioMonitoringRoutingModule} from './portfolio-monitoring-routing.module';
import { RepeatCustomerComponent } from './repeat-customer/repeat-customer.component';
import { UpgradesComponent } from './upgrades/upgrades.component';
import {PortfolioMonitoringService} from './portfolio-monitoring.service';

@NgModule({
  imports: [
    CommonModule,PortfolioMonitoringRoutingModule,SharedModule.forRoot()
  ],
  declarations: [PortfolioMonitoringComponent, RepeatCustomerComponent, UpgradesComponent],
  providers:[PortfolioMonitoringService]
})
export class PortfolioMonitoringModule { }
