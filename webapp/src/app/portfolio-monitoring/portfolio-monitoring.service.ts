import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EnvConfig } from '../shared/service/env.config.service';

@Injectable()
export class PortfolioMonitoringService {
  httpPrefix:string = this.envConfig.getEnvVariable('endPoint') + 'portfolio-monitoring';
  headers = new Headers({'Content-Type':'application/json',
                        'Authorization':'Bearer ' + localStorage.getItem('access_token')});

  constructor(private envConfig:EnvConfig, private http:Http) { }

  public getPortfolioMonitoringData = (): Observable<any[]> => {
    let options = new RequestOptions({headers:this.headers});
    
    return this.http.get(this.httpPrefix,options)
      .map((res:Response)=><any[]>res.json().payload);


  }

  public getHistogram = (startDate:Date,endDate:Date): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    let startMonth = startDate.getMonth()+1;
    let endMonth = endDate.getMonth()+1;
    // let strStartDate = startDate.getFullYear() + '-' + startDate.getMonth() + '-' + startDate.getDate();
    let strStartDate = startDate.getFullYear() + '-' + startMonth + '-' + startDate.getDate();
    // let strEndDate = endDate.getFullYear() + '-' + endDate.getMonth() + '-' + endDate.getDate();
    let strEndDate = endDate.getFullYear() + '-' + endMonth + '-' + endDate.getDate();
    return this.http.get(this.httpPrefix + "/histogram?start-date=" + strStartDate + "&end-date=" + strEndDate,options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getDuplicatCustomerData = (): Observable<any[]> => {
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/duplicate-customers',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getUpgradesData = (): Observable<any[]> => {
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.httpPrefix + '/upgrades',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getFinanceData = (): Observable<any[]> =>{
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + '/financial-leverage',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  public getTargetData = (): Observable<any[]> =>{ //TODO: need to replace with local dataset
    let options = new RequestOptions({headers:this.headers});

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + '/esp-target',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

}
