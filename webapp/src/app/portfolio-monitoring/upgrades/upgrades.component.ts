import { Component, OnInit, Input, Output, EventEmitter,
  OnChanges, SimpleChanges, SimpleChange,ViewChild } from '@angular/core';
import {Router,Params,ActivatedRoute} from '@angular/router';
import { CurrencyPipe,PercentPipe } from '@angular/common';
import {CustomHTMLElement} from '../../shared/class/custom.html.element.obj';
import {AppService} from '../../app.service';
import {Filter} from '../../shared/class/filter.obj';
import {FilterService} from  '../../shared/service/filter.service';
import {PortfolioMonitoringService} from '../portfolio-monitoring.service';
import {LoadingService} from '../../shared/service/loading-services';
import * as _ from 'underscore';

declare var Plotly: any;

@Component({
  selector: 'app-upgrades',
  templateUrl: './upgrades.component.html',
  styleUrls: ['./upgrades.component.css']
})
export class UpgradesComponent implements OnInit {
  rawData:any[];
  subDashboardTitle:string = 'Upgrades';
  showPrinterFriendly:boolean = false;
  parentDashboard:string = 'portfolio-monitoring';
  plotlyName1:string='plotlyDiv1';
  plotlyName2:string='plotlyDiv2';
  plotlyName3:string='plotlyDiv3';
  plotlyName4:string='plotlyDiv4';
  plotlyData1:any[];
  plotlyData2:any[];
  plotlyData3:any[];
  plotlyData4:any[];
  plotlyLayout1:any;
  plotlyLayout2:any;
  plotlyLayout3:any;
  plotlyLayout4:any;
  plotlyOptions:any = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'lasso2d',//Lasso Select
        'toggleSpikelines', //Toggle Spike
        'select2d', //Box Select
        'sendDataToCloud', //Save and Sent to Cloud
        'resetScale2d' //Reset axes
      ]
    };
  plotlyPrintOptions: any = {
    displayModeBar: false
  }
  _myPlot: CustomHTMLElement;
  _myPlot2: CustomHTMLElement;
  _myPlot3: CustomHTMLElement;
  _myPlot4: CustomHTMLElement;
  tableData: any[];
  tabIndex:number = 0;

  provinceFilter:Filter;
  selectedProvinces:string[];

  currTierFilter:Filter;
  selectedCurrTiers:string[];

  prevTierFilter:Filter;
  selectedPrevTiers:string[];

  customerTypeFilter:Filter;
  selectedCustomerTypes:string[];

  espFilter:Filter;
  selectedEsps:string[];

  startDate:Date;
  endDate:Date;

  constructor(public router: Router, private appService:AppService, private filterService:FilterService,
    private portfolioMonitoringService:PortfolioMonitoringService, private loadingService:LoadingService) { }

  ngOnInit() {
    this.loadingService.show();
    this.portfolioMonitoringService.getUpgradesData().subscribe((data:any[])=>{
        this.rawData = data;
        this.tableData = this.rawData;
        this.setChartData(this.rawData);
        if(document.getElementById(this.plotlyName1)){
          this.ngAfterViewInit();
        }
        this.loadingService.hide();
      },
      error=>{
        console.log(error);
        this.loadingService.hide();
      }
    )
    this.plotlyLayout1 = {
      title: 'Upgrades by Tier',
      showlegend: true,
      autosize: true,
      // xaxis: { title: 'Province' },
      yaxis: { title: 'Upgrades' },
      barmode: 'stack',
      hovermode:'closest'
    };
    this.plotlyLayout2 = {
      title: 'Total by Tier',
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: 'Upgrades',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: { l: 10, r: 10, b: 30, t: 50 },
      showlegend:false
    };
    this.plotlyLayout3 = {
      title: 'Downgrades by Tier',
      showlegend: true,
      autosize: true,
      // xaxis: { title: 'Province' },
      yaxis: { title: 'Downgrades' },
      barmode: 'stack',
      hovermode:'closest'
    };
    this.plotlyLayout4 = {
      title: 'Total by Tier',
      annotations: [
        {
          font: {
            size: 14
          },
          showarrow: false,
          text: 'Downgrades',
          x: 0.5,
          y: 0.5
        }
      ],
      margin: { l: 10, r: 10, b: 30, t: 50 },
      showlegend:false
    };

    this.provinceFilter = this.filterService.getProvinceFilter();
    this.selectedProvinces = this.provinceFilter.filters;
    this.currTierFilter = this.filterService.getTierFilter();
    this.selectedCurrTiers = this.currTierFilter.filters;
    this.prevTierFilter = this.filterService.getTierFilter();
    this.selectedPrevTiers = this.prevTierFilter.filters;
    this.customerTypeFilter = this.filterService.getCustomerTypeFilter();
    this.selectedCustomerTypes = this.customerTypeFilter.filters;
    this.espFilter = this.filterService.getESPFilter();
    this.selectedEsps = this.espFilter.filters;
  }

  ngAfterViewInit(){
    if(this.plotlyData4){
      Plotly.newPlot(this.plotlyName1, this.plotlyData1, this.plotlyLayout1, this.plotlyOptions);
      Plotly.newPlot(this.plotlyName2, this.plotlyData2, this.plotlyLayout2, this.plotlyOptions);
      Plotly.newPlot(this.plotlyName3, this.plotlyData3, this.plotlyLayout3, this.plotlyOptions);
      Plotly.newPlot(this.plotlyName4, this.plotlyData4, this.plotlyLayout4, this.plotlyOptions);
    }
  }

  ngAfterViewChecked(){
    let _this = this;
    if(document.getElementById(this.plotlyName1)
      && document.getElementById(this.plotlyName2)
      && document.getElementById(this.plotlyName3)
      && document.getElementById(this.plotlyName4)
      && this.plotlyData1 ){
      if(!this._myPlot && !this._myPlot2 && !this._myPlot3 && !this._myPlot4){
        this._myPlot = <CustomHTMLElement>document.getElementById(this.plotlyName1);
        this._myPlot2 = <CustomHTMLElement>document.getElementById(this.plotlyName2);
        this._myPlot3 = <CustomHTMLElement>document.getElementById(this.plotlyName3);
        this._myPlot4 = <CustomHTMLElement>document.getElementById(this.plotlyName4);
        let _this = this;
        this._myPlot.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        this._myPlot2.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        this._myPlot3.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        this._myPlot4.on('plotly_click',function(data){
          console.log(data);
          return;
        });
        window.onresize = function() {
          if(_this.tabIndex == 0 && document.getElementById(_this.plotlyName1)){
            Plotly.Plots.resize(_this._myPlot);
            Plotly.Plots.resize(_this._myPlot2);
            Plotly.Plots.resize(_this._myPlot3);
            Plotly.Plots.resize(_this._myPlot4);
          }
        };
      }
    }
  }
  setChartData(data){
    if(data.length>0){
      let _this = this;
      let tiers = [1,2,3,4,5,6];
      this.plotlyData1 = [];
      this.plotlyData3 = [];
      let pieTotalsUp = [];
      let textUp = [];
      let pieTotalsDown = [];
      let textDown = [];

      for(let i in tiers){
        let totalUpgrades = 0;
        let totalDowngrades = 0;
        let tierData = _.where(data,{tierLevelCurrent:tiers[i]});
        let espUpgrades = _.countBy(data,function(row){return row.espCompanyName});
        let espKeys = Object.keys(espUpgrades);
        espKeys.sort();
        _.each(espKeys,function(key){espUpgrades[key]=0});
        let espDowngrades = Object.assign({},espUpgrades);

        _.each(tierData,function(row){
          if(row['tierLevelCurrent']>row['tierLevelPrevious']){
            espUpgrades[row['espCompanyName']] += 1;
            totalUpgrades += 1;
          }else if(row['tierLevelCurrent']<row['tierLevelPrevious']){
            espDowngrades[row['espCompanyName']] += 1;
            totalDowngrades += 1;
          }
        });

        let yUp = [];
        let yDown = [];
        _.each(espKeys,function(key){
          yUp.push(espUpgrades[key]);
          yDown.push(espDowngrades[key]);
        })

        let traceUp = {
          x:espKeys,
          y:yUp,
          name: 'Tier ' + tiers[i].toString(),
          type: 'bar'
        }
        let traceDown = {
          x:espKeys,
          y:yDown,
          name: 'Tier ' + tiers[i].toString(),
          type: 'bar'
        }

        pieTotalsUp.push(totalUpgrades);
        textUp.push(totalUpgrades.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        pieTotalsDown.push(totalDowngrades);
        textDown.push(totalDowngrades.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        this.plotlyData1.push(traceUp);
        this.plotlyData3.push(traceDown);
      }

      let tierLabels = [];
      _.each(tiers,function(tier){tierLabels.push('Tier ' + tier.toString())});

      this.plotlyData2 = [{
        values: pieTotalsUp,
        labels: tierLabels,
        type: 'pie',
        hole: 0.3,
        text: textUp,
        textposition: 'inside',
        hoverinfo: 'label',
        textfont: {
          color: 'white'
        },
        marker: {
          colors: this.filterService.getPlotlyColors()
        }
      }];
      this.plotlyData4 = [{
        values: pieTotalsDown,
        labels: tierLabels,
        type: 'pie',
        hole: 0.3,
        text: textDown,
        textposition: 'inside',
        hoverinfo: 'label',
        textfont: {
          color: 'white'
        },
        marker: {
          colors: this.filterService.getPlotlyColors()
        }
      }];
    }else{
      this.plotlyData1 = [];
      this.plotlyData2 = [];
      this.plotlyData3 = [];
      this.plotlyData4 = [];
    }
  }

  filterData(){
    this.tableData = this.rawData;
    let _this = this;
    this.tableData = _.filter(this.tableData,function(row){
      return _.contains(_this.selectedProvinces,row[_this.provinceFilter.physicalName])
          && _.contains(_this.selectedCurrTiers,row['tierLevelCurrent'].toString())
          && _.contains(_this.selectedPrevTiers,row['tierLevelPrevious'].toString())
          && _.contains(_this.selectedCustomerTypes,row[_this.customerTypeFilter.physicalName])
          && _.contains(_this.selectedEsps,row[_this.espFilter.physicalName])
    });

    if(this.startDate){
      this.tableData = _.filter(this.tableData,function(row){
        let rowDate = new Date(row['dtConnection']);
        return rowDate >=_this.startDate;
      })
    }
    if(this.endDate){
      this.tableData = _.filter(this.tableData,function(row){
        let rowDate = new Date(row['dtConnection']);
        return rowDate<=_this.endDate;
      })
    }


    this.setChartData(this.tableData);
  }

  handleChange(e) {
    this.tabIndex = e.index;
    if(e.index == 0){
      Plotly.Plots.resize(this._myPlot);
      Plotly.Plots.resize(this._myPlot2);
      Plotly.Plots.resize(this._myPlot3);
      Plotly.Plots.resize(this._myPlot4);
    }
  }

  showPFModal(){
    this.showPrinterFriendly = true;
    let layout1 = this.plotlyLayout1;
    let layout2 = this.plotlyLayout2;
    let layout3 = this.plotlyLayout3;
    let layout4 = this.plotlyLayout4;
    layout1['width'] = 300;
    layout2['width'] = 300;
    layout3['width'] = 300;
    layout4['width'] = 300;
    layout1['height'] = 300;
    layout2['height'] = 300;
    layout3['height'] = 300;
    layout4['height'] = 300;
    layout2['annotations'][0]['text'] = "Up";
    layout4['annotations'][0]['text'] = "Down";

    Plotly.newPlot(this.plotlyName1 + 'Print', this.plotlyData1, layout1, this.plotlyPrintOptions);
    Plotly.newPlot(this.plotlyName2 + 'Print', this.plotlyData2, layout2, this.plotlyPrintOptions);
    Plotly.newPlot(this.plotlyName3 + 'Print', this.plotlyData3, layout3, this.plotlyPrintOptions);
    Plotly.newPlot(this.plotlyName4 + 'Print', this.plotlyData4, layout4, this.plotlyPrintOptions);
    Plotly.Plots.resize(document.getElementById(this.plotlyName1));
    Plotly.Plots.resize(document.getElementById(this.plotlyName2));
    Plotly.Plots.resize(document.getElementById(this.plotlyName3));
    Plotly.Plots.resize(document.getElementById(this.plotlyName4));
  }
  hidePFModal(){
    this.showPrinterFriendly = false;
  }

  getCSVFileName(){
    let today = new Date();
    let dateTimeStamp = today.getFullYear().toString()+(today.getMonth()+1).toString()+today.getDate().toString();
    return 'Upgrads_Downgrades_Data_' + dateTimeStamp;
  }

  processCheckBoxs(e){
    console.log(e);
    this.filterData();
    if(document.getElementById(this.plotlyName1)){
      Plotly.newPlot(this.plotlyName1, this.plotlyData1, this.plotlyLayout1, this.plotlyOptions);
    }
    if(document.getElementById(this.plotlyName2)){
      Plotly.newPlot(this.plotlyName2, this.plotlyData2, this.plotlyLayout2, this.plotlyOptions);
    }
    if(document.getElementById(this.plotlyName3)){
      Plotly.newPlot(this.plotlyName3, this.plotlyData3, this.plotlyLayout3, this.plotlyOptions);
    }
    if(document.getElementById(this.plotlyName4)){
      Plotly.newPlot(this.plotlyName4, this.plotlyData4, this.plotlyLayout4, this.plotlyOptions);
    }
  }

}
