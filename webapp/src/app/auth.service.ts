import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {EnvConfig} from './shared/service/env.config.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import * as _ from 'underscore';

@Injectable()
export class AuthService {
  constructor(private envConfig:EnvConfig, private http:Http) { }

  isLoggedIn = false;
  isAdmin = false;

  // store the URL so we can redirect after logging in
  redirectUrl: string;
  //timeoutHandle: any;

  login(email:string,pwd:string): Observable<boolean> {
    return this.getToken(email,pwd);
  }

  public getRoles = (): Observable<any[]> => {
    let headers = new Headers({'Content-Type':'application/json',
                               'Authorization':'Bearer ' + localStorage.getItem('access_token')});
    let options = new RequestOptions({headers:headers});

    return this.http.get(this.envConfig.getEnvVariable('endPoint') + 'roles',options)
      .map((res:Response)=><any[]>res.json().payload);
  }

  logout(): void {
    this.isLoggedIn = false;
    this.isAdmin = false;
    //localStorage.clear();
    localStorage.removeItem('access_token');
    localStorage.removeItem('authority');
    localStorage.removeItem('access_token');
    localStorage.removeItem('token_type');
    localStorage.removeItem('expires_in');
    localStorage.removeItem('scope');
    localStorage.removeItem('jti');
    localStorage.removeItem('usr_email');
  }

  checkAuth(){
    if(localStorage.getItem('access_token')){
      this.getRoles().subscribe((res:any[])=>{
          if(_.contains(_.pluck(res,'authority'),'ROLE_ADMIN')){
            localStorage.setItem('authority','ROLE_ADMIN');
            this.isAdmin = true;
          }else if(_.contains(_.pluck(res,'authority'),'ROLE_USER')){
            localStorage.setItem('authority','ROLE_USER');
            this.isAdmin = false;
          }else{
            localStorage.setItem('authority','ROLE_PUBLIC');
            this.isAdmin = false;
          }
          this.isLoggedIn = true;

        },
        error=>{
          this.logout();
        }
      );
    }else if(this.isLoggedIn){
      this.logout();
    }
  }

  public setNewTimeOut(){
    if(this.isLoggedIn){
      let now = new Date();
      if(!localStorage.getItem('timeout_check')){
        localStorage.setItem('timeout_check',now.toString());
      }
      let timeOutCheck = new Date(localStorage.getItem('timeout_check'));
      let fiveMin = 1000 * 60 * 5;
      if((now.getTime() - timeOutCheck.getTime()) > fiveMin){
        this.checkAuth();
        localStorage.setItem('timeout_check',now.toString());
      }
    }
  }

  private getToken = (username:string,password:string): Observable<any> => {
    let headers = new Headers({
            "Content-type": "application/x-www-form-urlencoded; charset=utf-8"
        });
    let options = new RequestOptions({ headers: headers });

    let data = "username=" + username + "&password=" + encodeURIComponent(password) + "&grant_type=password&" +
        "client_secret=755f81df-1d92-11e8-b577-5891cf6b43c8&client_id=EdisonClient";

    return this.http.post(this.envConfig.getEnvVariable('endPoint') + 'oauth/token', data, options)
      .map(res => res.json())
  }
}
